import { z } from "zod";

const page = z.number().min(0).default(0);
const maxPage = z.number().min(0).default(0);
const itemsPerPage = z.number().min(1).max(50).default(20);
const sortBy = z
  .object({
    key: z.string(),
    direction: z.enum(["asc", "desc"]),
  })
  .optional();

export function paginated<T extends z.AnyZodObject>(
  zShape: T,
) {
  return z.object({
    maxPage,
    page,
    itemsPerPage,
    sortBy,
    items: z.array(zShape),
    // [itemsKey]: zShape
  });
}

export const paginatedInput = (zShape: z.AnyZodObject) =>
  z.object({
    page,
    itemsPerPage,
    sortBy,
    where: zShape,
  });

export type PaginatedZodType<T extends z.AnyZodObject> =
  ReturnType<typeof paginated<T>>;
export type PaginatedShape<T extends z.AnyZodObject> =
  z.infer<PaginatedZodType<T>>;

type TableConfig<T = any, S = any> = {
  label?: string;
  type?: "array"|"primitive";
  renderAs:
    | "string"
    | "number"
    | "pill"
    | "image"
    | "date";
};

export type TableSchema<T = any, S = any> = {
  [key in keyof T]?: TableConfig<T, S>;
} | {
  [key: string]: TableConfig<T, S>;
};

type EditConfig<T = any, S = any> = {
  label?: string;
  type?: "primitive" | "array" | "json";
  disabled?: boolean;
  reference?: EditSchema<S>;
  description?: string;
  options?: { label: string; value: any }[];
  renderAs: "string" |
  "number" |
  "pill" |
  "image" |
  "date" |
  "markdown"|
  "rte";
};

export type EditSchema<T = any, S = any> = {
  [key in keyof T]?: EditConfig<T, S>;
} | {
  [key: string]: EditConfig<T, S>;
};