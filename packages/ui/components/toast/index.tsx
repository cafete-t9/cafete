import {
  AnimatePresence,
  motion,
  useScroll,
} from "framer-motion";
import { useEffect, useState } from "react";
import { ErrorIcon } from "../icons/ErrorIcon";
import { InfoIcon } from "../icons/InfoIcon";
import { SuccessIcon } from "../icons/SuccessIcon";
import { WarningIcon } from "../icons/WarningIcon";

export type TAdminToast = {
  id: string;
  title?: string;
  description?: string;
  duration?: number;
  snapshotAt: number;
  type: "info"|"success"|"error"|"warning";
  createdAt: number;
};

const ToastBox = ({
  toast,
  delay = 0,
  onClose = () => {},
}: {
  toast: TAdminToast;
  delay?: number;
  onClose?: (toastId: string) => any;
}) => {
  useEffect(() => {
    const tm = setTimeout(() => {
      onClose(toast.id);
    }, toast.duration);

    return () => {
      clearTimeout(tm);
    };
  }, [toast]);

  return (
    <motion.div
      initial={{ y: -90, opacity: 0 }}
      animate={{ y: 0, opacity: 1 }}
      transition={{
        delay: delay,
      }}
      exit={{ opacity: 0, y: -90 }}
      onClick={() => {
        onClose(toast.id);
      }}
      className={`rounded-lg bg-white dark:bg-gray-900 text-gray-800 dark:text-gray-50 p-3 w-40 cursor-pointer shadow-lg`}>
        <div className="flex flex-row gap-2">
          <div>
            { toast.type === "success" && <SuccessIcon className="text-green-500" /> }
            { toast.type === "info" && <InfoIcon className="text-blue-500" /> }
            { toast.type === "warning" && <WarningIcon className="text-yellow-500" /> }
            { toast.type === "error" && <ErrorIcon className="text-red-500" /> }
          </div>
          <div>
            <p className={`font-semibold text-sm underline underline-offset-2 decoration-2 ${
              toast.type === "info" ? "decoration-blue-400" :
              toast.type === "success" ? "decoration-green-400" :
              toast.type === "error" ? "decoration-red-400" :
              "decoration-yellow-400"
            } `}>
              {toast.title}
            </p>
            <p className="font-light text-xs">
              {toast.description}
            </p>
          </div>
        </div>
    </motion.div>
  );
};

export function AdminToast({
  toasts = [],
  onClose = () => {},
}: {
  toasts: TAdminToast[];
  onClose?: (toastId: string) => any;
}) {
  return (
    <AnimatePresence>
      {toasts.map((toast, index) => (
        <ToastBox
          key={toast.id}
          toast={toast}
          delay={index * 0.2}
          onClose={onClose}
        />
      ))}
    </AnimatePresence>
  );
}
