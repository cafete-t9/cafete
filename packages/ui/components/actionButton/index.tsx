import React from "react"

export const ActionButton = ({
  className = "",
  type = "button",
  children,
  ...props
}: React.HTMLProps<HTMLButtonElement>) => {
  return (
    <button
      className={`flex-shrink-0 px-4 py-2 text-sm font-semibold text-white bg-blue-600 rounded-2xl shadow-md hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 ${className}`}
      type={type as "button"|"submit"|"reset"}
      {...props}
      >
      {children}
    </button>
  )
}