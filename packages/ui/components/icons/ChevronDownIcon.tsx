import { SVGProps } from "react";

export function ChevronDownIcon(
  props: SVGProps<SVGSVGElement>,
) {
  const {
    fill = "none",
    viewBox = "0 0 24 24",
    stroke = "currentColor",
    strokeWidth = 2,
    className,
    ...restProps
  } = props;
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={`h-6 w-6 ${className ?? ""}`}
      fill={fill}
      viewBox={viewBox}
      stroke={stroke}
      strokeWidth={strokeWidth}
      {...restProps}>
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M19.5 8.25l-7.5 7.5-7.5-7.5"
      />
    </svg>
  );
}
