import { SVGProps } from "react";

export function ErrorIcon(props: SVGProps<SVGSVGElement>) {
  const {
    fill = "none",
    viewBox = "0 0 24 24",
    stroke = "currentColor",
    strokeWidth = 2,
    className,
    ...restProps
  } = props;
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={`h-6 w-6 ${className ?? ""}`}
      fill={fill}
      viewBox={viewBox}
      stroke={stroke}
      strokeWidth={strokeWidth}
      {...restProps}>
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M12 9v3.75m9-.75a9 9 0 11-18 0 9 9 0 0118 0zm-9 3.75h.008v.008H12v-.008z"
      />
    </svg>
  );
}
