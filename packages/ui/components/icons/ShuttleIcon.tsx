import { motion, SVGMotionProps } from "framer-motion"
import { SVGProps } from "react"

export function AnimatedShuttleIcon (props: SVGMotionProps<SVGSVGElement>) {
  const {
    fill = "none",
    viewBox = "0 0 34 16",
    stroke = "currentColor",
    strokeWidth = 0,
    className,
    ...restProps
  } = props

  return (
    <motion.svg
      xmlns="http://www.w3.org/2000/svg"
      className={`h-6 w-6 ${className ?? ""}`}
      fill={fill}
      viewBox={viewBox}
      stroke={stroke}
      strokeWidth={strokeWidth}
      {...restProps}
    >
      <g fill="currentColor" clipPath="url(#clip0_203_4)">
        <path d="M.054 7.962c2.732-3.167 5.697-3.338 6.839-3.028l-.108.543c-2.52.43-3.944 1.839-4.34 2.49 1.22 1.654 3.409 2.361 4.35 2.508l.002.543C3.58 11.36.962 9.123.054 7.962z"></path>
        <path
          fillRule="evenodd"
          d="M33.898 7.977c-.798-1.016-3.483-3.31-7.831-4.362-.471-.146-3.445-.279-11.57.357L11.392.869l-3.583.753 1.798 2.992-.324.65-1.249.378c-.36.652-.842 2.498.118 4.673l1.305.654.218.218-1.895 3.093 3.642.822 3.198-3.199c3.043.369 9.661.987 11.79.513 1.412-.25 4.886-1.49 7.487-4.44zM22.44 9.529a2.151 2.151 0 003.042.006c.839-.838.836-2.2-.006-3.042a2.151 2.151 0 00-3.042-.006c-.838.838-.836 2.2.006 3.042z"
          clipRule="evenodd"
        ></path>
      </g>
      <defs>
        <clipPath id="clip0_203_4">
          <path fill="currentColor" d="M0 0H34V16H0z"></path>
        </clipPath>
      </defs>
    </motion.svg>
  )
}

export function ShuttleIcon (props: SVGProps<SVGSVGElement>) {
  const {
    fill = "none",
    viewBox = "0 0 34 16",
    stroke = "currentColor",
    strokeWidth = 0,
    className,
    ...restProps
  } = props
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={`h-6 w-6 ${className ?? ""}`}
      fill={fill}
      viewBox={viewBox}
      stroke={stroke}
      strokeWidth={strokeWidth}
      {...restProps}
    >
      <g fill="currentColor" clipPath="url(#clip0_203_4)">
        <path d="M.054 7.962c2.732-3.167 5.697-3.338 6.839-3.028l-.108.543c-2.52.43-3.944 1.839-4.34 2.49 1.22 1.654 3.409 2.361 4.35 2.508l.002.543C3.58 11.36.962 9.123.054 7.962z"></path>
        <path
          fillRule="evenodd"
          d="M33.898 7.977c-.798-1.016-3.483-3.31-7.831-4.362-.471-.146-3.445-.279-11.57.357L11.392.869l-3.583.753 1.798 2.992-.324.65-1.249.378c-.36.652-.842 2.498.118 4.673l1.305.654.218.218-1.895 3.093 3.642.822 3.198-3.199c3.043.369 9.661.987 11.79.513 1.412-.25 4.886-1.49 7.487-4.44zM22.44 9.529a2.151 2.151 0 003.042.006c.839-.838.836-2.2-.006-3.042a2.151 2.151 0 00-3.042-.006c-.838.838-.836 2.2.006 3.042z"
          clipRule="evenodd"
        ></path>
      </g>
      <defs>
        <clipPath id="clip0_203_4">
          <path fill="currentColor" d="M0 0H34V16H0z"></path>
        </clipPath>
      </defs>
    </svg>
  )
}
