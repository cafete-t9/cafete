import { HTMLProps } from "react";

interface Props extends HTMLProps<HTMLHeadingElement> {
  actionLink?: {
    label: string
    href: string
  }
}

export function PageHeading ({
  className,
  children,
  actionLink,
  ...props
}: Props) {
  return (
    <h1 className={`text-3xl font-bold block ${actionLink ? "flex flex-row justify-between" : ""} ${className ?? ""}`} {...props}>
      {children}
      {
        actionLink && <a href={actionLink.href}>
          <button
            className="flex-shrink-0 px-4 py-2 text-sm font-semibold text-white bg-blue-600 rounded-2xl shadow-md hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500"
            type="button">
            {actionLink.label}
          </button>
        </a>
      }
    </h1>
  );
};
