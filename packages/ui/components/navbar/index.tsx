import { cloneElement, HTMLProps, isValidElement, ReactElement, useEffect, useState } from "react";

export function AdminNavbar({
  basePath = "/admin",
  currentRoute = "#NONE",
  children,
}: {
  basePath?: string;
  currentRoute?: string;
  children?: any;
}) {
  return (
    <div className="h-screen overflow-y-auto hidden lg:block sticky top-0 w-64">
      <div className="bg-white h-full dark:bg-gray-700 shadow-lg ">
        <div className="flex items-center justify-center pt-6"></div>
        <nav className="mt-6">
          <div>{children}</div>
        </nav>
      </div>
    </div>
  );
}

declare let window: any;

const getCurrentRoute = () => {
  if (typeof window !== "undefined") {
    return (window.location.pathname as string)
  }

  return null;
}

const isActiveRoute = (route: string|null, itemRoute: string) => {
  return route === itemRoute
}

export function AdminNavItem({
  className = "",
  href = "",
  children,
  ...props
}: HTMLProps<HTMLAnchorElement> & { activeRoute?: string; basePath?: string }) {
  return (
    <a className={`px-2 block`} href={href} {...props}>
      <div
        className={`w-full py-3 text-gray-500 dark:text-gray-200 flex items-center my-0 transition-colors duration-200 justify-start hover:bg-blue-500/40 rounded-xl ${className}`}>
        <span className="text-left mx-3 text-sm font-normal">
          {children}
        </span>
      </div>
    </a>
  );
}

export function AdminToolbar({
  user,
}: {
  user?: { username: string } | null;
}) {
  return (
    <header className="w-full bg-white dark:bg-gray-700 items-center sticky top-0 h-24">
      <div className="relative z-20 flex flex-col justify-center container h-full pr-3 mx-auto flex-center">
        <div className="relative items-center pl-1 flex w-full lg:max-w-68 sm:pr-2 sm:ml-0">
          <div className="relative p-1 flex items-center justify-start mr-4 sm:mr-2 left-auto">
            <a
              href="#"
              className="flex relative bg-black w-8 h-8 rounded-full text-gray-200">
              <span className="self-center mx-auto font-bold">
                {user?.username[0].toLocaleUpperCase()}
              </span>
            </a>
          </div>
          <div className="container relative left-0 z-50 flex w-3/4 h-auto">
            <div className="relative flex items-center w-full lg:w-64 h-full group">
              <div className="absolute z-50 flex items-center justify-center w-auto h-10 p-3 pr-2 text-sm text-gray-500 uppercase cursor-pointer sm:hidden">
                <svg
                  fill="none"
                  className="relative w-5 h-5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  stroke="currentColor"
                  viewBox="0 0 24 24">
                  <path d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                </svg>
              </div>
              <svg
                className="absolute left-0 z-20 hidden w-4 h-4 ml-4 text-gray-500 pointer-events-none fill-current group-hover:text-gray-400 sm:block"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20">
                <path d="M12.9 14.32a8 8 0 1 1 1.41-1.41l5.35 5.33-1.42 1.42-5.33-5.34zM8 14A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"></path>
              </svg>
              <input
                type="text"
                className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-gray-800 text-gray-400 aa-input"
                placeholder="Search"
              />
              <div className="absolute right-0 hidden h-auto px-2 py-1 mr-2 text-xs text-gray-400 border border-gray-300 rounded-2xl md:block">
                +
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}