import { TodoListIcon } from "../icons/PlayIcon";
import { RepeatIcon } from "../icons/RepeatIcon";
import { ActionButton } from "../actionButton";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { InputField, TextAreaField } from "../inputField";
import BoardTypeInput from "./boardTypeInput";

export function BoardCreation({}) {
  const { register, watch, setValue, handleSubmit } = useForm({
    defaultValues: {
      boardType: "PROJECT",
      title: "",
      description: "",
    },
  });
  const title = watch("title");
  const boardType = watch("boardType");

  return (
    <div className="w-full max-w-6xl">
      <section className="">
        <form onSubmit={handleSubmit(({ title, boardType, description }) => {
          console.log({ title, boardType })
        })} className="container max-w-2xl shadow-md md:w-3/4">
          <div className="p-4  border-t-2 border-blue-400 bg-sky-800 rounded-t-lg">
            <div className="w-full">
              <h1 className="text-gray-50">
                New Board{title ? `: ${title}` : ""}
              </h1>
            </div>
          </div>
          <div className="space-y-6 bg-white dark:bg-gray-700">
            <div className="items-center w-full p-4 space-y-4 text-gray-500 dark:text-gray-300 md:inline-flex md:space-y-0">
              <h2 className="max-w-sm mx-auto md:w-1/3">
                Board Info
              </h2>
              <div className="max-w-sm mx-auto md:w-2/3 space-y-2">
                <div className="relative">
                  <InputField
                    type="text"
                    className=" rounded-lg border-transparent flex-1 appearance-none w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"
                    placeholder="Board Title"
                    {...register("title")}
                  />
                </div>

                <div className="relative">
                  <TextAreaField
                    className=" rounded-lg border-transparent flex-1 appearance-none w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"
                    placeholder="Board Description"
                    rows={5}
                    {...register("description")}
                  />
                </div>
              </div>
            </div>
            <hr className="h-px bg-gray-200 border-0 dark:bg-gray-600" />

            <div className="items-center w-full p-4 space-y-4 text-gray-500 dark:text-gray-300 md:inline-flex md:space-y-0">
              <h2 className="max-w-sm mx-auto md:w-1/3">
                Board Type
              </h2>
              <div className="max-w-sm mx-auto md:w-2/3 space-y-2">
                <BoardTypeInput
                  value={boardType}
                  setValue={(next) =>
                    setValue("boardType", next)
                  }
                  name="boardType"
                  register={register}
                />
              </div>
            </div>

            <div className="w-full px-4 pb-4 ml-auto text-gray-500 md:w-1/3">
              <ActionButton
                className="w-full"
                type="submit">
                Create
              </ActionButton>
            </div>
          </div>
        </form>
      </section>
    </div>
  );
}
