import { useState } from "react";
import { RadioGroup } from "@headlessui/react";

type BoardType = {
  value: string;
  name: string;
  description: string;
  note: string;
  example: string;
};

const boardTypes: BoardType[] = [
  {
    value: "PROJECT",
    name: "Project",
    description:
      "Project Boards are useful if you want to setup a board for a single project.",
    note: "Project tasks are used once and archived when they are completed.",
    example:
      "You have a new project and want to manage it's development in a kanban format",
  },
  {
    value: "PROCESS",
    name: "Process",
    description:
      "Process Boards are useful if you have processes which happen periodically or are triggered by events.",
    note: "Process tasks are used repeatedly.",
    example:
      "You have a publishing process for your articles and want to manage the process.",
  },
];

export default function BoardTypeInput({
  name,
  setValue,
  value,
  register,
}: {
  setValue: (nx: string) => any;
  value: string;
  register: (...opts: any) => any;
  name: string;
}) {
  // const [selected, setSelected] = useState(boardTypes[0]);
  const selected = boardTypes.find(
    (b) => b.value === value,
  );

  return (
    <div className="w-full">
      <div className="mx-auto w-full max-w-md">
        <input type="hidden" {...register(name)} />
        <RadioGroup
          value={selected}
          onChange={(bType: BoardType) =>
            setValue(bType.value)
          }>
          <RadioGroup.Label className="sr-only">
            Board Type
          </RadioGroup.Label>
          <div className="space-y-2">
            {boardTypes.map((boardType) => (
              <RadioGroup.Option
                key={boardType.name}
                value={boardType}
                className={({ active, checked }) =>
                  `${active ? "" : ""}
                  ${
                    checked
                      ? "bg-sky-900 bg-opacity-75 text-white border border-blue-300"
                      : "bg-white dark:bg-gray-800 text-white border border-transparent"
                  }
                    relative flex cursor-pointer rounded-lg px-5 py-4 shadow-md focus:outline-none`
                }>
                {({ active, checked }) => (
                  <>
                    <div className="flex w-full items-center justify-between">
                      <div className="flex items-center">
                        <div className="text-sm">
                          <RadioGroup.Label
                            as="p"
                            className={`font-medium  ${
                              checked
                                ? "text-white"
                                : "text-gray-900 dark:text-white"
                            }`}>
                            {boardType.name}
                          </RadioGroup.Label>
                          <RadioGroup.Description
                            as="span"
                            className={`block pr-6 ${
                              checked
                                ? "text-sky-100"
                                : "text-gray-500"
                            }`}>
                            <p>{boardType.description}</p>
                            <p
                              className={`${
                                checked
                                  ? "text-sky-300"
                                  : "text-gray-300 dark:text-gray-600"
                              }`}>
                              {boardType.note}
                            </p>
                            <hr className="h-px mt-2 bg-gray-200 border-0 dark:bg-gray-600" />

                            <p
                              className={`mt-2 ${
                                checked
                                  ? "text-sky-300"
                                  : "text-gray-300 dark:text-gray-600"
                              }`}>
                              Example: {boardType.example}
                            </p>
                          </RadioGroup.Description>
                        </div>
                      </div>
                      <div className="shrink-0 text-white">
                        {checked ? (
                          <CheckIcon className="h-6 w-6" />
                        ) : (
                          <div className="w-6 h-6" />
                        )}
                      </div>
                    </div>
                  </>
                )}
              </RadioGroup.Option>
            ))}
          </div>
        </RadioGroup>
      </div>
    </div>
  );
}

function CheckIcon(props: any) {
  return (
    <svg viewBox="0 0 24 24" fill="none" {...props}>
      <circle
        cx={12}
        cy={12}
        r={12}
        fill="#fff"
        opacity="0.2"
      />
      <path
        d="M7 13l3 3 7-7"
        stroke="#fff"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}
