import { TableSchema } from "../../types";
import {
  flexRender,
  getCoreRowModel,
  getPaginationRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { useEffect, useMemo, useState } from "react";
import { motion } from "framer-motion";
import { useForm } from "react-hook-form";
import { AnimatedShuttleIcon } from "../icons/ShuttleIcon";
import { ActionButton } from "../actionButton";
import { PencilIcon } from "../icons/PencilIcon";

const defaultData = {
  items: [],
  page: 0,
  maxPage: 0,
  itemsPerPage: 20,
  sortBy: undefined,
};

declare const navigator: any | undefined;
function getLocale() {
  const nav =
    typeof navigator !== "undefined" ? navigator : null;
  return nav?.languages && nav?.languages.length
    ? nav?.languages[0]
    : nav?.language;
}

const locale = getLocale();

export function PaginatedTable<T extends TableSchema>({
  displaySchema,
  data = defaultData,
  showEdit = false,
  editRoute = "#",
  onPageSelect = () => {},
  showSearch = false,
  onSearch = (query: string) => {},
  idKey = "id",
}: TableProps<T>) {
  const [internalLoading, setInternalLoading] =
    useState(false);
  const schema = displaySchema;
  // const items = data?.items || [];
  const items = useMemo(() => {
    return (data?.items || []).map((item) => {
      return {
        ...item,
        editableURL: showEdit
          ? editRoute.replace(`[${idKey}]`, item[idKey])
          : null,
      };
    });
  }, [data.items, showEdit, idKey, editRoute]);

  const [{ pageIndex, pageSize }, setPagination] = useState(
    { pageIndex: data.page, pageSize: data.itemsPerPage },
  );
  const { handleSubmit, register } = useForm({
    defaultValues: {
      query: "",
    },
  });

  useEffect(() => {
    if (pageIndex !== data.page) {
      onPageSelect(pageIndex);
      setInternalLoading(true);
    } else {
      setInternalLoading(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageIndex, data.page]);

  const columns = [
    ...Object.keys(displaySchema),
    ...(showEdit ? ["editableURL"] : []),
  ];

  const table = useReactTable({
    data: items,
    columns: columns.map((col) => ({
      accessorKey: col,
      header: displaySchema[col]?.label
        ? displaySchema[col]?.label
        : col === "editableURL"
        ? ""
        : col,
      accessorFn: (row, key) => {
        if (col === "editableURL") return row[col];
        const renderAs = displaySchema[col]?.renderAs;

        switch (renderAs) {
          case "date":
            return new Intl.DateTimeFormat(locale, {
              day: "2-digit",
              month: "2-digit",
              year: "2-digit",
              hour: "numeric",
              minute: "numeric",
            }).format(new Date(row[col]));
          default:
            return row[col];
        }
      },
      cell: (({
        getValue,
        row: { index, ...row },
        column,
        table,
      }: any) => {
        if (column.id === "editableURL") {
          return (
            <a href={getValue()}>
              <ActionButton className="text-xs flex gap-2">
                <PencilIcon className="w-3 h-3 self-center text-white" />
                Edit
              </ActionButton>
            </a>
          );
        }
        if (
          displaySchema[column.id]?.renderAs === "pill"
        ) {
          if (displaySchema[column.id]?.type === "array") {
            return getValue().map((value: string) => (
              <span
                key={value}
                className="px-3 mr-2 py-1 text-xs rounded-lg bg-sky-800 border border-sky-600 text-gray-50">
                {value}
              </span>
            ));
          }
          return (
            <span className="px-3 py-1 text-xs rounded-lg bg-sky-800 border border-sky-600 text-gray-50">
              {getValue()}
            </span>
          );
        }
        return <span>{getValue()}</span>;
      }) as any,
      enableResizing: true,
    })),
    columnResizeMode: "onChange",
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    pageCount: data.maxPage,
    manualPagination: true,
    onPaginationChange: setPagination,
    state: {
      pagination: {
        pageIndex: data.page,
        pageSize: data.itemsPerPage,
      },
    },
  });

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className="max-w-8xl">
      <div className="py-8">
        <div className="flex flex-row mb-1 sm:mb-0 justify-between w-full">
          <div className="text-end">
            <form
              onSubmit={handleSubmit(({ query }) => {
                onSearch(query);
              })}
              className="flex flex-col md:flex-row w-3/4 md:w-full max-w-sm md:space-x-3 space-y-3 md:space-y-0 justify-center">
              {showSearch && (
                <div className="flex flex-row gap-6">
                  <div className=" relative ">
                    <input
                      type="text"
                      {...register("query")}
                      className=" rounded-2xl border-transparent flex-1 appearance-none w-full py-2 px-4 bg-white dark:bg-gray-900 text-gray-700 dark:text-gray-100 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"
                      placeholder="search..."
                    />
                  </div>
                  <button
                    className="flex-shrink-0 px-4 py-2 text-base font-semibold text-white bg-blue-600 rounded-2xl shadow-md hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500"
                    type="submit">
                    Filter
                  </button>
                </div>
              )}
            </form>
          </div>
        </div>
        <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
          <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
            <table
              style={{ width: table.getCenterTotalSize() }}
              className="min-w-full leading-normal">
              <thead>
                {table
                  .getHeaderGroups()
                  .map((headerGroup) => (
                    <tr key={headerGroup.id}>
                      {headerGroup.headers.map((header) => (
                        <th
                          scope="col"
                          key={header.id}
                          colSpan={header.colSpan}
                          style={{
                            width: header.getSize(),
                          }}
                          className="px-5 py-3 bg-white relative group dark:bg-gray-700 border-b border-gray-900 text-gray-800 dark:text-gray-300 font-bold text-left text-sm">
                          {header.isPlaceholder
                            ? null
                            : flexRender(
                                header.column.columnDef
                                  .header,
                                header.getContext(),
                              )}
                          <div
                            {...{
                              onMouseDown:
                                header.getResizeHandler(),
                              onTouchStart:
                                header.getResizeHandler(),
                              className: `absolute right-0 top-0 h-full w-1 bg-blue-700/40 opacity-0 group-hover:opacity-100 cursor-col-resize select-none touch-none ${
                                header.column.getIsResizing()
                                  ? "bg-blue-600 opacity-100"
                                  : ""
                              }`,
                            }}
                          />
                        </th>
                      ))}
                    </tr>
                  ))}
              </thead>
              <tbody>
                {table.getRowModel().rows.map((row) => (
                  <tr
                    className="border-b border-gray-200 dark:border-gray-600 bg-white hover:bg-gray-100 dark:bg-gray-700 dark:hover:bg-gray-600"
                    key={row.id}>
                    {row.getVisibleCells().map((cell) => (
                      <td
                        {...{
                          key: cell.id,
                          style: {
                            width: cell.column.getSize(),
                          },
                        }}
                        className="px-5 py-3 text-sm">
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext(),
                        )}
                      </td>
                    ))}
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="px-5 border-b border-gray-200 dark:border-gray-600 bg-white dark:bg-gray-700 py-5 flex flex-col xs:flex-row items-start xs:justify-between">
              <div className="flex items-center">
                <button
                  type="button"
                  disabled={!table.getCanPreviousPage()}
                  onClick={() => table.previousPage()}
                  className="w-full p-4 border flex text-base rounded-l-xl text-gray-600 disabled:text-gray-200 disabled:cursor-not-allowed bg-white hover:bg-gray-100 disabled:hover:bg-white dark:bg-gray-600 dark:hover:bg-gray-500 disabled:dark:hover:bg-gray-600 dark:text-gray-300 disabled:dark:text-gray-500 dark:border-none">
                  <svg
                    width={9}
                    fill="currentColor"
                    height={8}
                    viewBox="0 0 1792 1792"
                    className="mx-auto"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M1427 301l-531 531 531 531q19 19 19 45t-19 45l-166 166q-19 19-45 19t-45-19l-742-742q-19-19-19-45t19-45l742-742q19-19 45-19t45 19l166 166q19 19 19 45t-19 45z"></path>
                  </svg>
                </button>

                <button
                  type="button"
                  disabled={!table.getCanNextPage()}
                  onClick={() => table.nextPage()}
                  className="w-full p-4 border text-base rounded-r-xl text-gray-600 disabled:text-gray-200 disabled:cursor-not-allowed bg-white hover:bg-gray-100 disabled:hover:bg-white dark:bg-gray-600 dark:hover:bg-gray-500 disabled:dark:hover:bg-gray-600 dark:text-gray-300 disabled:dark:text-gray-500 dark:border-none">
                  <svg
                    width={9}
                    fill="currentColor"
                    height={8}
                    viewBox="0 0 1792 1792"
                    className="rotate-180 mx-auto"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M1427 301l-531 531 531 531q19 19 19 45t-19 45l-166 166q-19 19-45 19t-45-19l-742-742q-19-19-19-45t19-45l742-742q19-19 45-19t45 19l166 166q19 19 19 45t-19 45z"></path>
                  </svg>
                </button>
                <div className="px-2 inline-block w-32 overflow-hidden">
                  {internalLoading && (
                    <AnimatedShuttleIcon
                      initial={{ x: "-100%" }}
                      animate={{ x: "250%" }}
                      transition={{
                        repeatType: "loop",
                        duration: 2.3,
                        repeat: Infinity,
                      }}
                      className="text-gray-50 h-8 w-8"
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
}

type TableProps<T extends TableSchema> = {
  displaySchema: T;
  data?: {
    page: number;
    maxPage: number;
    itemsPerPage: number;
    sortBy?: {
      key: string;
      direction: "asc" | "desc";
    };
    items: any[];
  };
  onPageSelect?: (nextPage: number) => any;
  showSearch?: boolean;
  showEdit?: boolean;
  editRoute?: string;
  onSearch?: (query: string) => any;
  idKey?: string;
};
