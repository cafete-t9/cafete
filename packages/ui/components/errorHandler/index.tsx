export function ErrorHandler({ error }: { error: Error }) {
  if (error?.message?.trim() === "UNAUTHORIZED") {
    return <div className="w-full mt-7 bg-white dark:bg-slate-900 rounded-2xl p-6">
      <h2 className="text-red-400 font-bold text-xl">
        UNAUTHORIZED
      </h2>
      <pre className="mt-4 rounded-lg text-sm max-h-72 overflow-auto bg-black text-red-400 p-2">
        You are not authorized to perform this action.
      </pre>
    </div>
  }

  return (
    <div className="w-full mt-7 bg-white dark:bg-slate-900 rounded-2xl p-6">
      <h2 className="text-red-400 font-bold text-xl">
        Error: {
          error.name
        }
      </h2>
      <pre className="text-gray-800 dark:text-gray-100">{error.message}</pre>
      <pre className="mt-4 rounded-lg text-sm max-h-72 overflow-auto bg-black text-red-400 p-2">
        {error.stack}
      </pre>
    </div>
  )
}