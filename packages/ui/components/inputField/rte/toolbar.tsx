import lexComposerCtx from "@lexical/react/LexicalComposerContext";
// import { $getSelection, $isRangeSelection } from "lexical";
import lexical from "lexical"
import { useCallback, useRef, useState } from "react";
import lexicalList from "@lexical/list"
// import { $getNearestNodeOfType } from "@lexical/utils";
import lexicalUtils from "@lexical/utils";
// import { $isHeadingNode } from "@lexical/rich-text";
import lexicalRT from "@lexical/rich-text"

const { $getNearestNodeOfType } = lexicalUtils;
const { $isHeadingNode } = lexicalRT;
const { $getSelection, $isRangeSelection } = lexical;
const { $isListNode, ListNode } = lexicalList
const { useLexicalComposerContext } = lexComposerCtx

export function RTEToolbar({}) {
  const [editor] = useLexicalComposerContext();
  const toolbar = useRef<HTMLDivElement>(null);
  const [canUndo, setCanUndo] = useState(false);
  const [canRedo, setCanRedo] = useState(false);

  const [blockType, setBlockType] =
    useState<string>("paragraph");
  const [selectedElementKey, setSelectedElementKey] =
    useState<any | null>(null);
  const [isBold, setBold] = useState(false);
  const [isItalic, setItalic] = useState(false);
  const [isLink, setLink] = useState(false);
  const [isUnderline, setUnderline] = useState(false);

  const updateToolbar = useCallback(() => {
    const selection = $getSelection();
    if ($isRangeSelection(selection)) {
      const anchorNode = selection.anchor.getNode();
      const element =
        anchorNode.getKey() === "root"
          ? anchorNode
          : anchorNode.getTopLevelElementOrThrow();
      const elementKey = element.getKey();
      const elementDOM = editor.getElementByKey(elementKey);

      if (elementDOM !== null) {
        setSelectedElementKey(elementKey);
        if ($isListNode(element)) {
          const parentList = $getNearestNodeOfType(anchorNode, ListNode);
          const type = parentList ? parentList.getTag() : element.getTag();
          setBlockType(type);
        } else {
          const type = $isHeadingNode(element) ? element.getTag() : element.getType();
          setBlockType(type);
        }
      }
    }
  }, []);

  return (
    <div
      ref={toolbar}
      className="rounded-t-xl h-12 bg-gray-900 w-full sticky top-0"></div>
  );
}
