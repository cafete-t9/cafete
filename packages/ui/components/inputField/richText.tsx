// import { $getRoot, $getSelection } from "lexical";
import lexical from "lexical"
import { useEffect } from "react";

import lexComposer from "@lexical/react/LexicalComposer";
// import { PlainTextPlugin } from "@lexical/react/LexicalPlainTextPlugin";
import lexRTE from "@lexical/react/LexicalRichTextPlugin";
import lexContentEdt from "@lexical/react/LexicalContentEditable";
import lexHistory from "@lexical/react/LexicalHistoryPlugin";
import lexOnchange from "@lexical/react/LexicalOnChangePlugin";
import lexComposerCtx from "@lexical/react/LexicalComposerContext";
import { RTEToolbar } from "./rte/toolbar";

const { $getRoot, $getSelection } = lexical;
const { LexicalComposer } = lexComposer
const { RichTextPlugin }  = lexRTE
const { ContentEditable } = lexContentEdt
const { HistoryPlugin } = lexHistory
const { OnChangePlugin } = lexOnchange
const { useLexicalComposerContext } = lexComposerCtx

function onChange(editorState: any) {
  editorState.read(() => {
    // Read the contents of the EditorState here.
    const root = $getRoot();
    const selection = $getSelection();

    console.log(root, selection);
  });
}

export function RichTextEditor({
  placeholder = "Enter text...",
}: {
  placeholder?: string;
}) {
  const initialConfig = {
    namespace: "MyEditor",
    onError: (err: any) => {
      console.error(err);
    },
  };

  return (
    <div className="bg-gray-800 rounded-xl relative cursor-text col-span-3 group focus-within:outline-1 focus-within:outline-blue-600">
      <LexicalComposer initialConfig={initialConfig}>
        <RTEToolbar />
        <div className="relative mt-1 p-2">
          <RichTextPlugin
            contentEditable={<ContentEditable className="min-h-[90px] focus:outline-none" />}
            placeholder={<div className="absolute left-2 top-2 select-none pointer-events-none">{placeholder}</div>}
          />
        </div>
        <OnChangePlugin onChange={onChange} />
        <HistoryPlugin />
      </LexicalComposer>
    </div>
  );
}
