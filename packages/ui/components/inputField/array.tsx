import { Listbox, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import { ChevronDownIcon } from "../icons/ChevronDownIcon";

export function ArrayField({
  value,
  name,
  options = [],
  onChange = () => {},
}: {
  value: any[];
  name: string;
  options: { label: string; value: any }[];
  onChange?: (next: typeof value) => any;
}) {
  return (
    <div className="relative py-1 px-1 leading-normal rounded-xl focus-within:border-transparent focus-within:outline-none focus-within:ring-2 focus-within:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-gray-800 text-gray-400">
      <Listbox
        value={value}
        onChange={(valueList) => {
          console.log("valuelist", valueList);
          onChange(valueList);
        }}
        multiple>
        <Listbox.Button
          style={{ gridTemplateColumns: "repeat(auto-fill, 1fr)"}}
          className={`flex flex-row flex-wrap bg-transparent disabled:!bg-transparent focus:ring-0 focus:outline-none p-1 text-left`}>
            {value.map((v) => (
              <div
                key={v}
                // style={{ wordBreak: "keep-all" }}
                className="py-2 px-3 pr-6 rounded-xl bg-blue-700 mx-1 my-1 text-xs text-white">
                {options.find((o) => o.value === v)?.label}
              </div>
            ))}
            <ChevronDownIcon className="text-gray-300 h-5 w-5 absolute top-6 right-4" />
        </Listbox.Button>
        <Transition
          as={Fragment}
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0">
          <Listbox.Options className="z-[10] absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white dark:bg-gray-900 py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
            {options.map((option, optIdx) => (
              <Listbox.Option
                key={optIdx}
                className={({ active }) =>
                  `relative cursor-default select-none py-2 pl-10 pr-4 ${
                    active
                      ? "bg-blue-100 !text-blue-800 dark:bg-blue-900 dark:!text-blue-400"
                      : "text-gray-900 dark:text-gray-200"
                  }`
                }
                value={option.value}>
                {({ selected }) => (
                  <>
                    <span
                      className={`block truncate ${
                        selected
                          ? "font-medium text-blue-500"
                          : "font-normal"
                      }`}>
                      {option.label}
                    </span>
                  </>
                )}
              </Listbox.Option>
            ))}
          </Listbox.Options>
        </Transition>
      </Listbox>
    </div>
  );
}
