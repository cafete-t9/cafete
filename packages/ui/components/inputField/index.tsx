import React, { ForwardRefRenderFunction } from "react";

const FieldLabel: ForwardRefRenderFunction<HTMLLabelElement, React.HTMLProps<HTMLLabelElement>> = ({
  className = "",
  children,
  ...props
}, ref) => {
  return (
    <label ref={ref} className={`block mb-1 mt-2 ${className}`}>
      {children}
    </label>
  )
}

const Field: ForwardRefRenderFunction<HTMLInputElement, React.HTMLProps<HTMLInputElement>> = ({
  className = "",
  children,
  ...props
}, ref) => {
  return (
    <input
      ref={ref}
      className={`block w-full py-1.5 pl-4 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-gray-800 text-gray-400 disabled:!bg-transparent read-only:!bg-transparent read-only:focus:ring-0 ${className}`}
      {...props}
    />
  )
}

const Text: ForwardRefRenderFunction<HTMLTextAreaElement, React.HTMLProps<HTMLTextAreaElement>> = ({
  className = "",
  children,
  ...props
}, ref) => {
  return (
    <textarea
      ref={ref}
      className={`block w-full py-1.5 pl-4 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-gray-800 text-gray-400 ${className}`}
      {...props}
    />
  )
}

export const TextAreaField = React.forwardRef(Text)

export const InputField = React.forwardRef(Field)

export const InputFieldLabel = React.forwardRef(FieldLabel)