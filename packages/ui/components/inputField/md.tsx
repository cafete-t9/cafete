import {
  compile,
  compileSync,
  evaluateSync,
  runSync,
} from "@mdx-js/mdx";
import { createElement, Fragment } from "react";
import * as runtime from "react/jsx-runtime";


const code = compileSync("# Hello World", {})
const { default: Component } = runSync(code, runtime)
// const Component = evaluateSync("#Hello World", {
//   format: "mdx",
//   Fragment: "Fragment",
//   jsx: true,
//   jsxs: "React.createElement"
// });

export function MarkdownInput() {
  console.log("rendered");
  return (
    <div>
      <Component.default />
    </div>
  );
}
