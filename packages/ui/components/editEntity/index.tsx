import { useEffect, useMemo } from "react";
import {
  Controller,
  useFieldArray,
  useForm,
} from "react-hook-form";
import { EditSchema } from "../../types";
import { ActionButton } from "../actionButton";
import { InputField, InputFieldLabel } from "../inputField";
import { ArrayField } from "../inputField/array";
import { RichTextEditor } from "../inputField/richText";

const ArrayInput = ({
  control,
  name,
  disabled = false,
  register,
  options = [],
}: {
  control: any;
  name: string;
  disabled?: boolean;
  register: any;
  options?: { label: string; value: any }[];
}) => {
  const {
    fields,
    // append,
    // prepend,
    // remove,
    // swap,
    // move,
    // insert,
  } = useFieldArray({
    control,
    name,
  });

  return (
    <>
      {/* {fields.map((field, index) => ( */}
      <Controller
        render={(field) => {
          return (
            <ArrayField
              name={field.field.name}
              options={options!}
              value={field.field.value}
              onChange={(event) => {
                console.log("nextval", event)
                field.field.onChange(event);
              }}
            />
          );
        }}
        control={control}
        name={name}
      />
    </>
  );
};
// <InputField
//   key={field.id}
//   {...register(`${name}.${index}`, { disabled,  })}
// />
// ))}

export function EditEntity({
  schema,
  data,
  idField = "id",
  onSubmit = () => {},
}: {
  schema: EditSchema;
  data?: { [key in keyof typeof schema]: any } | null;
  idField?: keyof typeof schema;
  onSubmit?: (formData: {
    [key in keyof typeof schema]: any;
  }) => any;
}) {
  const fields = useMemo(
    () => Object.keys(schema),
    [schema, idField],
  );
  const { register, handleSubmit, setValue, control } =
    useForm({
      defaultValues: data || {},
    });

  return (
    <form
      className="mt-4 p-3 rounded-xl bg-gray-700 max-w-6xl w-full"
      onSubmit={handleSubmit(onSubmit)}>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-3">
        <input
          type="hidden"
          {...register(idField as string)}
        />
        {fields.map((field) => (
          <InputFieldLabel key={field} className={`${schema[field]?.type === "json" ? "col-span-3": ""}`}>
            <p className="ml-2">
              {schema[field]?.label ?? field}
            </p>
            {schema[field]?.type === "array" && (
              <ArrayInput
                register={register}
                control={control}
                name={field}
                disabled={schema[field]?.disabled}
                options={schema[field]?.options}
              />
            )}
            { (schema[field]?.type === "primitive" || !schema[field]?.type) && (
              <InputField
                readOnly={schema[field]?.disabled}
                {...register(field)}
              />
            )}
            {
              schema[field]?.type === "json" && (
                <RichTextEditor />
              )
            }
            <p className="text-xs text-gray-500 dark:text-gray-400 ml-4">{schema[field]?.description ?? ""}</p>
          </InputFieldLabel>
        ))}
      </div>
      <ActionButton
        type="submit"
        className="float-right mt-4">
        Save
      </ActionButton>
      <div className="clear-both" />
    </form>
  );
}
