/// <reference types="node" />
import type { SSRManifest } from 'astro';
import type { Options } from './types';
export declare function createExports(manifest: SSRManifest): {
    handler: (req: import("http").IncomingMessage, res: import("http").ServerResponse<import("http").IncomingMessage>, next?: ((err?: unknown) => void) | undefined) => Promise<void>;
};
export declare function start(manifest: SSRManifest, options: Options): void;
