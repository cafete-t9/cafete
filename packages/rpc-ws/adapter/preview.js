import { fileURLToPath } from "url";
import { createServer } from "./http-server.js";
const preview = async function({
  client,
  serverEntrypoint,
  host,
  port,
  base
}) {
  let ssrHandler;
  try {
    process.env.ASTRO_NODE_AUTOSTART = "disabled";
    const ssrModule = await import(serverEntrypoint.toString());
    if (typeof ssrModule.handler === "function") {
      ssrHandler = ssrModule.handler;
    } else {
      throw new Error(
        `The server entrypoint doesn't have a handler. Are you sure this is the right file?`
      );
    }
  } catch (_err) {
    throw new Error(
      `The server entrypoint ${fileURLToPath} does not exist. Have you ran a build yet?`
    );
  }
  const handler = (req, res) => {
    ssrHandler(req, res, (ssrErr) => {
      if (ssrErr) {
        res.writeHead(500);
        res.end(ssrErr.toString());
      } else {
        res.writeHead(404);
        res.end();
      }
    });
  };
  const baseWithoutTrailingSlash = base.endsWith("/") ? base.slice(0, base.length - 1) : base;
  function removeBase(pathname) {
    if (pathname.startsWith(base)) {
      return pathname.slice(baseWithoutTrailingSlash.length);
    }
    return pathname;
  }
  const server = createServer(
    {
      client,
      port,
      host,
      removeBase
    },
    handler
  );
  console.log(`Preview server listening on http://${host}:${port}`);
  return server;
};
export {
  preview as default
};
