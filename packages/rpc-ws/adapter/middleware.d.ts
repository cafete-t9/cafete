import type { NodeApp } from 'astro/app/node';
import type { IncomingMessage, ServerResponse } from 'http';
export default function (app: NodeApp): (req: IncomingMessage, res: ServerResponse, next?: ((err?: unknown) => void) | undefined) => Promise<void>;
