import type { NodeApp } from 'astro/app/node';
import type { Options } from './types';
export declare function getResolvedHostForHttpServer(host: string | boolean): string | undefined;
export default function startServer(app: NodeApp, options: Options): Promise<void>;
