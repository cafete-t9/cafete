function getAdapter(options) {
  return {
    name: "@astroshuttle/rpc-ws-adapter",
    serverEntrypoint: "@astroshuttle/rpc-ws/adapter/server.js",
    previewEntrypoint: "@astroshuttle/rpc-ws/adapter/preview.js",
    exports: ["handler", "router"],
    args: options
  };
}
function createIntegration(userOptions) {
  if (!(userOptions == null ? void 0 : userOptions.mode)) {
    throw new Error(`[@astroshuttle/rpc-ws-adapter] Setting the 'mode' option is required.`);
  }
  let needsBuildConfig = false;
  let _options;
  return {
    name: "@astroshuttle/rpc-ws-adapter",
    hooks: {
      "astro:config:setup": ({ updateConfig }) => {
        updateConfig({
          vite: {
            ssr: {
              noExternal: ["@astroshuttle/rpc-ws-adapter", "@astroshuttle/rpc-ws"]
            }
          }
        });
      },
      "astro:config:done": ({ setAdapter, config }) => {
        var _a, _b, _c;
        needsBuildConfig = !((_a = config.build) == null ? void 0 : _a.server);
        _options = {
          ...userOptions,
          client: (_b = config.build.client) == null ? void 0 : _b.toString(),
          server: (_c = config.build.server) == null ? void 0 : _c.toString(),
          host: config.server.host,
          port: config.server.port
        };
        setAdapter(getAdapter(_options));
        if (config.output === "static") {
          console.warn(`[@astroshuttle/node] \`output: "server"\` is required to use this adapter.`);
        }
      },
      "astro:build:start": ({ buildConfig }) => {
        if (needsBuildConfig) {
          _options.client = buildConfig.client.toString();
          _options.server = buildConfig.server.toString();
        }
      }
    }
  };
}
export {
  createIntegration as default,
  getAdapter
};
