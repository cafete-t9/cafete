import { polyfill } from "@astrojs/webapi";
import { NodeApp } from "astro/app/node";
import middleware from "./middleware.js";
import startServer from "./standalone.js";
polyfill(globalThis, {
  exclude: "window document"
});
function createExports(manifest, options) {
  const app = new NodeApp(manifest);
  return {
    handler: middleware(app),
    router: () => {
      return appRouter
    }
  };
}
function start(manifest, options) {
  if (options.mode !== "standalone" || process.env.ASTRO_NODE_AUTOSTART === "disabled") {
    return;
  }
  const app = new NodeApp(manifest);
  startServer(app, options);
}
export {
  createExports,
  start
};
