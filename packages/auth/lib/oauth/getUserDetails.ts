import openIdClient from "./client";
import { TokenSet } from "openid-client";
import { OAuthConfig } from "../../shuttleAuth/types";

const astroAuthURL = import.meta.env?.SHUTTLEAUTH_URL;

const getUserDetails = async (
  oauthConfig: OAuthConfig,
  code: string,
  cookies: {
    [key: string]: string;
  }
) => {
  const oauthClient = await openIdClient(oauthConfig);
  let userTokens: TokenSet;

  if (oauthConfig.idToken) {
    userTokens = await oauthClient.callback(
      `${astroAuthURL}/api/auth/oauth/${oauthConfig.id}`,
      { code: code },
      {
        code_verifier: cookies["__shuttleauth__pkce__"],
        state: cookies["__shuttleauth__state__"],
      }
    );
  } else {
    userTokens = await oauthClient.oauthCallback(
      `${astroAuthURL}/api/auth/oauth/${oauthConfig.id}`,
      { code: code },
      {
        code_verifier: cookies["__shuttleauth__pkce__"],
        state: cookies["__shuttleauth__state__"],
      }
    );
  }

  const user = await oauthClient.userinfo(userTokens.access_token ?? "", {
    params: oauthConfig.userInfoParams,
  });

  const transformedUser = {
    user: oauthConfig.profile(user),
    ...userTokens,
  };

  return transformedUser;
};

export default getUserDetails;