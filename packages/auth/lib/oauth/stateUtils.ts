import { generators } from "openid-client";
import { OAuthConfig } from "../../shuttleAuth/types";

export const getState = (config: OAuthConfig) => {
  if (!config.checks?.includes("state")) {
    return null;
  }

  const state = generators.state();

  return state;
};