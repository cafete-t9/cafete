import shuttleAuthHandler from "./handlers/index";
import { CredentialConfig, OAuthConfig } from "./types";

export interface ShuttleAuthParams {
  authProviders?: (OAuthConfig | CredentialConfig)[];
  hooks?: {
    jwt?: (user: any) => Promise<any>;
    signIn?: (user: any) => Promise<boolean | string>;
    redirectError?: (error: Error) => Promise<string>;
    account?: (user: any) => Promise<any>;
  };
}

interface AuthHandlerResponse {
  status?: number;
  body?: any;
  headers?: {
    [key: string]: string | undefined | string[];
  };
}

const ShuttleAuth = (astroAuthParams: ShuttleAuthParams) => {
  return async ({
    params: { shuttleauth },
    request,
  }: {
    params: {
      shuttleauth: string;
    };
    request: Request;
  }) => {
    const response: AuthHandlerResponse = (await shuttleAuthHandler(
      request,
      shuttleauth,
      astroAuthParams
    )) ?? {
      status: 500,
      body: {
        error: "Internal Server Error",
      },
    };

    if (!import.meta.env?.SHUTTLEAUTH_SECRET || !import.meta.env?.SHUTTLEAUTH_URL) {
      throw new Error("SHUTTLEAUTH_SECRET and SHUTTLEAUTH_URL must be set");
    }

    return new Response(JSON.stringify(response?.body), {
      status: response?.status || 200,
      headers: {
        "Content-Type": "application/json",
        ...response?.headers,
      },
    });
  };
};

export default ShuttleAuth;