import ShuttleAuth from "./auth";
import getUser, { redirectUser } from "./getUser";
import getError from "./getError";

export default ShuttleAuth;
export { getUser, getError, redirectUser };