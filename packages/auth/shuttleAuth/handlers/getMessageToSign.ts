const getMessageToSign = (message: string) => {
  return message ?? "Shuttle Auth works!";
};

export default getMessageToSign;