import parseCookie from "../../utils/parseCookieString";
import jwt from "jsonwebtoken";

const getServerUser = async (request: Request, user?: (user: any) => any) => {
  const cookies = parseCookie(request.headers.get("cookie") || "");
  const session = cookies["__shuttleauth__session__"];

  if (!session) {
    return {
      status: 401,
      body: {
        error: "Unauthorized",
      },
    };
  }

  const payload = jwt.verify(session, import.meta.env?.SHUTTLEAUTH_SECRET);

  const newPayload = user ? await user(payload) : payload;
  const newJWT = jwt.sign(newPayload, import.meta.env?.SHUTTLEAUTH_SECRET);

  const domain = import.meta.env?.SHUTTLEAUTH_DOMAIN
  return {
    status: 200,
    body: newPayload,
    headers: {
      "Set-Cookie": `__shuttleauth__session__=${newJWT};${domain ? " Domain=" + domain + ";" : ""} HttpOnly; Secure; Path=/;`,
    },
  };
};

export default getServerUser;