// import { MetamaskUserOptions } from "@astro-auth/types";
// import { AstroAuthParams } from "..";
import getURLSlash from "../../utils/getURLSlash";
import parseCookie from "../../utils/parseCookieString";
import { ShuttleAuthParams } from "../auth";
import getMessageToSign from "./getMessageToSign";
import getServerUser from "./getServerUser";
import OAuthCallback from "./oauthCallback";
import signIn from "./signIn";
import signOut from "./signout";

const astroAuthHandler = async (
  request: Request,
  url: string,
  config: ShuttleAuthParams,
) => {
  const cookies = parseCookie(
    request.headers.get("cookie") || "",
  );
  const requestBody: {
    provider: string;
    callback: string;
  } = await request
    .clone()
    .json()
    .catch(() => {});

  switch (url) {
    case "signin": {
      const authConfig = config.authProviders?.find(
        (provider) => provider.id === requestBody.provider,
      );

      return signIn(
        request,
        requestBody.callback,
        authConfig,
        config.hooks?.jwt,
        config.hooks?.redirectError,
      );
    }
    case "signout": {
      return signOut(request);
    }
    case "user": {
      return getServerUser(request, config.hooks?.account);
    }
    default: {
      if (url.startsWith("oauth")) {
        const oauthConfig = config.authProviders?.find(
          (provider) => provider.id === url.split("/")[1],
        );
        const code = new URL(request.url).searchParams.get(
          "code",
        );

        if (oauthConfig?.type == "credential") {
          return {
            status: 500,
          };
        }

        const { user, encodedJWT } = await OAuthCallback(
          request,
          oauthConfig,
          code ?? undefined,
          config.hooks?.jwt,
          config.hooks?.redirectError,
        );

        // This could be a boolean or a string
        const shouldUserLoginHookResponse = config.hooks
          ?.signIn
          ? await config.hooks?.signIn(user)
          : null;

        const shouldUserLogin = config.hooks?.signIn
          ? typeof shouldUserLoginHookResponse == "boolean"
            ? !!shouldUserLoginHookResponse
            : false
          : true;

        if (!shouldUserLogin) {
          return {
            status: 302,
            headers: {
              Location:
                typeof shouldUserLoginHookResponse ==
                "string"
                  ? shouldUserLoginHookResponse
                  : "/?error=Not Allowed",
              "Content-Type": undefined,
              "Set-Cookie":
                "__shuttleauth__session__=deleted; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT",
            },
          };
        }

        const domain = import.meta.env?.SHUTTLEAUTH_DOMAIN;

        return {
          status: 307,
          headers: {
            "Set-Cookie": `__shuttleauth__session__=${encodedJWT};${
              domain ? " Domain=" + domain + ";" : ""
            } HttpOnly; Secure; Path=/;`,
            "Content-Type": undefined,
            Location:
              cookies["__shuttleauth__callback__"] ?? "/",
          },
        };
      }
    }
  }
};

export default astroAuthHandler;
