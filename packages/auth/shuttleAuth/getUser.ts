import { AstroGlobal } from "astro";
import parseCookie from "../utils/parseCookieString";
import jwt from "jsonwebtoken";
import {
  applyWSSHandler,
  CreateWSSContextFnOptions,
} from "@trpc/server/adapters/ws";

const getUser = ({
  client,
  server,
  webSocket
}: {
  client?: AstroGlobal;
  server?: Request;
  webSocket?: CreateWSSContextFnOptions["req"]
}) => {
  if (client) {
    try {
      const sessionCookie = parseCookie(
        client.request.headers?.get?.("cookie") ?? ""
      )["__shuttleauth__session__"];

      if (!sessionCookie) {
        return null;
      }

      const decodedData = jwt.verify(
        sessionCookie,
        import.meta.env?.SHUTTLEAUTH_SECRET
      );

      return decodedData;
    } catch (error: any) {
      return null;
    }
  } else if (server) {
    try {
      const sessionCookie = parseCookie(
        server.headers?.get("cookie") ?? ""
      )["__shuttleauth__session__"];

      if (!sessionCookie) {
        return null;
      }

      const decodedData = jwt.verify(
        sessionCookie,
        import.meta.env?.SHUTTLEAUTH_SECRET
      );

      return decodedData;
    } catch (error: any) {
      return null;
    }
  } else if (webSocket) {
    const hasCookie = webSocket.headers.cookie
      const sessionCookie = parseCookie(
        hasCookie ?? ""
      )["__shuttleauth__session__"];

      if (!sessionCookie) {
        return null;
      }

      const decodedData = jwt.verify(
        sessionCookie,
        import.meta.env?.SHUTTLEAUTH_SECRET
      );

      return decodedData;
  }
};

export default getUser;

export const redirectUser = (loginPage: string) => {
  return new Response(null, {
    status: 302,
    headers: {
      Location: loginPage.startsWith("/") ? loginPage : `/${loginPage}`,
    },
  });
};