// import type { CredentialConfig } from "@astro-auth/types";
import { APIContext } from "astro";
import bcrypt from "bcryptjs";
import CredentialProvider from "./providers/CredentialProvider";
import ShuttleAuth, { getUser } from "./shuttleAuth";
import { CredentialConfig } from "./shuttleAuth/types";

const ssr = !!(import.meta as any).env?.SSR;

type SignUpOptions = {
  username: string;
  password: string;
  confirmPassword: string;
  signUp: true;
};

type SignInOptions = {
  username: string;
  password: string;
};

type AuthResult = Promise<
  | false
  | {
      user: {
        id: string;
        username: string;
        roles: string[];
      };
    }
  | undefined
>;

type SignupHandler = (
  options: SignUpOptions,
  db?: any,
) => AuthResult;
type SignInHandler = (
  options: SignInOptions,
  db?: any,
) => AuthResult;

type AuthorizeOptions = SignUpOptions & { email: string };

export const signUpHandler = async (
  {
    username,
    password,
    confirmPassword,
  }: Omit<SignUpOptions, "signUp">,
  db: any,
) => {
  if (password !== confirmPassword) return false;
  const hashedPassword = await bcrypt.hash(password, 10);
  try {
    const user = await db.user.create({
      data: {
        username,
        hashedPassword,
      },
    });
    delete user.hashedPassword
    return {
      user: user,
    };
  } catch (e) {
    console.error(e)
    if ((e.code = "P2002")) {
      return await signInHandler({ username, password }, db);
    }
    return false;
  }
};

export const signInHandler = async (
  {
    username,
    password,
  }: Pick<SignInOptions, "username" | "password">,
  db: any,
) => {
  const user = await db.user.findFirst({
    where: { username },
  });
  if (!user) return false;
  const match = await bcrypt.compare(
    password,
    user.hashedPassword,
  );

  if (!match) return false;

  delete user.hashedPassword
  
  return {
    user,
  };
};

export const AdminAuthProvider = ({
  onSignUp,
  onSignIn,
}: {
  onSignUp?: SignupHandler;
  onSignIn?: SignInHandler;
}): CredentialConfig =>
  CredentialProvider({
    async authorize({
      email,
      password,
      signUp,
      confirmPassword,
    }: AuthorizeOptions) {
      // bcrypt.hash()
      // if (ssr) {
        if (signUp) {
          return onSignUp(
            {
              username: email,
              password,
              confirmPassword,
              signUp: true,
            },
          );
        }

        return onSignIn({ username: email, password });
      // }
    },
  });

export const AuthModule = ({
  onSignUp,
  onSignIn,
}: {
  onSignUp?: SignupHandler;
  onSignIn?: SignInHandler;
}) =>
  ShuttleAuth({
    authProviders: [
      AdminAuthProvider({
        onSignUp,
        onSignIn,
      }),
    ],
    hooks: {
      async jwt(payload) {
        return {
          accessToken: payload.accessToken,
          user: payload.user,
        };
      },
      async account(config) {
        return config.user;
      },
    },
  });

export const isAuthorized = async ({
  role,
  ctx,
}: {
  role: string;
  ctx: APIContext;
}) => {
  const { user } = getUser({ server: ctx.request }) as {
    user: any;
  };

  return user && user.roles?.includes(role);
};

isAuthorized.rpc = async ({
  role,
  ctx,
}: {
  role: string;
  ctx: APIContext;
}) => {
  const { user } = getUser({ server: ctx.request }) as {
    user: any;
  };

  return user && user.roles?.includes(role);
};

export const getAuthenticatedUser = getUser;

export default ShuttleAuth;
