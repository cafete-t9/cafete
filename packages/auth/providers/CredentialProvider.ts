import { CredentialConfig, CredentialUserOptions } from "../shuttleAuth/types";

const CredentialProvider = (
  options: CredentialUserOptions
): CredentialConfig => {
  return {
    id: "credential",
    type: "credential",
    options,
  };
};

export default CredentialProvider;