import path from "path"
import { defineConfig as dfCfg } from "vite"


export default dfCfg({
  build: {
    lib: {
      entry: path.join(__dirname, "./index.ts"),
      formats: ["cjs", "es"],
      name: "socketRouter"
    },
    commonjsOptions: {
      transformMixedEsModules: true,
    },
    rollupOptions: {
      external: ["events", "node:events", "crypto", "buffer", "path", "jsonwebtoken", "jose", "@trpc/server", "@trpc/client", "superjson", "date-fns"]
    },
    emptyOutDir: true
  },
  legacy: { },
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
      "src": "/src/"
    }
  },
})