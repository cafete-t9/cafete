import { Providers } from "./types";

const signIn = async ({
  provider,
  login,
  callbackURL,
}: {
  provider: Providers;
  login?: any;
  callbackURL?: string;
}) => {
  if (provider == "credential" && !login) {
    throw new Error(
      "SHUTTLEAUTH: Login Details Are Required For The Credential Provider"
    );
  }

  let metamaskInfo: {
    address?: string;
    signature?: string;
  } = {};

  const response = await fetch("api/auth/signin", {
    method: "POST",
    body: JSON.stringify({
      provider,
      callback: callbackURL ?? location.href,
      login: login ?? metamaskInfo,
    }),
  });

  const data = await response.json();

  if (!response.ok) {
    return {
      error: data.error,
    };
  }

  if (window.location) {
    location.href = data.loginURL || data.redirect;
  }
  return data;
};

export default signIn;