
export const signIn = async ({
  username, password, redirectTo = "/",
}: { username: string, password: string, redirectTo?: string }) => {
  const response = await fetch("/api/auth/signin", {
    method: "POST",
    body: JSON.stringify({
      provider: "credential",
      callback: redirectTo ?? location.href,
      login: {
        email: username,
        password
      },
    }),
  });

  const data = await response.json();

  if (!response.ok) {
    return {
      failed: true,
      error: data.error,
    };
  }

  if (window.location) {
    location.href = data.loginURL || data.redirect;
  }

  return data;
}

export const signUp = async ({
  username, password, confirmPassword, redirectTo = "/",
}: { username: string, password: string, confirmPassword: string, redirectTo?: string }) => {
  const response = await fetch("/api/auth/signin", {
    method: "POST",
    body: JSON.stringify({
      provider: "credential",
      callback: redirectTo ?? location.href,
      login: {
        email: username,
        password,
        confirmPassword,
        signUp: true
      },
    }),
  });

  const data = await response.json();

  if (!response.ok) {
    return {
      failed: true,
      error: data.error,
    };
  }

  if (window.location) {
    location.href = data.loginURL || data.redirect;
  }

  return data;
}