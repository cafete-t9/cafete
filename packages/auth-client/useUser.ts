import axios from "redaxios";
import { atom } from "nanostores";

export const useUser = async () => {
  const user = await axios.get("/api/auth/user");
  return user.data;
};

const userAtom = atom<any>(null);

export const setUser = (newUser: any) => {
  userAtom.set(newUser);
};