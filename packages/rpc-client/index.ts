import {
  createTRPCProxyClient,
  createWSClient,
  httpBatchLink,
  TRPCClientError,
  wsLink,
} from "@trpc/client";
import type {
  inferProcedureInput,
  inferProcedureOutput,
  BuildProcedure,
  AnyRouter,
  DataTransformer,
} from "@trpc/server";
import useSWR, { mutate } from "swr";
import fetch from "isomorphic-fetch"

type PickByType<T, Value> = {
  [P in keyof T as T[P] extends Value | undefined
    ? P
    : never]: T[P];
};

export function generateRPCClient<
  AppRouter extends AnyRouter,
  SocketRouter extends AnyRouter,
>({
  url,
  transformer,
  socketURL,
}: {
  url: string;
  transformer: DataTransformer;
  socketURL?: string;
}) {
  // type RouterDef = inferRouterDef<AppRouter>

  const socketClient = socketURL && typeof window !== "undefined"
    ? createWSClient({
        url: socketURL,
      })
    : null;

  let links = [
    httpBatchLink({
      url,
      fetch: fetch,
    }),
  ];
  if (socketClient && typeof window !== "undefined") {
    links = [
      wsLink({
        client: socketClient,
      }),
    ]
  }

  const rawRpcClient = createTRPCProxyClient<AppRouter & SocketRouter>({
    transformer: transformer,
    links,
  });

  function subscribe<
    QueryKey extends keyof PickByType<
      SocketRouter["_def"]["procedures"],
      BuildProcedure<"subscription", any, any>
    >,
    ProcedureInput = inferProcedureInput<
      // @ts-expect-error
      SocketRouter[QueryKey]
    >,
    ProcedureOutput = inferProcedureOutput<
      SocketRouter[QueryKey]
    >,
  >(
    queryKey: QueryKey,
    onData: (data: ProcedureOutput) => any,
    ...[input]: undefined extends ProcedureInput
      ? [input?: ProcedureInput]
      : [input: ProcedureInput]
  ): { unsubscribe: () => any } {
    try {
        return (rawRpcClient[queryKey] as any).subscribe(input, {
          onData,
        })
    } catch(e) {
      console.error(e)
    }

    return { unsubscribe: () => {
      
    } }
  }

  async function runQueryAndThrow<
    QueryKey extends keyof PickByType<
      AppRouter["_def"]["procedures"],
      BuildProcedure<"query", any, any>
    >,
    ProcedureInput = inferProcedureInput<
      // @ts-expect-error
      AppRouter[QueryKey]
    >,
    ProcedureOutput = inferProcedureOutput<
      AppRouter[QueryKey]
    >,
  >(
    queryKey: QueryKey,
    // input: ProcedureInput,
    ...[input]: undefined extends ProcedureInput
      ? [input?: ProcedureInput]
      : [input: ProcedureInput]
  ): Promise<ProcedureOutput> {
    try {
      return (await (
        (rawRpcClient[queryKey] as any).query as any
      )(input)) as ProcedureOutput;
    } catch (e) {
      const err = e as TRPCClientError<AppRouter>;
      if (
        err.message === "UNAUTHORIZED" &&
        (import.meta as any).env?.SSR
      ) {
        setTimeout(
          () =>
            console.error(
              "---\n Did you call a protected client rpc-call on the server side? Protected calls should only be called on the client-side",
            ),
          10,
        );
      }
      throw e;
    }
  }

  async function runQuery<
    QueryKey extends keyof PickByType<
      AppRouter["_def"]["procedures"],
      BuildProcedure<"query", any, any>
    >,
    ProcedureInput = inferProcedureInput<
      //@ts-expect-error
      AppRouter[QueryKey]
    >,
    ProcedureOutput = inferProcedureOutput<
      AppRouter[QueryKey]
    >,
  >(
    queryKey: QueryKey,
    // input: ProcedureInput,
    ...[input]: undefined extends ProcedureInput
      ? [input?: ProcedureInput]
      : [input: ProcedureInput]
  ): Promise<{
    data: ProcedureOutput | null;
    error: TRPCClientError<AppRouter> | null;
  }> {
    try {
      return await runQueryAndThrow(queryKey, input);
    } catch (e: any) {
      return {
        data: null,
        error: e as TRPCClientError<AppRouter>,
      };
    }
  }

  function useQuery<
    QueryKey extends keyof PickByType<
      AppRouter["_def"]["procedures"],
      BuildProcedure<"query", any, any>
    >,
    ProcedureInput = inferProcedureInput<
      //@ts-expect-error
      AppRouter[QueryKey]
    >,
    ProcedureOutput = inferProcedureOutput<
      AppRouter[QueryKey]
    >,
  >(
    queryKey: QueryKey,
    ...rest: undefined | null extends ProcedureInput
      ? [
          input?: ProcedureInput | undefined | null,
          defaultValue?: ProcedureOutput,
        ]
      : [
          input: ProcedureInput,
          defaultValue?: ProcedureOutput,
        ]
  ) {
    const [input, defaultValue] = rest;
    // TODO: Find out why useSWR is not a callable type
    const { data, error, mutate } = (useSWR as any)(
      queryKey,
      async () => runQueryAndThrow(queryKey, input),
      { fallbackData: defaultValue },
    );

    const refresh = async (qry: ProcedureInput) => {
      const result = await runQueryAndThrow(queryKey, qry);

      mutate(result);

      return result;
    };

    return {
      data: data as ProcedureOutput | undefined,
      error: error as TRPCClientError<AppRouter>,
      isLoading: !error && !data,
      hasError: !!error,
      refresh,
    };
  }

  async function runMutation<
    MutationKey extends keyof PickByType<
      AppRouter["_def"]["procedures"],
      BuildProcedure<"mutation", any, any>
    >,
    ProcedureInput = inferProcedureInput<
      //@ts-expect-error
      AppRouter[QueryKey]
    >,
    ProcedureOutput = inferProcedureOutput<
      AppRouter[MutationKey]
    >,
  >(
    mutationKey: MutationKey,
    // input: ProcedureInput,
    ...[input]: undefined extends ProcedureInput
      ? [input?: ProcedureInput]
      : [input: ProcedureInput]
  ): Promise<{
    data: ProcedureOutput | null;
    error: TRPCClientError<AppRouter> | null;
  }> {
    try {
      return await runMutationAndThrow(mutationKey, input);
    } catch (e: any) {
      return {
        data: null,
        error: e as TRPCClientError<AppRouter>,
      };
    }
  }

  function useMutation<
    MutationKey extends keyof PickByType<
      AppRouter["_def"]["procedures"],
      BuildProcedure<"mutation", any, any>
    >,
    ProcedureInput = inferProcedureInput<
      //@ts-expect-error
      AppRouter[MutationKey]
    >,
    ProcedureOutput = inferProcedureOutput<
      AppRouter[MutationKey]
    >,
  >(mutationKey: MutationKey) {
    return async (input: ProcedureInput) =>
      mutate(mutationKey as string, async () =>
        runMutationAndThrow(mutationKey, input),
      ) as Promise<ProcedureOutput>;
  }

  async function runMutationAndThrow<
    MutationKey extends keyof PickByType<
      AppRouter["_def"]["procedures"],
      BuildProcedure<"mutation", any, any>
    >,
    ProcedureInput = inferProcedureInput<
      //@ts-expect-error
      AppRouter[QueryKey]
    >,
    ProcedureOutput = inferProcedureOutput<
      AppRouter[MutationKey]
    >,
  >(
    mutationKey: MutationKey,
    // input: ProcedureInput,
    ...[input]: undefined extends ProcedureInput
      ? [input?: ProcedureInput]
      : [input: ProcedureInput]
  ): Promise<ProcedureOutput> {
    return (await (
      (rawRpcClient[mutationKey] as any).mutate as any
    )(input)) as ProcedureOutput;
  }

  return {
    rawClient: rawRpcClient,
    useQuery,
    useMutation,
    runQuery,
    runMutation,
    runQueryAndThrow,
    runMutationAndThrow,
    subscribe
  };
}
