import { defineConfig } from "rollup"
import typescript from "@rollup/plugin-typescript"

const config = [
  defineConfig({
    input: "./src/rpc/client/index.ts",
    output: {
      file: "dist/rpc/client/index.js",
      format: "esm"
    },
    external: ["react"],
    plugins: [
      typescript({ jsx: "react" })
    ]
  }),
  // defineConfig({

  // })
]

export default config