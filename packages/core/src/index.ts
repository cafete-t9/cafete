import type { AstroIntegration } from "astro";
import { join } from "path";

const getEntryPoint = (pth: string) =>
  join(import.meta.url, "../", pth)
    .replace("file:\\", "")
    .replaceAll("\\", "/");

type AstroAdminOptions = {
  modules?: {
    tasks?: boolean;
  };
};

export const astroAdmin: ({
  modules: { tasks },
}?: AstroAdminOptions) => AstroIntegration = () => ({
  name: "AstroAdmin",
  hooks: {
    "astro:config:setup"({ injectRoute, config,  }) {
      // const loginPath = injectRoute({
      //   pattern: "/auth/login",
      //   entryPoint: getEntryPoint("auth/login.astro"),
      // });

      // config.vite.plugins.push(vcommonjs({}));

      // config.vite.build.rollupOptions.plugins.push(
      //   commonjs({}),
      // );

      config.vite.ssr.external.push(
        "bcryptjs",
        "ethers",
        "lexical",
        "@lexical/react",
        "@lexical/clipboard",
        "@lexical/react/LexicalComposer",
        "@lexical/react/LexicalPlainTextPlugin",
        "@lexical/react/LexicalContentEditable",
        "@lexical/react/LexicalHistoryPlugin",
        "@lexical/react/LexicalOnChangePlugin",
        "@lexical/react/LexicalComposerContext",
        "@lexical/react/LexicalRichTextPlugin",
        "@lexical/react/LexicalLinkPlugin",
        "@lexical/react/LexicalListPlugin",
        "@lexical/react/LexicalCheckListPlugin",
        "@lexical/react/LexicalAutoLinkPlugin",
        "@lexical/react/LexicalMarkdownShortcutPlugin",
        "@lexical/react/LexicalComposerContext",
        "@lexical/link",
        "@lexical/list",
        "@lexical/rich-text",
        "@lexical/selection",
        "@lexical/utils",
        "jsonwebtoken"
      )

      // config.vite.build.rollupOptions.plugins = [
      //   nodePolyfills(),
      //   ...config.vite.build.rollupOptions.plugins,
      // ];
      // config.vite.ssr.external = ["secure-password"]

      // TODO: https://github.com/withastro/astro/issues/3671
      // const apiPath = injectRoute({
      //   pattern: "/api/auth/[...astroauth]",
      //   entryPoint: getEntryPoint("auth/[...api].ts")
      // })
    },
  },
});

export default astroAdmin;
