import { createTRPCProxyClient, httpBatchLink } from '@trpc/client';
import useSWR, { mutate } from 'swr';

function generateRPCClient({ url }) {
    // type RouterDef = inferRouterDef<AppRouter>
    const rawRpcClient = createTRPCProxyClient({
        links: [
            httpBatchLink({
                url,
            }),
        ],
    });
    async function runQueryAndThrow(queryKey, 
    // input: ProcedureInput,
    ...[input]) {
        try {
            return (await rawRpcClient[queryKey].query(input));
        }
        catch (e) {
            const err = e;
            if (err.message === "UNAUTHORIZED" && import.meta.env.SSR) {
                setTimeout(() => console.error("---\n Did you call a protected client rpc-call on the server side? Protected calls should only be called on the client-side"), 10);
            }
            throw e;
        }
    }
    async function runQuery(queryKey, 
    // input: ProcedureInput,
    ...[input]) {
        try {
            return await runQueryAndThrow(queryKey, input);
        }
        catch (e) {
            return {
                data: null,
                error: e,
            };
        }
    }
    function useQuery(queryKey, ...rest) {
        const [input, defaultValue] = rest;
        // TODO: Find out why useSWR is not a callable type
        const { data, error } = useSWR(queryKey, async () => runQueryAndThrow(queryKey, input), { fallbackData: defaultValue });
        return {
            data: data,
            error: error,
            isLoading: !error && !data,
            hasError: !!error,
        };
    }
    async function runMutation(mutationKey, 
    // input: ProcedureInput,
    ...[input]) {
        try {
            return await runMutationAndThrow(mutationKey, input);
        }
        catch (e) {
            return {
                data: null,
                error: e,
            };
        }
    }
    function useMutation(mutationKey) {
        return async (input) => mutate(mutationKey, async () => runMutationAndThrow(mutationKey, input));
    }
    async function runMutationAndThrow(mutationKey, 
    // input: ProcedureInput,
    ...[input]) {
        return (await rawRpcClient[mutationKey].mutate(input));
    }
    return {
        rawClient: rawRpcClient,
        useQuery,
        useMutation,
        runQuery,
        runMutation,
        runQueryAndThrow,
        runMutationAndThrow,
    };
}

export { generateRPCClient };
