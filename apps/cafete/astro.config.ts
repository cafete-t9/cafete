import { defineConfig } from "astro/config";
import react from "@astrojs/react";
import tailwind from "@astrojs/tailwind";
import node from "@astrojs/node";
import astroAdmin from "@astroshuttle/core";
import { tRPCDevSocket } from "@astroshuttle/rpc-ws";
import path from "node:path";

// https://astro.build/config
const config =  defineConfig({
  integrations: [
    react(),
    tailwind(),
    astroAdmin({ modules: { tasks: true } }),
    tRPCDevSocket({
      createContextName: "socketContext",
      port: 8080,
      routerFilePath: path.resolve(
        __dirname,
        "src/backend/integrations/socketRouter.ts",
      ),
      routerName: "socketRouter",
    }),
  ],
  output: "server",
  adapter: node({
    mode: "middleware",
  }),
  outDir: "dist/main",
  vite: {
    ssr: {
      external: [],
      noExternal: []
    },
    build: {
      commonjsOptions: {
        transformMixedEsModules: true,
      },
    },
  },
});

export default config;