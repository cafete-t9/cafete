-- CreateEnum
CREATE TYPE "FlatrateType" AS ENUM ('COFFEE');

-- CreateTable
CREATE TABLE "FlatrateSubscription" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "expiresAt" TIMESTAMP(3) NOT NULL,
    "issuedById" TEXT NOT NULL,
    "type" "FlatrateType" NOT NULL DEFAULT 'COFFEE',

    CONSTRAINT "FlatrateSubscription_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "FlatrateSubscription" ADD CONSTRAINT "FlatrateSubscription_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "FlatrateSubscription" ADD CONSTRAINT "FlatrateSubscription_issuedById_fkey" FOREIGN KEY ("issuedById") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
