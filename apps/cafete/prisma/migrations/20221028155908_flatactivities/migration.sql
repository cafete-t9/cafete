-- CreateEnum
CREATE TYPE "FlatActivityType" AS ENUM ('NONE', 'PENALTY', 'PRODUCTBUY', 'BUY', 'EXPIRE', 'BAN');

-- CreateTable
CREATE TABLE "FlatrateActivity" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "issuedById" TEXT NOT NULL,
    "type" "FlatActivityType" NOT NULL DEFAULT 'NONE',
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,

    CONSTRAINT "FlatrateActivity_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "FlatrateActivity" ADD CONSTRAINT "FlatrateActivity_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "FlatrateActivity" ADD CONSTRAINT "FlatrateActivity_issuedById_fkey" FOREIGN KEY ("issuedById") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
