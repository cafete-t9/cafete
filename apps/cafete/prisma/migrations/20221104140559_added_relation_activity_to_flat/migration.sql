-- AlterTable
ALTER TABLE "FlatrateActivity" ADD COLUMN     "subscriptionId" TEXT;

-- AddForeignKey
ALTER TABLE "FlatrateActivity" ADD CONSTRAINT "FlatrateActivity_subscriptionId_fkey" FOREIGN KEY ("subscriptionId") REFERENCES "FlatrateSubscription"("id") ON DELETE SET NULL ON UPDATE CASCADE;
