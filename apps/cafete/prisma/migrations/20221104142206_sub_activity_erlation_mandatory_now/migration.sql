/*
  Warnings:

  - Made the column `subscriptionId` on table `FlatrateActivity` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "FlatrateActivity" DROP CONSTRAINT "FlatrateActivity_subscriptionId_fkey";

-- AlterTable
ALTER TABLE "FlatrateActivity" ALTER COLUMN "subscriptionId" SET NOT NULL;

-- AddForeignKey
ALTER TABLE "FlatrateActivity" ADD CONSTRAINT "FlatrateActivity_subscriptionId_fkey" FOREIGN KEY ("subscriptionId") REFERENCES "FlatrateSubscription"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
