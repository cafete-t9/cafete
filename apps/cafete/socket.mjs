import {
  PrismaClient,
} from "@prisma/client";
import { observable } from "@trpc/server/observable";
const backend = await import("./dist/exports/dist/server/chunks/backend.js")
const { appRouter, queryRouter, mutationRouter, userProcedure, getAuthenticatedUser, t, activityEvents } = backend

const db = new PrismaClient()

const router = appRouter ? appRouter : t.mergeRouters(
  queryRouter,
  mutationRouter,
  t.router({
    onNotification: userProcedure.subscription((req) => {
      // `resolve()` is triggered for each client when they start subscribing `onAdd`
      // return an `observable` with a callback which is triggered immediately
      return observable((emit) => {
        const onNotification = (data) => {
          // emit data to client
          emit.next(data);
        };
        const userEvents = activityEvents(
          req.ctx.user.id,
          onNotification,
        );

        const subscribe = async () => {
          await userEvents.subscribe();
        };

        subscribe();

        // unsubscribe function when client disconnects or stops subscribing
        return async () => {
          // ee.off('add', onAdd);
          await userEvents.unsubscribe();
        };
      });
    }),
  }),
);

export const socketContext = async ({ req }) => {
  const session = getAuthenticatedUser({ webSocket: req })

  if (session && session.user && session.user.id) {
    const actualUser = await db.user.findFirst({
      where: {
        id: session.user.id,
      },
      select: {
        email: true,
        id: true,
        roles: true,
        name: true,
        username: true,
      },
    });

    return {
      user: actualUser ? actualUser : undefined,
    };
  }
  return { user: undefined };
}

export const socketRouter = router