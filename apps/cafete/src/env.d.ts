/// <reference types="astro/client" />


interface ImportMetaEnv {
  readonly NODE_ENV?: string;
  readonly SITE_BASE: string;
  readonly SHUTTLEAUTH_URL: string;
  readonly SHUTTLEAUTH_SECRET: string;
  readonly DATABASE_URL: string;
  readonly REDIS_URL: string;
}