// import { appRouter } from 'server'
import type { APIContext } from "astro";
import { resolveHTTPResponse } from "@trpc/server/http";
// import { appRouter } from "src/backend";
import { getAuthenticatedUser } from "@astroshuttle/auth";
import type { User } from "@prisma/client";
import db from "src/db";
import { appRouter } from "src/backend";
import type { HTTPHeaders } from "@trpc/server/dist/http/internals/types";

/**
 * Handles trpc query client requests.
 *
 * @param {APIContext} - Astro API Context
 * @returns {Promise<Response>} - trpc response
 *
 * @beta
 */
async function httpHandler({
  request,
  params,
}: APIContext): Promise<Response> {
  const query = new URL(request.url).searchParams;

  const requestBody =
    request.method === "GET" ? {} : await request.json();

  const { status, headers, ...response } =
    await resolveHTTPResponse({
      async createContext() {
        const session = getAuthenticatedUser({
          server: request,
        }) as {
          user: Pick<User, "email" | "id" | "roles">;
        };

        if (session?.user?.id) {
          const actualUser = await db.user.findFirst({
            where: {
              id: session.user.id,
            },
            select: {
              email: true,
              id: true,
              roles: true,
              name: true,
              username: true,
            },
          });
          return {
            user: actualUser ? actualUser : undefined,
          };
        }
        return { user: undefined };
      },
      router: appRouter,
      path: params.trpc as string,
      req: {
        query,
        method: request.method,
        headers: request.headers as unknown as HTTPHeaders,
        body: requestBody,
      },
    });

  return new Response(response.body, {
    headers: headers as HeadersInit,
    status,
  });
}

export const post = httpHandler;

export const get = httpHandler;
