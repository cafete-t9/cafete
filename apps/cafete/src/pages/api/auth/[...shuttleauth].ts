import { authentication } from "src/backend/models/user";
import { AuthModule } from "@astroshuttle/auth";

export const all = AuthModule({
  onSignIn: authentication.handleSignIn,
  onSignUp: authentication.handleSignUp
});