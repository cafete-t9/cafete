import { runSubscription } from "src/backend/client";
import toast from "react-hot-toast";

if (typeof window !== undefined) {
  runSubscription(
    "onNotification",
    (data: any) => {
      const component = (
        <div onClick={() => {
          if (!data.href) {
            toast.remove(toastId)
          }
        }} className="">
          <p className="text-lg font-bold">{data.title}</p>
          <p className="text-xs">
            {data.description || "-"}
          </p>
        </div>
      );
      const toastId = toast.success(
        data.href ? <a href={data.href}>{component}</a> : component,
        { duration: 6000 },
      );
    },
    null,
  );
}

export function NotificationSystem() {
  return null;
}
