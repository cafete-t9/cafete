import { useMutation, useQuery } from "src/backend/client";
import {
  queryUserOutputSchema, uniqUsernameClient,
} from "src/backend/schemas/user";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { z } from "zod";

export function UserSettingsPage({
  user,
}: {
  user: z.infer<typeof queryUserOutputSchema>;
}) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      name: user!.name,
      username: user!.username
    },
    reValidateMode: "onBlur",
    resolver: zodResolver(
      z.object({
        username: uniqUsernameClient,
        name: z.string().nullable().optional(),
      }),
    ),
  });
  const { data } = useQuery("getProfile", null, user);

  const update = useMutation("updateProfile")

  return (
    <form
      onSubmit={handleSubmit(async (data, error) => {
        try {
          await update(data)
          toast.success("Updated successfully. You will be redirected in 3 seconds...")
          setTimeout(() => {
            // window.location = {...window.location, hostname: "/app"}
            window.location.href = "/app"
          }, 3000)
        } catch (e: any) {
          toast.error(e?.message || "Error updating.")
        }
      })}>
      <div className="flex flex-col gap-2 py-2">
        <label className="text-sm text-gray-800">
          <p className="font-semibold">name</p>
          <input
            {...register("name")}
            className="px-2 py-1 rounded-lg bg-gray-300 shadow-md border border-gray-400/60 w-full"
          />
          <p className="text-red-600">
            {errors.name ? errors.name.message : ""}
          </p>
        </label>
        <label className="text-sm text-gray-800">
          <p className="font-semibold">username</p>
          <input
            {...register("username")}
            className="px-2 py-1 rounded-lg bg-gray-300 shadow-md border border-gray-400/60 w-full"
          />
          <p className="text-red-600">
            {errors.username ? errors.username.message : ""}
          </p>
        </label>
      </div>

      <button className="w-full px-3 py-2 rounded-xl bg-green-600 text-white mt-3">
        update
      </button>
    </form>
  );
}
