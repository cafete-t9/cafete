import { runSubscription, useQuery } from "src/backend/client";
import type { FlatrateActivity, FlatrateSubscription } from "@prisma/client";
import { format } from "date-fns";
import { useEffect } from "react";
import toast from "react-hot-toast";

export function ActivityHistory({
  initialData,
}: {
  initialData?: {
    flat: FlatrateSubscription | null;
    history: FlatrateActivity[];
    coffeeBlock: FlatrateActivity | null;
  };
}) {

  const { data, error, refresh, isLoading } = useQuery(
    "getCurrentFlatData",
    null,
    initialData,
  );

  return (
    <div className="flex flex-col">
      {isLoading && <HistoryLoader />}
      {data?.history?.map((hs, hidx) => {
        return (
          <div
            key={hidx}
            className="grid grid-cols-8 gap-x-6 gap-y-4 px-4 even:bg-gray-50 py-2">
            <div className="flex flex-col col-span-2">
              <span className="font-semibold">
                {format(hs.createdAt, "dd.MM.yy")}
              </span>
              <span>{format(hs.createdAt, "HH:mm")}</span>
            </div>
            <div className="flex flex-col col-span-6">
              <span className="font-bold text-base">
                {hs.title}
              </span>
              <span className="text-xs text-gray-600">
                {hs.description}
              </span>
            </div>
          </div>
        );
      })}
    </div>
  );
}

const HistoryLoader = () => {
  return (
    <>
      <div className="grid grid-cols-8 gap-x-6 gap-y-4 px-2 even:bg-gray-200 py-2">
        <div className="flex flex-col col-span-2">
          <span className="font-semibold w-16 h-8 bg-gray-600 animate-pulse rounded-lg"></span>
          <span className="font-semibold w-16 h-8 bg-gray-300 animate-pulse rounded-lg"></span>
        </div>
        <div className="flex flex-col col-span-6">
          <span className="font-bold text-lg w-20 h-8 bg-gray-800 rounded-lg animate-pulse"></span>
          <span className="text-xs text-gray-600 w-30 h-8 rounded-lg bg-gray-400 animate-pulse"></span>
        </div>
      </div>
      <div className="grid grid-cols-8 gap-x-6 gap-y-4 px-2 even:bg-gray-200 py-2">
        <div className="flex flex-col col-span-2">
          <span className="font-semibold w-16 h-8 bg-gray-600 animate-pulse rounded-lg"></span>
          <span className="font-semibold w-16 h-8 bg-gray-300 animate-pulse rounded-lg"></span>
        </div>
        <div className="flex flex-col col-span-6">
          <span className="font-bold text-lg w-20 h-8 bg-gray-800 rounded-lg animate-pulse"></span>
          <span className="text-xs text-gray-600 w-30 h-8 rounded-lg bg-gray-400 animate-pulse"></span>
        </div>
      </div>
      <div className="grid grid-cols-8 gap-x-6 gap-y-4 px-2 even:bg-gray-200 py-2">
        <div className="flex flex-col col-span-2">
          <span className="font-semibold w-16 h-8 bg-gray-600 animate-pulse rounded-lg"></span>
          <span className="font-semibold w-16 h-8 bg-gray-300 animate-pulse rounded-lg"></span>
        </div>
        <div className="flex flex-col col-span-6">
          <span className="font-bold text-lg w-20 h-8 bg-gray-800 rounded-lg animate-pulse"></span>
          <span className="text-xs text-gray-600 w-30 h-8 rounded-lg bg-gray-400 animate-pulse"></span>
        </div>
      </div>
    </>
  );
};
