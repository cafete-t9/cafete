import { Toaster, useToasterStore } from "react-hot-toast"

export function ToasterContainer() {
  const { toasts, pausedAt } = useToasterStore();

  return (
    <>
      <Toaster position="top-center" reverseOrder />
    </>
  )
}