import { runSubscription, useMutation, useQuery } from "src/backend/client";
import type { Wallet } from "@prisma/client";
import { useEffect } from "react";
import toast from "react-hot-toast";

export function WalletPage({
  defaultValue,
  userName,
}: {
  defaultValue?: Wallet | null;
  userName?: string;
}) {
  const { data: wallet, refresh } = useQuery(
    "getWallet",
    null,
    defaultValue,
  );

  const createWallet = useMutation("createWallet");

  useEffect(() => {
    const sub = runSubscription("onNotification", (data: any) => {
      refresh(null)
    });

    return () => {
      sub.unsubscribe()
    }
  }, [refresh])

  return (
    <>
      <div className="flex bg-gradient-to-tr from-green-600 via-lime-600 to-green-900 text-gray-100 w-full h-52 p-4 relative">
        <div className="self-center">
          <h2 className="text-base font-light">
            Your balance
          </h2>
          <p className="text-3xl">
            {wallet?.balance || 0}{" "}
            <span className="text-base ml-2">T9C</span>
          </p>
        </div>
      </div>
      <div className="px-4">
        <h1 className="text-xl font-bold my-2">
          {userName}&apos;s wallet
        </h1>

        {!wallet && (
          <div className="p-3">
            <div className="rounded-xl w-full p-3 bg-white">
              <h2 className="text-xl font-bold my-2">
                You don&apos;t have a wallet yet!
              </h2>
              <p>
                To create a wallet, please press the button
                below.
              </p>

              <button
                type="button"
                onClick={async () => {
                  try {
                    const newWallet = await createWallet();
                    await refresh(null);
                    toast.success("Wallet created!");
                  } catch (e: any) {
                    toast.error(e.message);
                  }
                }}
                className="w-full rounded-xl p-2 text-center text-white bg-pink-700 active:bg-pink-500">
                Create Wallet
              </button>

              <p className="text-xs my-1 text-gray-600">Don&apos;t worry, creating a wallet is for free, just make sure you read the note below.</p>

            </div>
          </div>
        )}

        {wallet && <WalletActions wallet={wallet} />}
      </div>
    </>
  );
}

const WalletActions = ({ wallet }: { wallet: Wallet }) => {
  return (
    <div className="grid grid-cols-2 gap-4">
      <a href="/send">
        <button className="w-full p-3 bg-green-700 shadow-lg text-white rounded-xl">
          Send
        </button>
      </a>
      <a href="/receive">
        <button className="w-full p-3 bg-green-700 shadow-lg shadow-green-600/40 text-white rounded-xl">
          Receive
        </button>
      </a>
      <a href="/pay" className="col-span-2">
        <button className="w-full p-3 bg-pink-700 shadow-lg shadow-pink-600/40 text-white rounded-xl">
          Pay
        </button>
      </a>
    </div>
  );
};
