import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { signUp } from "@astroshuttle/auth-client";
import { z } from "zod";
import { uniqUsernameClient } from "src/backend/schemas/user";
import toast from "react-hot-toast";

const SignupSchema = z
  .object({
    username: uniqUsernameClient,
    password: z.string().min(1),
    confirmPassword: z.string().min(1),
  })
  .superRefine(({ password, confirmPassword }, ctx) => {
    if (confirmPassword !== password) {
      ctx.addIssue({
        code: "custom",
        message: "Passwords did not match.",
        path: ["confirmPassword"],
      });
    }
  });

export const SignupPage = ({
  redirectTo,
}: {
  redirectTo?: string;
}) => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<z.infer<typeof SignupSchema>>({
    resolver: zodResolver(SignupSchema),
  });

  const onSubmit = async (
    {
      username,
      password,
      confirmPassword,
    }: {
      username: string;
      password: string;
      confirmPassword: string;
    },
    errors: any,
  ) => {
    await signUp({
      username,
      password,
      confirmPassword,
      redirectTo,
    });
  };

  const onError = async (err: any) => {
    // console.log(err);
    toast.error(err.message)
  };

  return (
    <div className="bg-gradient-to-tr from-green-800 to-black min-h-screen py-6 sm:py-8 lg:py-12 flex text-gray-200">
      <div className="max-w-screen-2xl px-4 md:px-8 mx-auto self-center grow">
        <h2 className="text-gray-200 text-2xl lg:text-3xl font-bold text-center mb-4 md:mb-8">
          Sign Up
        </h2>

        <form
          onSubmit={handleSubmit(onSubmit, onError)}
          className="max-w-lg border border-gray-900 bg-gray-100 shadow-2xl rounded-lg mx-auto">
          <div className="flex flex-col gap-4 p-4 md:p-8">
            <div>
              <label
                htmlFor="username"
                className="inline-block text-gray-800 text-sm sm:text-base mb-2 w-full">
                Username
                <input
                  {...register("username")}
                  className="w-full bg-gray-900 text-gray-200 border focus:ring ring-indigo-900 rounded outline-none transition duration-100 px-3 py-2"
                />
                {errors.username && (
                  <span className="text-red-500 text-sm">
                    {errors?.username.message}
                  </span>
                )}
              </label>
            </div>

            <div>
              <label
                htmlFor="password"
                className="inline-block text-gray-800 text-sm sm:text-base mb-2 w-full">
                Password
                <input
                  {...register("password")}
                  type="password"
                  className="w-full bg-gray-900 text-gray-200 border focus:ring ring-indigo-900 rounded outline-none transition duration-100 px-3 py-2"
                />
                {errors.password && (
                  <span className="text-red-500 text-sm">
                    {errors?.password.message}
                  </span>
                )}
              </label>
            </div>

            <div>
              <label
                htmlFor="password"
                className="inline-block text-gray-800 text-sm sm:text-base mb-2 w-full">
                Repeat password
                <input
                  {...register("confirmPassword")}
                  type="password"
                  className="w-full bg-gray-900 text-gray-200 border focus:ring ring-indigo-900 rounded outline-none transition duration-100 px-3 py-2"
                />
                {errors.confirmPassword && (
                  <span className="text-red-500 text-sm">
                    {errors?.confirmPassword.message}
                  </span>
                )}
              </label>
            </div>

            <button
              type="submit"
              name="sign-up"
              className="block bg-green-800 hover:bg-green-700 active:bg-green-600 focus-visible:ring ring-gray-300 text-white text-sm md:text-base font-semibold text-center rounded-lg outline-none transition duration-100 px-8 py-3">
              Sign Up
            </button>
          </div>

          <div className="flex justify-center items-center bg-gray-100 p-4 rounded-b-lg">
            <p className="text-gray-500 text-sm text-center">
              Already have an account?{" "}
              <a
                href="/sign-in"
                className="text-indigo-500 hover:text-indigo-600 active:text-indigo-700 transition duration-100">
                Sign In
              </a>
            </p>
          </div>
        </form>
      </div>
    </div>
  );
};
