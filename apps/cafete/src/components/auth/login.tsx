import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { signIn } from "@astroshuttle/auth-client";
import { z } from "zod";
import { username } from "src/backend/schemas/user";
import toast from "react-hot-toast";

const LoginSchema = z.object({
  username,
  password: z.string().min(1),
});

export const LoginPage = ({
  redirectTo,
}: {
  redirectTo?: string;
}) => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<z.infer<typeof LoginSchema>>({
    resolver: zodResolver(LoginSchema),
  });

  const onSubmit = async (
    {
      username,
      password,
    }: { username: string; password: string },
    errors: any,
  ) => {
    // console.log("ERRORS", errors)t
    try {
      const response = await signIn({
        username,
        password,
        redirectTo,
      });
      if (response.failed) {
        throw new Error(response.error?.message)
      }
    } catch (e: any) {
      toast.error(e.message || "Wrong Credentials")
    }
  };

  const onError = async (err: any) => {
    toast.error(err.message)
  };

  return (
    <div className="bg-gradient-to-tr from-green-800 to-black min-h-screen py-6 sm:py-8 lg:py-12 flex text-gray-200">
      <div className="max-w-screen-2xl px-4 md:px-8 mx-auto self-center grow">
        <h2 className="text-gray-200 text-2xl lg:text-3xl font-bold text-center mb-4 md:mb-8">
          Sign In
        </h2>

        <form
          onSubmit={handleSubmit(onSubmit, onError)}
          className="max-w-lg border border-gray-900 bg-gray-100 shadow-2xl rounded-lg mx-auto">
          <div className="flex flex-col gap-4 p-4 md:p-8">
            <div>
              <label
                htmlFor="username"
                className="inline-block text-gray-800 text-sm sm:text-base mb-2 w-full">
                Username
                <input
                  {...register("username")}
                  className="w-full bg-gray-900 text-gray-200 border focus:ring ring-indigo-900 rounded outline-none transition duration-100 px-3 py-2"
                />
                {errors.username && (
                  <span className="text-red-500 text-sm">
                    {errors?.username?.message}
                  </span>
                )}
              </label>
            </div>

            <div>
              <label
                htmlFor="password"
                className="inline-block text-gray-800 text-sm sm:text-base mb-2 w-full">
                Password
                <input
                  {...register("password")}
                  type="password"
                  className="w-full bg-gray-900 text-gray-200 border focus:ring ring-indigo-900 rounded outline-none transition duration-100 px-3 py-2"
                />
                {errors.password && (
                  <span className="text-red-500 text-sm">
                    {errors?.password.message}
                  </span>
                )}
              </label>
            </div>

            <button
              type="submit"
              className="block bg-green-800 hover:bg-green-700 active:bg-green-600 focus-visible:ring ring-gray-300 text-white text-sm md:text-base font-semibold text-center rounded-lg outline-none transition duration-100 px-8 py-3">
              Log in
            </button>
          </div>

          <div className="flex justify-center items-center bg-gray-100 p-4 rounded-b-lg">
            <p className="text-gray-500 text-sm text-center">
              No account?{" "}
              <a
                href={`/sign-up${
                  redirectTo ? "?r=" + redirectTo : ""
                }`}
                className="text-indigo-500 hover:text-indigo-600 active:text-indigo-700 transition duration-100">
                Sign up
              </a>
            </p>
          </div>
        </form>
      </div>
    </div>
  );
};
