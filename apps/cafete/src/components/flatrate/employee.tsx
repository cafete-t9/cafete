import {
  runQuery,
  runSubscription,
  useMutation,
} from "src/backend/client";
import QrScanner from "qr-scanner";
import {
  Fragment,
  useEffect,
  useRef,
  useState,
} from "react";
import { Transition, Dialog } from "@headlessui/react";
import { motion } from "framer-motion";
import { z } from "zod";
import type { queryUserWithFlatSchema } from "src/backend/schemas/user";
import toast from "react-hot-toast";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";

export function FlatRateEmployeePage({
  user,
}: {
  user: {
    id: string;
    email?: string;
    username: string;
    roles: string[];
  };
}) {
  const videoRef = useRef<HTMLVideoElement | null>(null);
  const [isScanning, setScanning] = useState(false);
  const [scannerOutput, setOutput] = useState<any>(null);
  const [confirmActivation, setConfirmActivation] =
    useState(false);
  const [confirmCoffee, setConfirmCoffee] = useState(false);
  const [confirmTopUp, setConfirmTopUp] = useState(false);
  const [confirmCharge, setConfirmCharge] = useState(false);
  const activateCoffeeFlat = useMutation(
    "activateCoffeeFlat",
  );
  const chargeCoffee = useMutation("chargeCoffeeFromFlat");
  const [customer, setCustomer] = useState<z.infer<
    typeof queryUserWithFlatSchema
  > | null>(null);
  // ({
  //   user: {
  //     id: "d53f718f-61ae-4c46-b3b0-dc232698a740",
  //     username: "admin",
  //   },
  // });
  const callTopUp = useMutation("topUpCoins");

  useEffect(() => {
    if (videoRef && isScanning) {
      const qrScanner = new QrScanner(
        videoRef.current!,
        async (result) => {
          setOutput(result.data);
          setScanning(false);
          const cst = await runQuery("getUserWithFlat", {
            id: result.data,
          });
          setCustomer(cst);
        },
        {
          highlightScanRegion: true,
          highlightCodeOutline: true,
        },
      );

      qrScanner.start();

      return () => {
        qrScanner.destroy();
      };
    }
  }, [videoRef, isScanning]);

  return (
    <div className="container mx-auto lg:pt-10 max-w-xl relative">
      <div
        style={{ transform: "translate3d(0, 0, 0)" }}
        className={`w-64 h-64 rounded-full overflow-hidden mx-auto bg-gray-500 border-8 border-gray-500 ${
          isScanning
            ? "!border-red-500"
            : customer
            ? "!border-green-600 !bg-green-800 text-white"
            : ""
        } mt-4 relative`}>
        {customer && (
          <div className="absolute inset-0 flex text-6xl font-bold">
            <div className="self-center mx-auto">
              {customer.user?.username[0].toUpperCase()}
            </div>
          </div>
        )}
        <video
          ref={videoRef}
          className="w-full h-full object-cover mx-auto absolute top-0 left-0 self-center"
        />
      </div>
      <div className="sticky bottom-2 right-2 w-full my-2">
        <div className="w-full px-4">
          <button
            onClick={() => setScanning((o) => !o)}
            className={`${
              isScanning
                ? "bg-red-500 border-white border-2"
                : "bg-green-600 border-white border-2"
            } text-white px-4 py-4 rounded-xl font-semibold w-full`}>
            {isScanning ? "Stop" : "Start"} Scan
          </button>
        </div>
      </div>
      {customer && (
        <div className="px-4">
          <h1 className="text-center font-semibold text-xl">
            {customer?.user?.username}
          </h1>

          <h2 className="text-xl my-2">Actions</h2>

          <div className="grid grid-cols-2 gap-6">
            <div>
              <button
                onClick={() => setConfirmCoffee(true)}
                disabled={!!customer?.isBlocked}
                className="w-full py-8 px-4 rounded-xl text-center my-2 bg-green-600 hover:bg-green-500 active:bg-green-500 disabled:bg-gray-600 text-white">
                ☕ Flatrate Coffee
              </button>
              {customer?.isBlocked && (
                <p className="text-xs font-bold text-red-600">
                  User had a coffee recently and is
                  currently blocked from getting another.
                </p>
              )}
            </div>
            <button
              onClick={() => setConfirmActivation(true)}
              className="w-full py-8 px-4 rounded-xl my-2 bg-green-600 hover:bg-green-500 active:bg-green-500 text-white">
              🔁 Activate Flatrate
            </button>
            <button
              onClick={() => setConfirmCharge(true)}
              className="w-full py-10 px-4 rounded-xl my-2 bg-pink-600 hover:bg-pink-500 active:bg-pink-500 text-white">
              Charge T9C (Digital Pay)
            </button>
            <button
              onClick={() => setConfirmTopUp(true)}
              className="w-full py-10 px-4 rounded-xl my-2 bg-green-600 hover:bg-green-500 active:bg-green-500 text-white">
              + Add T9C Coins
            </button>
          </div>
        </div>
      )}

      <ConfirmationPopup
        show={confirmActivation}
        title={`User: ${
          customer?.user?.username || "UNDEFINED"
        }`}
        description="Please make sure the user paid before activating the flatrate!"
        confirmText="Yes, activate flat"
        onConfirm={async () => {
          // setCustomer(null);
          await activateCoffeeFlat({
            type: "COFFEE",
            userId: customer!.user!.id,
            days: 30,
          });

          toast.success(
            `Flat activated for ${
              customer!.user!.username
            }`,
          );

          setConfirmActivation(false);
          // setCustomer(null);
        }}
        onCancel={() => {
          setConfirmActivation(false);
        }}
      />

      <ConfirmationPopup
        show={confirmCoffee}
        title={`User: ${
          customer?.user?.username || "UNDEFINED"
        }`}
        description="You are going to charge a coffee on the flatrate and block the user from getting another for 90 minutes."
        confirmText="Yes, charge coffee"
        cancelText="Cancel"
        onConfirm={async () => {
          try {
            await chargeCoffee({
              userId: customer!.user!.id,
            });

            toast.success(
              `Coffee charged for ${
                customer!.user!.username
              }`,
            );

            setConfirmCoffee(false);
          } catch (e: any) {
            toast.error(
              `Error charging coffee: ${e.message}`,
            );
          }
        }}
        onCancel={() => {
          setConfirmCoffee(false);
        }}
      />

      {confirmCharge && (
        <ChargeRequest
          show={confirmCharge}
          title={`User: ${customer?.user?.username}`}
          description="Request an amount from the user for payments"
          cancelText="Cancel"
          userId={customer!.user!.id}
          onCancel={() => {
            setConfirmCharge(false);
          }}
          onConfirm={async ({
            amount,
          }: {
            amount: number;
          }) => {
            setConfirmCharge(false);
          }}
        />
      )}

      {confirmTopUp && (
        <TopUpCoins
          show={confirmTopUp}
          title={`User: ${
            customer?.user?.username || "UNDEFINED"
          }`}
          confirmText="Give user T9C"
          description="Add the amount to top up the users wallet balance."
          cancelText="Cancel"
          userName={customer!.user!.username}
          onCancel={() => {
            setConfirmTopUp(false);
          }}
          onConfirm={async ({
            amount,
          }: {
            amount: number;
          }) => {
            try {
              await callTopUp({
                amount,
                receiver: customer!.user!.username,
              });
            } catch (e: any) {
              toast.error(
                e?.message || "Unknown Error, call Ferhat",
              );
            }
            setConfirmTopUp(false);
          }}
        />
      )}
    </div>
  );
}

const ConfirmationPopup = ({
  title,
  description,
  show = false,
  onConfirm = () => {},
  onCancel = () => {},
  confirmText = "Yes",
  cancelText = "No",
}: {
  title: string;
  description: string;
  confirmText?: string;
  cancelText?: string;
  show?: boolean;
  onConfirm?: () => any;
  onCancel?: () => any;
}) => {
  return (
    <Transition appear show={show} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-[20]"
        onClose={() => {
          onCancel();
        }}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0">
          <div className="fixed inset-0 bg-black bg-opacity-25" />
        </Transition.Child>

        <div className="fixed bottom-0 rounded-t-xl w-full overflow-y-auto">
          <motion.div
            initial={{ translateY: 1500 }}
            animate={{ translateY: 0 }}
            className="flex min-h-full items-center justify-center text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95">
              <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-t-2xl bg-white p-6 pb-20 text-left align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  className="text-lg font-medium leading-6 text-gray-900">
                  {title}
                </Dialog.Title>
                <div className="mt-2">
                  <p className="text-sm text-gray-500">
                    {description}
                  </p>
                </div>

                <div className="flex flex-col gap-2 mt-6">
                  <button
                    type="button"
                    className="inline-flex w-full justify-center rounded-md border border-transparent bg-green-600 px-4 py-6 text-sm font-medium text-white hover:bg-green-500 focus:outline-none focus-visible:ring-2 focus-visible:ring-green-400 focus-visible:ring-offset-2"
                    onClick={onConfirm}>
                    {confirmText}
                  </button>

                  <div className="mt-4 w-full">
                    <button
                      type="button"
                      className="inline-flex w-full justify-center rounded-md border border-transparent bg-transparent px-4 py-2 text-sm font-medium text-blue-900 focus:outline-none "
                      onClick={onCancel}>
                      {cancelText}
                    </button>
                  </div>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </motion.div>
        </div>
      </Dialog>
    </Transition>
  );
};

const amountSchema = z
  .number({
    invalid_type_error: "Uhh.. I speak integer only.",
  })
  .int("You can't send fractional T9C")
  .min(1, "Send at least 1 T9C")
  .max(
    50000,
    "Dude, that's more than 500 Euros... Not possible!",
  );

const ChargeRequest = ({
  title,
  description,
  show = false,
  onConfirm = () => {},
  onCancel = () => {},
  confirmText = "Yes",
  userId,
  cancelText = "No",
}: {
  userId: string;
  title: string;
  description: string;
  confirmText?: string;
  cancelText?: string;
  show?: boolean;
  onConfirm?: ({ amount }: { amount: number }) => any;
  onCancel?: () => any;
}) => {
  const [step, setStep] = useState<
    "CHARGE" | "AWAIT" | "DONE" | "REJECTED"
  >("CHARGE");

  useEffect(() => {
    const subscription = runSubscription(
      "onTeamRequest",
      (data: any) => {
        toast.success(
          <div>
            <h2 className="text-xl font-bold">
              {data.title}
            </h2>
            <p>{data.description}</p>
          </div>,
        );
        onConfirm({ amount: 0 });
      },
    );

    const errorSubscription = runSubscription(
      "onChargeReject",
      (data: any) => {
        toast.error(
          <div>
            <h2 className="text-xl font-bold text-red-700">
              {data.title}
            </h2>
            <p>{data.description}</p>
          </div>,
        );
        setStep("REJECTED");
      },
    );

    return () => {
      subscription.unsubscribe();
      errorSubscription.unsubscribe();
    };
  }, [onConfirm]);

  const requestCharge = useMutation("requestCharge");

  return (
    <Transition appear show={show} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-[20]"
        onClose={() => {
          onCancel();
        }}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0">
          <div className="fixed inset-0 bg-black bg-opacity-25" />
        </Transition.Child>

        <div className="fixed bottom-0 rounded-t-xl w-full overflow-y-auto">
          <motion.div
            initial={{ translateY: 1500 }}
            animate={{ translateY: 0 }}
            className="flex min-h-full items-center justify-center text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95">
              <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-t-2xl bg-white p-6 pb-20 text-left align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  className="text-lg font-medium leading-6 text-gray-900">
                  {title}
                </Dialog.Title>
                {step === "CHARGE" && (
                  <ChargeRequestForm
                    onConfirm={async ({ amount }) => {
                      try {
                        console.log("requesting charge");
                        await requestCharge({
                          amount,
                          userId,
                        });
                        console.log("charge requested");
                        setStep("AWAIT");
                      } catch (e: any) {
                        toast.error(
                          `Error requesting charge: ${e.message}`,
                        );
                      }
                    }}
                    description={description}
                    onCancel={onCancel}
                    confirmText="Request Charge"
                    cancelText="Cancel"
                  />
                )}
                {step === "AWAIT" && (
                  <div className="my-6 w-full">
                    <div className="mx-auto">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="currentColor"
                        className="text-gray-600 animate-spin mx-auto w-20 h-20">
                        <path
                          fill="none"
                          d="M0 0h24v24H0z"></path>
                        <path
                          fill="currentColor"
                          d="M12 2a1 1 0 011 1v3a1 1 0 01-2 0V3a1 1 0 011-1zm0 15a1 1 0 011 1v3a1 1 0 01-2 0v-3a1 1 0 011-1zm10-5a1 1 0 01-1 1h-3a1 1 0 010-2h3a1 1 0 011 1zM7 12a1 1 0 01-1 1H3a1 1 0 010-2h3a1 1 0 011 1zm12.071 7.071a1 1 0 01-1.414 0l-2.121-2.121a1 1 0 011.414-1.414l2.121 2.12a1 1 0 010 1.415zM8.464 8.464a1 1 0 01-1.414 0l-2.12-2.12a1 1 0 011.414-1.415l2.12 2.121a1 1 0 010 1.414zM4.93 19.071a1 1 0 010-1.414l2.121-2.121a1 1 0 111.414 1.414l-2.12 2.121a1 1 0 01-1.415 0zM15.536 8.464a1 1 0 010-1.414l2.12-2.121a1 1 0 011.415 1.414L16.95 8.464a1 1 0 01-1.414 0z"></path>
                      </svg>
                      <p className="text-lg font-bold text-center">
                        Awaiting Payment..
                      </p>
                    </div>
                  </div>
                )}
                {step === "REJECTED" && (
                  <div className="my-6 w-full">
                    <div className="mx-auto">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="currentColor"
                        className="text-red-600 animate-bounce mx-auto w-20 h-20">
                        <path
                          fillRule="evenodd"
                          d="M12 2.25c-5.385 0-9.75 4.365-9.75 9.75s4.365 9.75 9.75 9.75 9.75-4.365 9.75-9.75S17.385 2.25 12 2.25zm-1.72 6.97a.75.75 0 10-1.06 1.06L10.94 12l-1.72 1.72a.75.75 0 101.06 1.06L12 13.06l1.72 1.72a.75.75 0 101.06-1.06L13.06 12l1.72-1.72a.75.75 0 10-1.06-1.06L12 10.94l-1.72-1.72z"
                          clipRule="evenodd"
                        />
                      </svg>

                      <p className="text-lg font-bold text-center">
                        Payment Rejected by user
                      </p>
                      <button
                        type="button"
                        onClick={() => setStep("CHARGE")}
                        className="w-full py-3 px-3 bg-green-600 text-white rounded-xl">
                        try again
                      </button>
                    </div>
                  </div>
                )}
              </Dialog.Panel>
            </Transition.Child>
          </motion.div>
        </div>
      </Dialog>
    </Transition>
  );
};

const ChargeRequestForm = ({
  description,
  onConfirm = () => {},
  onCancel = () => {},
  confirmText = "Yes",
  cancelText = "No",
}: {
  description: string;
  confirmText?: string;
  cancelText?: string;
  onConfirm?: ({ amount }: { amount: number }) => any;
  onCancel?: () => any;
}) => {
  const [isSending, setSending] = useState(false);
  const [canSend, setCanSend] = useState(true);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      amount: null,
    },
    resolver: zodResolver(
      z.object({
        amount: amountSchema,
      }),
    ),
  });
  return (
    <form
      onSubmit={handleSubmit(async ({ amount }) => {
        if (amount) {
          setSending(true);
          await onConfirm({ amount });
          setSending(false);
        } else {
          toast.error("Not a valid amount");
        }
      })}>
      <div className="mt-2">
        <p className="text-sm text-gray-500">
          {description}
        </p>
      </div>

      <div className="text-center font-semibold mt-2">
        Enter amount
      </div>
      <div className="flex items-center gap-2 mx-4">
        <input
          type="number"
          placeholder="0000"
          {...register("amount", {
            valueAsNumber: true,
          })}
          className="text-6xl font-semibold text-right w-[170px] rounded-lg py-2 placeholder:text-black/50 focus:outline-none bg-transparent text-green-700 mx-auto"
        />
        <p className="text-4xl text-gray-400 shrink">T9C</p>
      </div>
      <p className="text-red-600 font-bold text-center my-2">
        {errors.amount ? errors.amount.message : " \n "}
      </p>

      <div className="flex flex-col gap-2 mt-6">
        <button
          type="submit"
          disabled={isSending}
          tabIndex={2}
          className={`w-full ${
            canSend ? "" : "!bg-gradient-to-tr"
          } self-center rounded-xl font-semibold shadow-xl active:shadow-sm transition-all px-3 py-2 text-white bg-gradient-to-tr from-green-800 via-lime-600 to-green-700 active:from-green text-lg disabled:bg-none disabled:bg-gray-600`}>
          {!isSending && canSend && confirmText}
          {isSending && (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="currentColor"
              className="text-white animate-spin mx-auto">
              <path fill="none" d="M0 0h24v24H0z"></path>
              <path
                fill="currentColor"
                d="M12 2a1 1 0 011 1v3a1 1 0 01-2 0V3a1 1 0 011-1zm0 15a1 1 0 011 1v3a1 1 0 01-2 0v-3a1 1 0 011-1zm10-5a1 1 0 01-1 1h-3a1 1 0 010-2h3a1 1 0 011 1zM7 12a1 1 0 01-1 1H3a1 1 0 010-2h3a1 1 0 011 1zm12.071 7.071a1 1 0 01-1.414 0l-2.121-2.121a1 1 0 011.414-1.414l2.121 2.12a1 1 0 010 1.415zM8.464 8.464a1 1 0 01-1.414 0l-2.12-2.12a1 1 0 011.414-1.415l2.12 2.121a1 1 0 010 1.414zM4.93 19.071a1 1 0 010-1.414l2.121-2.121a1 1 0 111.414 1.414l-2.12 2.121a1 1 0 01-1.415 0zM15.536 8.464a1 1 0 010-1.414l2.12-2.121a1 1 0 011.415 1.414L16.95 8.464a1 1 0 01-1.414 0z"></path>
            </svg>
          )}
        </button>

        <div className="mt-4 w-full">
          <button
            type="button"
            className="inline-flex w-full justify-center rounded-md border border-transparent bg-transparent px-4 py-2 text-sm font-medium text-blue-900 focus:outline-none "
            onClick={onCancel}>
            {cancelText}
          </button>
        </div>
      </div>
    </form>
  );
};

const TopUpCoins = ({
  userName,
  title,
  description,
  show = false,
  onConfirm = () => {},
  onCancel = () => {},
  confirmText = "Yes",
  cancelText = "No",
}: {
  userName?: string;
  title: string;
  description: string;
  confirmText?: string;
  cancelText?: string;
  show?: boolean;
  onConfirm?: ({ amount }: { amount: number }) => any;
  onCancel?: () => any;
}) => {
  const [isSending, setSending] = useState(false);
  const [canSend, setCanSend] = useState(true);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      amount: null,
    },
    resolver: zodResolver(
      z.object({
        amount: amountSchema,
      }),
    ),
  });

  return (
    <Transition appear show={show} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-[20]"
        onClose={() => {
          onCancel();
        }}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0">
          <div className="fixed inset-0 bg-black bg-opacity-25" />
        </Transition.Child>

        <div className="fixed bottom-0 rounded-t-xl w-full overflow-y-auto">
          <motion.div
            initial={{ translateY: 1500 }}
            animate={{ translateY: 0 }}
            className="flex min-h-full items-center justify-center text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95">
              <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-t-2xl bg-white p-6 pb-20 text-left align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  className="text-lg font-medium leading-6 text-gray-900">
                  {title}
                </Dialog.Title>
                <form
                  onSubmit={handleSubmit(
                    async ({ amount }) => {
                      setSending(true);
                      await onConfirm({ amount });
                      setSending(false);
                    },
                  )}>
                  <div className="mt-2">
                    <p className="text-sm text-gray-500">
                      {description}
                    </p>
                  </div>

                  <div className="text-center font-semibold mt-2">
                    Enter amount
                  </div>
                  <div className="flex items-center gap-2 mx-4">
                    <input
                      type="number"
                      placeholder="0000"
                      {...register("amount", {
                        valueAsNumber: true,
                      })}
                      className="text-6xl font-semibold text-right w-[170px] rounded-lg py-2 placeholder:text-black/50 focus:outline-none bg-transparent text-green-700 mx-auto"
                    />
                    <p className="text-4xl text-gray-400 shrink">
                      T9C
                    </p>
                  </div>
                  <p className="text-red-600 font-bold text-center my-2">
                    {errors.amount
                      ? errors.amount.message
                      : " \n "}
                  </p>

                  <div className="flex flex-col gap-2 mt-6">
                    <button
                      type="submit"
                      disabled={isSending}
                      tabIndex={2}
                      className={`w-full ${
                        canSend ? "" : "!bg-gradient-to-tr"
                      } self-center rounded-xl font-semibold shadow-xl active:shadow-sm transition-all px-3 py-2 text-white bg-gradient-to-tr from-green-800 via-lime-600 to-green-700 active:from-green text-lg disabled:bg-none disabled:bg-gray-600`}>
                      {!isSending && canSend && confirmText}
                      {isSending && (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="currentColor"
                          className="text-white animate-spin mx-auto">
                          <path
                            fill="none"
                            d="M0 0h24v24H0z"></path>
                          <path
                            fill="currentColor"
                            d="M12 2a1 1 0 011 1v3a1 1 0 01-2 0V3a1 1 0 011-1zm0 15a1 1 0 011 1v3a1 1 0 01-2 0v-3a1 1 0 011-1zm10-5a1 1 0 01-1 1h-3a1 1 0 010-2h3a1 1 0 011 1zM7 12a1 1 0 01-1 1H3a1 1 0 010-2h3a1 1 0 011 1zm12.071 7.071a1 1 0 01-1.414 0l-2.121-2.121a1 1 0 011.414-1.414l2.121 2.12a1 1 0 010 1.415zM8.464 8.464a1 1 0 01-1.414 0l-2.12-2.12a1 1 0 011.414-1.415l2.12 2.121a1 1 0 010 1.414zM4.93 19.071a1 1 0 010-1.414l2.121-2.121a1 1 0 111.414 1.414l-2.12 2.121a1 1 0 01-1.415 0zM15.536 8.464a1 1 0 010-1.414l2.12-2.121a1 1 0 011.415 1.414L16.95 8.464a1 1 0 01-1.414 0z"></path>
                        </svg>
                      )}
                    </button>

                    <div className="mt-4 w-full">
                      <button
                        type="button"
                        className="inline-flex w-full justify-center rounded-md border border-transparent bg-transparent px-4 py-2 text-sm font-medium text-blue-900 focus:outline-none "
                        onClick={onCancel}>
                        {cancelText}
                      </button>
                    </div>
                  </div>
                </form>
              </Dialog.Panel>
            </Transition.Child>
          </motion.div>
        </div>
      </Dialog>
    </Transition>
  );
};
