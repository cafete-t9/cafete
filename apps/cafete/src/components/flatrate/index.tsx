import { useEffect, useState } from "react";
import qrcode from "qrcode";
import {
  runSubscription,
  useMutation,
  useQuery,
} from "src/backend/client";
import {
  add,
  differenceInDays,
  format,
  formatDistanceStrict,
  formatDuration,
  intervalToDuration,
} from "date-fns";
import { LoadingSpinner } from "../icons/LoadingSpinner";
import type {
  FlatrateActivity,
  FlatrateSubscription,
} from "@prisma/client";
import toast, { Toaster } from "react-hot-toast";

export function FlatRatePage({
  user,
  qrCode,
  initialData,
}: {
  user: {
    id: string;
    email?: string;
    username: string;
    roles: string[];
  };
  qrCode: string;
  initialData?: {
    flat: FlatrateSubscription | null;
    history: FlatrateActivity[];
    coffeeBlock: FlatrateActivity | null;
  };
}) {
  const [counter, setCounter] = useState(0);
  const { data, error, refresh, isLoading } = useQuery(
    "getCurrentFlatData",
    {},
    initialData,
  );

  const hasFlat =
    data?.flat?.expiresAt &&
    data.flat.expiresAt > new Date();

  const expireLeft = hasFlat
    ? intervalToDuration({
        start: new Date(),
        end: data.flat!.expiresAt,
      })
    : null;

  const expLeftStr = hasFlat
    ? formatDuration(expireLeft!, {
        delimiter: ", ",
        format: ["months", "days", "hours", "minutes"],
      })
    : "";

  useEffect(() => {
    const intv = setInterval(
      () => setCounter((o) => o + 1),
      60000,
    );

    let timerIntv = data?.coffeeBlock
      ? setInterval(() => {
          setCounter((o) => o + 1);
        }, 1000)
      : null;

    return () => {
      clearInterval(intv);
      if (timerIntv) {
        clearInterval(timerIntv);
      }
    };
  }, [data]);

  useEffect(() => {
    if (typeof window !== undefined) {
      const { unsubscribe } = runSubscription("onNotification", (data: any) => {
        refresh({})
      });

      return () => {
        unsubscribe()
      }
    }
  }, [refresh]);

  // useEffect(() => {
  //   const refreshFlatHistory = setInterval(async () => {
  //     const prevHistory = data?.history
  //     const nextData = await refresh({})

  //     if (nextData.history[0]?.id !== prevHistory?.[0]?.id) {
  //       const newActivity = nextData.history[0]

  //       toast.success(<div className="">
  //         <p className="text-xl font-bold">{newActivity.title}</p>
  //         <p className="text-xs">{newActivity.description}</p>
  //       </div>, { duration: 3000 })
  //     }
  //   }, 2000)

  //   return () => {
  //     clearInterval(refreshFlatHistory)
  //   }
  // }, [data, refresh])

  const isBlocked = !!data?.coffeeBlock;
  const blockLiftTime = data?.coffeeBlock?.createdAt
    ? add(data.coffeeBlock.createdAt, { minutes: 90 })
    : new Date();
  const blockInterval = intervalToDuration({
    start: new Date(),
    end: blockLiftTime,
  });

  const blockTimeLeftStr = `${blockInterval.hours
    ?.toString()
    .padStart(2, "0")}:${blockInterval.minutes
    ?.toString()
    .padStart(2, "0")}:${blockInterval.seconds
    ?.toString()
    .padStart(2, "0")}`;

  // console.log(data?.coffeeBlock?.createdAt, blockLiftTime)

  return (
    <div className="container mx-auto lg:pt-10 max-w-xl">
      <div className="lg:mt-10 lg:rounded-xl ">
        <div
          className={`h-72 w-full lg:rounded-lg rounded-b-none ${
            isBlocked
              ? "bg-yellow-600"
              : "bg-gradient-to-tr from-green-800 via-lime-600 to-green-900"
          }  shadow-black flex`}>
          <img
            src={qrCode}
            alt={user.username}
            className="h-52 w-52 rounded-md mx-auto self-center"
          />
        </div>
        <div className="mx-auto w-16 h-16 rounded-full relative -top-8 shadow-md">
          <div
            className={`w-20 h-20 ${
              isBlocked
                ? "bg-yellow-600"
                : "bg-gradient-to-tr from-green-800 via-lime-600 to-green-900"
            } absolute -left-2 -top-2 rounded-full`}></div>
          <div className="w-16 h-16 bg-gray-300 absolute left-0 top-0 rounded-full flex">
            <div className="mx-auto self-center font-bold text-4xl">
              {user.username[0]?.toUpperCase()}
            </div>
          </div>
        </div>
        {/* <div className="mx-auto w-16 h-16 bg-gray-300 rounded-full relative -top-8 shadow-md"></div> */}

        <div className="p-2 min-h-[60px] -mt-8 w-full">
          <div className="mx-auto text-center font-bold">
            {user?.username}
          </div>
          <div className="w-full text-center my-4">
            <button
              onClick={async () => {
                refresh({});
              }}
              className={`${
                hasFlat
                  ? isBlocked
                    ? "bg-yellow-600"
                    : "bg-green-600"
                  : "bg-gray-800"
              } text-white font-bold px-4 py-2 rounded-xl mx-auto`}>
              {isLoading && (
                <LoadingSpinner
                  className="text-white h-5 w-5 animate-spin"
                  strokeWidth={2}
                />
              )}
              {hasFlat
                ? isBlocked
                  ? blockTimeLeftStr
                  : "ACTIVE"
                : isLoading
                ? ""
                : "INACTIVE"}
            </button>
            {hasFlat && (
              <p className="text-xs text-gray-500">
                Expires in {expLeftStr}
              </p>
            )}
            {hasFlat && isBlocked && (
              <p className="text-xs text-orange-700">
                Enjoy your coffee! Come back in{" "}
                {blockTimeLeftStr} if you want another.
              </p>
            )}
            {/* {
              data?.coffeeBlock && (<p>BLOCKED: {data.coffeeBlock.createdAt?.toDateString()} </p>)
            } */}
          </div>
          <div className="">
            {/* <h2 className="text-3xl my-2 px-2">History</h2>
            <div className="flex flex-col">
              {isLoading && <HistoryLoader />}
              {data?.history?.map((hs, hidx) => {
                return (
                  <div
                    key={hidx}
                    className="grid grid-cols-8 gap-x-6 gap-y-4 px-2 even:bg-gray-200 py-2">
                    <div className="flex flex-col col-span-2">
                      <span className="font-semibold">
                        {format(hs.createdAt, "dd.MM.yy")}
                      </span>
                      <span>
                        {format(hs.createdAt, "HH:mm")}
                      </span>
                    </div>
                    <div className="flex flex-col col-span-6">
                      <span className="font-bold text-lg">
                        {hs.title}
                      </span>
                      <span className="text-xs text-gray-600">
                        {hs.description}
                      </span>
                    </div>
                  </div>
                );
              })}
            </div> */}
          </div>
        </div>
      </div>

      <Toaster />
    </div>
  );
}

const HistoryLoader = () => {
  return (
    <>
      <div className="grid grid-cols-8 gap-x-6 gap-y-4 px-2 even:bg-gray-200 py-2">
        <div className="flex flex-col col-span-2">
          <span className="font-semibold w-16 h-8 bg-gray-600 animate-pulse rounded-lg"></span>
          <span className="font-semibold w-16 h-8 bg-gray-300 animate-pulse rounded-lg"></span>
        </div>
        <div className="flex flex-col col-span-6">
          <span className="font-bold text-lg w-20 h-8 bg-gray-800 rounded-lg animate-pulse"></span>
          <span className="text-xs text-gray-600 w-30 h-8 rounded-lg bg-gray-400 animate-pulse"></span>
        </div>
      </div>
      <div className="grid grid-cols-8 gap-x-6 gap-y-4 px-2 even:bg-gray-200 py-2">
        <div className="flex flex-col col-span-2">
          <span className="font-semibold w-16 h-8 bg-gray-600 animate-pulse rounded-lg"></span>
          <span className="font-semibold w-16 h-8 bg-gray-300 animate-pulse rounded-lg"></span>
        </div>
        <div className="flex flex-col col-span-6">
          <span className="font-bold text-lg w-20 h-8 bg-gray-800 rounded-lg animate-pulse"></span>
          <span className="text-xs text-gray-600 w-30 h-8 rounded-lg bg-gray-400 animate-pulse"></span>
        </div>
      </div>
      <div className="grid grid-cols-8 gap-x-6 gap-y-4 px-2 even:bg-gray-200 py-2">
        <div className="flex flex-col col-span-2">
          <span className="font-semibold w-16 h-8 bg-gray-600 animate-pulse rounded-lg"></span>
          <span className="font-semibold w-16 h-8 bg-gray-300 animate-pulse rounded-lg"></span>
        </div>
        <div className="flex flex-col col-span-6">
          <span className="font-bold text-lg w-20 h-8 bg-gray-800 rounded-lg animate-pulse"></span>
          <span className="text-xs text-gray-600 w-30 h-8 rounded-lg bg-gray-400 animate-pulse"></span>
        </div>
      </div>
    </>
  );
};
