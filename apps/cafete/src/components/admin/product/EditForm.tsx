import { useMutation, useQuery } from "src/backend/client";
import { createProductSchema } from "src/backend/schemas/products";
import { zodResolver } from "@hookform/resolvers/zod";
import type { Product } from "@prisma/client";
import { Controller, useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { useState } from "react";
import { Combobox } from "@headlessui/react";
import type { z } from "zod";

export function EditProductForm({
  product,
}: {
  product: Product;
}) {
  const {
    data: { items: categories },
  } = useQuery(
    "getCategories",
    { where: {} },
    { items: [] },
  );
  const [query, setQuery] = useState("");
  const filteredCats =
    query === ""
      ? categories
      : categories.filter((cat: any) =>
          cat.title
            .toLowerCase()
            .replace(/\s+/g, "")
            .includes(
              query.toLowerCase().replace(/\s+/g, ""),
            ),
        );

  const {
    register,
    formState: { errors },
    handleSubmit,
    control
  } = useForm<z.infer<typeof createProductSchema>>({
    defaultValues: {
      ...product,
    },
    resolver: zodResolver(createProductSchema),
  });

  const updateProduct = useMutation("updateProduct");

  return (
    <form
      onSubmit={handleSubmit(
        async (data) => {
          console.log("submitting");
          try {
            await updateProduct({
              ...product,
              ...data,
            });
            toast.success("done");
            location.pathname = "/prices";
          } catch (e: any) {
            toast.error(e.message);
          }
        },
        (err) => {
          toast.error(JSON.stringify(err));
        },
      )}>
      <label className="text-sm text-gray-800">
        <p className="font-semibold">title</p>
        <input
          {...register("title")}
          className="px-2 py-1 rounded-lg bg-gray-300 shadow-md border border-gray-400/60 w-full"
        />
        <p className="text-red-600">
          {errors.title ? errors.title.message : ""}
        </p>
      </label>
      <label className="text-sm text-gray-800 flex my-2">
        {/* <p className="font-semibold"></p> */}
        <input
          {...register("available", {
            setValueAs(value) {
              return !!value;
            },
          })}
          type="checkbox"
          className="px-2 py-1"
        />
        <span className="self-center pl-2">
          show in price list
        </span>
        <p className="text-red-600">
          {errors.title ? errors.title.message : ""}
        </p>
      </label>

      <label className="text-sm text-gray-800">
        <p className="font-semibold">description</p>
        <textarea
          {...register("description")}
          rows={8}
          className="px-2 py-1 rounded-lg bg-gray-300 shadow-md border border-gray-400/60 w-full"
        />
        <p className="text-red-600">
          {errors.description
            ? errors.description.message
            : ""}
        </p>
      </label>

      <label className="text-sm text-gray-800">
        <p className="font-semibold">stock</p>
        <input
          {...register("stock", { valueAsNumber: true })}
          type="number"
          className="px-2 py-1 rounded-lg bg-gray-300 shadow-md border border-gray-400/60 w-full"
        />
        <p className="text-red-600">
          {errors.stock ? errors.stock.message : ""}
        </p>
      </label>

      <label className="text-sm text-gray-800">
        <p className="font-semibold">
          euroPrice (in cents)
        </p>
        <input
          {...register("euroPrice", {
            valueAsNumber: true,
          })}
          type="number"
          className="px-2 py-1 rounded-lg bg-gray-300 shadow-md border border-gray-400/60 w-full"
        />
        <p className="text-red-600">
          {errors.euroPrice ? errors.euroPrice.message : ""}
        </p>
      </label>

      <label className="text-sm text-gray-800">
        <p className="font-semibold">
          walletPrice (in cents)
        </p>
        <input
          {...register("walletPrice", {
            valueAsNumber: true,
          })}
          type="number"
          className="px-2 py-1 rounded-lg bg-gray-300 shadow-md border border-gray-400/60 w-full"
        />
        <p className="text-red-600">
          {errors.walletPrice
            ? errors.walletPrice.message
            : ""}
        </p>
      </label>

      <label>
        <p className="font-semibold text-sm text-gray-800">
          categories
        </p>
        <Controller
          control={control}
          name="categories"
          render={({ field, fieldState, formState }) => {
            return (
              <Combobox
                multiple
                value={field.value}
                onChange={(e) => {
                  field.onChange(e);
                }}>
                <div className="relative mt-1">
                  <div className="relative w-full cursor-default overflow-hidden rounded-lg bg-gray-300 border-gray-400/60 text-left shadow-md focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-teal-300 sm:text-sm">
                    <Combobox.Input
                      className="w-full border-none py-2 pl-3 pr-10 text-sm leading-5 bg-gray-300 border-gray-400/60 text-gray-900 focus:ring-0"
                      displayValue={(person: any) =>
                        person.title
                      }
                      onChange={(event) =>
                        setQuery(event.target.value)
                      }
                    />
                    <Combobox.Button className="absolute inset-y-0 right-0 flex items-center pr-2">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="h-5 w-5 text-gray-600"
                        aria-hidden="true">
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M8.25 15L12 18.75 15.75 15m-7.5-6L12 5.25 15.75 9"
                        />
                      </svg>
                    </Combobox.Button>
                  </div>
                  <Combobox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                    {filteredCats.length === 0 &&
                    query !== "" ? (
                      <div className="relative cursor-default select-none py-2 px-4 text-gray-700">
                        Nothing found.
                      </div>
                    ) : (
                      filteredCats.map((cat: any) => (
                        <Combobox.Option
                          key={cat.id}
                          className={({ active }) =>
                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                              active
                                ? "bg-teal-600 text-white"
                                : "text-gray-900"
                            }`
                          }
                          value={cat}>
                          {({ selected, active }) => (
                            <>
                              <span
                                className={`block truncate ${
                                  selected
                                    ? "font-medium"
                                    : "font-normal"
                                }`}>
                                {cat.title}
                              </span>
                              {selected ? (
                                <span
                                  className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                    active
                                      ? "text-white"
                                      : "text-teal-600"
                                  }`}>
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth={1.5}
                                    stroke="currentColor"
                                    className="w-5 h-5">
                                    <path
                                      strokeLinecap="round"
                                      strokeLinejoin="round"
                                      d="M4.5 12.75l6 6 9-13.5"
                                    />
                                  </svg>
                                </span>
                              ) : null}
                            </>
                          )}
                        </Combobox.Option>
                      ))
                    )}
                  </Combobox.Options>
                  {(field.value as unknown as any[])
                    ?.length > 0 && (
                    <ul className="flex flex-row flex-wrap gap-2 mt-2">
                      {(
                        field.value as unknown as any[]
                      )?.map((cat: any) => (
                        <li
                          key={cat.id}
                          className="px-2 py-1 bg-green-600 rounded-lg text-white text-xs">
                          {cat.title}
                        </li>
                      ))}
                    </ul>
                  )}
                </div>
              </Combobox>
            );
          }}
        />
      </label>

      <button
        type="submit"
        className="w-full bg-green-700 mt-3 rounded-xl text-white p-3">
        Update
      </button>
    </form>
  );
}
