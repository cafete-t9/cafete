import { useQuery } from "src/backend/client";
import {
  ErrorHandler,
  PaginatedTable,
} from "@astroshuttle/ui";
import type { PaginatedShape, TableSchema } from "@astroshuttle/ui/types";
import { useState } from "react";

type QueryParams = Parameters<typeof useQuery>;
type QueryKey = QueryParams[0];
type QueryInput<T extends QueryKey> = Parameters<
  typeof useQuery<T>
>[1];

const emptyQuery = {
  page: 0,
  itemsPerPage: 20,
  sortBy: {},
  where: {},
};

export function Table({
  displaySchema,
  queryKey,
  defaultQueryInput,
  searchField,
  editURL,
  idKey = "id",
}: {
  displaySchema: TableSchema;
  queryKey: QueryKey;
  defaultQueryInput?: QueryInput<typeof queryKey>;
  searchField?: keyof typeof displaySchema;
  editURL?: string;
  idKey?: string;
}) {
  const [queryInput, setQueryInput] = useState(
    (defaultQueryInput || emptyQuery) as typeof emptyQuery,
  );
  const { data, error, isLoading, refresh } = useQuery(
    queryKey,
    queryInput,
  );

  const onInputChange = async (
    nextInput: typeof queryInput,
  ) => {
    setQueryInput(nextInput);
    await refresh(nextInput);
  };

  if (isLoading) {
    return <TableLoader />;
  }

  if (error) {
    return <ErrorHandler error={error} />;
  }

  return (
    <PaginatedTable
      displaySchema={displaySchema}
      showSearch={!!searchField}
      data={data as unknown as PaginatedShape<any>}
      onPageSelect={(nextPage) => {
        onInputChange({
          ...queryInput,
          page: nextPage,
        });
      }}
      editRoute={editURL}
      showEdit={!!editURL}
      idKey={idKey}
      onSearch={(query) => {
        onInputChange({
          ...queryInput,
          where:
            query && query.length > 0
              ? {
                  [searchField as string]: {
                    contains: query,
                  },
                }
              : {},
        });
      }}
    />
  );
}

export function TableLoader() {
  return (
    <>
      <div className="flex flex-row mb-1 sm:mb-0 justify-between w-full text-end">
        <div className="mt-8 mb-4 flex flex-col md:flex-row w-3/4 md:w-full max-w-sm md:space-x-3 space-y-3 md:space-y-0 justify-start">
          <div className="relative">
            <div className="rounded-2xl animate-pulse border-transparent flex-1 h-10 w-60 appearance-none py-2 px-4 bg-white dark:bg-gray-900 text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" />
          </div>
          <div className="flex-shrink-0 animate-pulse px-4 flex py-2 h-10 text-base font-semibold text-white bg-blue-600 rounded-2xl shadow-md hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-purple-200">
            <div className="bg-white/40 h-2 self-center w-8 rounded-xl" />
          </div>
        </div>
      </div>
      <div className="animate-pulse w-full bg-gray-300 dark:bg-gray-600 rounded-2xl min-h-[500px]">
        <div className="w-full h-30 border-b border-b-gray-400 dark:border-b-gray-700 rounded-t-2xl" />
      </div>
    </>
  );
}
