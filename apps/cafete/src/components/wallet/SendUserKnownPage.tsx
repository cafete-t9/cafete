import { username } from "src/backend/schemas/user";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

const amountSchema = z
  .number({
    invalid_type_error: "Uhh.. I speak integer only.",
  })
  .int("You can't send fractional T9C")
  .min(1, "Send at least 1 T9C")
  

export function SendUserKnownPage({
  receiverName,
  maxValue = 1000,
}: {
  receiverName: string;
  maxValue: number;
}) {
  const {
    register,
    handleSubmit,
    setValue,
    watch,
    formState: { errors },
  } = useForm<{
    amount: number;
    receiver: string;
  }>({
    defaultValues: {
      amount: null as any,
      receiver: receiverName,
    },
    mode: "all",
    resolver: zodResolver(
      z.object({
        amount: amountSchema.max(
           1000,
          "A max. of 1000 (1k) T9C per transaction allowed.",
        ).max(maxValue, `You don't have more than ${maxValue} T9C to send.`),
        receiver: z.string().min(1),
      }),
    ),
  });

  const amt = watch("amount", 0);

  return (
    <div className="container mx-auto pt-10 max-w-xl relative px-10">
      <form onSubmit={handleSubmit((data) => {
        location.pathname = `/send/${receiverName}/${data.amount}`
      })} className="h-full flex flex-col">
        <div className="grow-0">
          <div className="text-center font-semibold">
            Enter amount
          </div>
          <div className="text-center text-gray-500 text-xs">
            Max: {Math.min(1000, maxValue)}
          </div>
          <div className="flex items-center gap-2 mx-4">
            <input
              type="number"
              placeholder="0000"
              {...register("amount", { valueAsNumber: true })}
              className="text-6xl font-semibold text-right w-[170px] rounded-lg py-2 placeholder:text-black/50 focus:outline-none bg-transparent text-green-700 mx-auto"
            />
            <p className="text-4xl text-gray-400 shrink">
              T9C
            </p>
          </div>
          <p className="text-red-600 font-bold text-center my-2">
            {errors.amount ? errors.amount.message : " \n "}
          </p>

          { !errors.amount && <div className="text-center mt-4 font-semibold">
            You are about to send{" "}
            <span className="text-green-700">{isNaN(amt) ? "-" : amt}</span> T9C
            to{" "}
            <span className="text-green-700">
              @{receiverName}
            </span>
            .
          </div> }
          {
            errors.amount && <div className="text-center mt-4 font-semibold">
              I can&apos;t send that.
            </div>
          }
        </div>
        <div className="my-10">
          <button disabled={!!errors.amount} type="submit" className="w-full self-center rounded-xl font-semibold shadow-xl active:shadow-sm transition-all px-3 py-2 text-gray-200 bg-gradient-to-tr from-green-800 via-lime-600 to-green-700 active:from-green text-lg disabled:!bg-none disabled:!bg-gray-600">
            Send now
          </button>
        </div>
      </form>
    </div>
  );
}
