import { runQuery, useMutation } from "src/backend/client";
import type { User } from "@prisma/client";
import QrScanner from "qr-scanner";
import {
  Fragment,
  useEffect,
  useRef,
  useState,
} from "react";
import { Transition, Dialog } from "@headlessui/react";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import toast from "react-hot-toast";

export function SendCoinPage({
  user,
  qrFail,
}: {
  user: {
    id: string;
    email?: string;
    username: string;
    roles: string[];
  };
  qrFail: boolean;
}) {
  const videoRef = useRef<HTMLVideoElement | null>(null);
  const [isScanning, setScanning] = useState(false);
  const [scannerOutput, setOutput] = useState<any>(null);

  const [receiver, setReceiver] = useState<string | null>(
    null,
  );

  const { register, formState, handleSubmit } = useForm({
    defaultValues: {
      username: null as any,
    },
    resolver: zodResolver(
      z.object({
        username: z
          .string()
          .min(1, "please provide a username"),
      }),
    ),
  });

  // const { register, handleSubmit } =

  useEffect(() => {
    if (videoRef && isScanning) {
      const qrScanner = new QrScanner(
        videoRef.current!,
        async (result) => {
          setOutput(result.data);
          setScanning(false);
          setReceiver(result.data);
        },
        {
          highlightCodeOutline: true,
          highlightScanRegion: true,
        },
      );

      qrScanner.start();

      return () => {
        qrScanner.destroy();
      };
    }
  }, [videoRef, isScanning]);

  useEffect(() => {
    if (qrFail) {
      toast.error(
        <div>
          <h2 className="text-lg font-bold">
            User not found
          </h2>
          <p className="text-gray-600 text-xs">
            I couldn&apos;t identify a user with the scanned
            QR Code. Please scan again or manually enter the
            username.
          </p>
        </div>,
      );
    }
  }, [qrFail]);

  useEffect(() => {
    if (scannerOutput) {
      location.pathname = `/send/${scannerOutput}`;
    }
  }, [scannerOutput]);

  return (
    <div className="container mx-auto lg:pt-10 max-w-xl relative">
      <div
        style={{ transform: "translate3d(0, 0, 0)" }}
        className={`w-64 h-64 rounded-full overflow-hidden mx-auto bg-gray-500 border-8 border-gray-500 ${
          isScanning
            ? "!border-red-500"
            : receiver
            ? "!border-green-600 !bg-green-800 text-white"
            : ""
        } mt-4 relative`}>
        {receiver && (
          <div className="absolute inset-0 flex text-lg text-center font-bold">
            <div className="self-center mx-auto">
              @{receiver}
            </div>
          </div>
        )}
        <video
          ref={videoRef}
          className="w-full h-full object-cover mx-auto absolute top-0 left-0 self-center"
        />
      </div>
      <div className="flex justify-between px-2 mt-1">
        <button
          type="button"
          onClick={() => setScanning((o) => !o)}
          className={`${
            isScanning ? "bg-red-500" : "bg-green-600"
          } text-white px-4 py-2 rounded-xl font-semibold self-center mx-auto`}>
          {isScanning ? "Stop" : "Start"} Scan
        </button>
      </div>
      <div className="text-center text-xs text-gray-700 my-2 px-4">
        To transfer T9C to another user, press &quot;Start
        Scan&quot; and scan the recipient&apos;s QR Code.
      </div>

      <div className="text-gray-300 inline-flex justify-center items-center w-full">
        <hr className="my-8 w-64 h-0.5 rounded-xl bg-gray-300 border-0" />
        <span className="absolute left-1/2 px-3 font-medium text-gray-900 bg-gray-200 -translate-x-1/2">
          or
        </span>
      </div>

      <form onSubmit={handleSubmit(({ username }: { username?: string }) => {
        location.pathname = `/send/${username?.replaceAll("@", "")}`;
      })}>
        <div className="text-xs text-center">enter username</div>
        <div className="flex justify-center">
          <input
            placeholder="@username"
            {...register("username")}
            className="px-3 py-2 bg-gray-400 rounded-l-xl placeholder:text-gray-600"
          />
          <button className="rounded-r-xl bg-green-700 active:bg-green-600 px-2 py-2 text-white">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6 self-center mx-auto">
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"
              />
            </svg>
          </button>
        </div>
      </form>

      {/* {receiver && (
        <TransferModal
          username={receiver}
          onClose={() => setReceiver(null)}
          onSend={() => setReceiver(null)}
        />
      )} */}
    </div>
  );
}
