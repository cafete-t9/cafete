import { useMutation } from "src/backend/client";
import { zodResolver } from "@hookform/resolvers/zod";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { z } from "zod";
import {
  motion,
  useAnimationControls,
} from "framer-motion";

export function SendFinalPage({
  amount,
  receiverName,
}: {
  amount: number;
  receiverName: string;
}) {
  const [isSending, setSending] = useState(false);
  const [canSend, setCanSend] = useState(true);
  const [success, setSuccess] = useState(false);
  const {
    register,
    watch,
    formState: { errors },
    handleSubmit,
  } = useForm({
    defaultValues: {
      comment: null as any,
    },
    resolver: zodResolver(
      z.object({
        comment: z
          .string()
          .max(
            150,
            "Please shorten your message to a max. of 150 characters.",
          )
          .optional()
          .nullable(),
      }),
    ),
  });
  const sendAmount = useMutation("transferCoins");
  const flyOut = useAnimationControls();
  const checkControl = useAnimationControls();

  const comment = watch("comment");

  useEffect(() => {
    if (success) {
      const run = async () => {
        await flyOut.start({
          opacity: 1,
          x: 0,
          y: 0,
          rotateZ: 0,
          transition: { duration: 1 },
        });
        await flyOut.start({
          opacity: 0,
          x: 300,
          y: -50,
          rotateZ: -45,
          transition: { duration: 1 },
        });

        await checkControl.start({
          opacity: 1,
          scale: 1,
        })
      };

      run();
    }
  }, [success, flyOut, checkControl]);

  return (
    <form
      onSubmit={handleSubmit(async (data) => {
        if (!canSend) {
          return (location.pathname = "/wallet");
        }
        setSending(true);
        try {
          await sendAmount({
            amount,
            receiver: receiverName,
            comment: data.comment,
          });
          setCanSend(false);
          setSuccess(true);
        } catch (e: any) {
          console.log("err", e);
          toast.error(
            <div className="">
              <p className="text-lg font-bold text-red-600">
                Failed to send
              </p>
              <p className="text-xs">{e.message}</p>
            </div>,
          );
        }
        setSending(false);
      })}
      className="">
      {!success && (
        <div className="container mx-auto pt-10 max-w-xl relative px-10">
          <h1 className="text-xl font-bold my-2 text-center">
            Review Transaction
          </h1>
          <hr className="my-2" />
          <div className="grid grid-cols-6 gap-2 font-semibold">
            <div className="col-span-2">You</div>
            <div className="col-span-2 text-center flex">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6 self-center mx-auto">
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"
                />
              </svg>
            </div>
            <div className="col-span-2 text-right text-white rounded-lg px-2 py-1 bg-green-700">
              {receiverName}
            </div>
          </div>
          <div className="grid grid-cols-6 gap-2">
            <div className="col-span-4">Cafete fee</div>
            <div className="col-span-2 text-right">
              0 T9C
            </div>
          </div>
          <div className="grid grid-cols-6 gap-2 font-bold">
            <div className="col-span-4">Total</div>
            <div className="col-span-2 text-right text-white rounded-lg px-2 py-1 bg-green-700">
              {amount} T9C
            </div>
          </div>
          <div className="px-6 text-center my-4">
            You are sending {amount} T9C to {receiverName}
          </div>

          <label className="my-4 w-full">
            <div className="w-full flex justify-between">
              <span className="text-xs">Comment</span>
              <span
                className={`text-xs ${
                  comment?.length > 150
                    ? "text-red-500 font-bold"
                    : ""
                }`}>
                {comment?.length || 0}/150
              </span>
            </div>
            <textarea
              tabIndex={1}
              {...register("comment")}
              className="w-full rounded-xl bg-gray-300 text-gray-800 p-3"
              rows={4}
            />
            <p className="text-red-600 font-semibold text-xs text-center my-2">
              {errors.comment
                ? (errors.comment.message as any)
                : " \n "}
            </p>
          </label>

          <div className="flex flex-col gap-3 mt-10">
            <button
              type="button"
              onClick={() => {
                if (
                  window.history.length > 1 &&
                  document.referrer.indexOf(
                    window.location.host,
                  ) !== -1
                ) {
                  window.history.back();
                } else {
                  location.pathname = `/send/${receiverName}`;
                }
              }}
              tabIndex={3}
              disabled={isSending}
              className={`bg-transparent ${
                isSending ? "text-gray-400" : ""
              }`}>
              Cancel
            </button>

            <button
              type="submit"
              disabled={isSending}
              tabIndex={2}
              className={`w-full ${
                canSend ? "" : "!bg-gradient-to-tr"
              } self-center rounded-xl font-semibold shadow-xl active:shadow-sm transition-all px-3 py-2 text-white bg-gradient-to-tr from-green-800 via-lime-600 to-green-700 active:from-green text-lg disabled:bg-none disabled:bg-gray-600`}>
              {!isSending && canSend && "Confirm and send"}
              {isSending && (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  className="text-white animate-spin mx-auto">
                  <path
                    fill="none"
                    d="M0 0h24v24H0z"></path>
                  <path
                    fill="currentColor"
                    d="M12 2a1 1 0 011 1v3a1 1 0 01-2 0V3a1 1 0 011-1zm0 15a1 1 0 011 1v3a1 1 0 01-2 0v-3a1 1 0 011-1zm10-5a1 1 0 01-1 1h-3a1 1 0 010-2h3a1 1 0 011 1zM7 12a1 1 0 01-1 1H3a1 1 0 010-2h3a1 1 0 011 1zm12.071 7.071a1 1 0 01-1.414 0l-2.121-2.121a1 1 0 011.414-1.414l2.121 2.12a1 1 0 010 1.415zM8.464 8.464a1 1 0 01-1.414 0l-2.12-2.12a1 1 0 011.414-1.415l2.12 2.121a1 1 0 010 1.414zM4.93 19.071a1 1 0 010-1.414l2.121-2.121a1 1 0 111.414 1.414l-2.12 2.121a1 1 0 01-1.415 0zM15.536 8.464a1 1 0 010-1.414l2.12-2.121a1 1 0 011.415 1.414L16.95 8.464a1 1 0 01-1.414 0z"></path>
                </svg>
              )}
              {!canSend && "back to wallet"}
            </button>
          </div>
        </div>
      )}

      {success && (
        <div className="container mx-auto pt-20 mt-10 max-w-xl relative overflow-hidden">
          <motion.svg
            xmlns="http://www.w3.org/2000/svg"
            fill="currentColor"
            initial={{
              opacity: 0,
              x: -300,
              y: 100,
              rotateZ: 45,
            }}
            animate={flyOut}
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="text-green-600 w-20 h-20">
            <path d="M3.478 2.405a.75.75 0 00-.926.94l2.432 7.905H13.5a.75.75 0 010 1.5H4.984l-2.432 7.905a.75.75 0 00.926.94 60.519 60.519 0 0018.445-8.986.75.75 0 000-1.218A60.517 60.517 0 003.478 2.405z" />
          </motion.svg>
          
          <div className="mx-auto flex flex-col items-center">
            <motion.svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
              initial={{
                opacity: 0,
                scale: 0,
              }}
              animate={checkControl}
              className="w-40 h-40 text-green-700">
              <path
                fillRule="evenodd"
                d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z"
                clipRule="evenodd"
              />
            </motion.svg>
          </div>
          <div className="px-4">
            <motion.h2 initial={{
                opacity: 0,
                scale: 0,
              }}
              animate={checkControl} className="text-center text-lg px-6">Success!</motion.h2>
            <a href="/wallet">
              <motion.button
                  type="button"
                  initial={{
                    opacity: 0,
                    scale: 0,
                  }}
                  animate={checkControl}
                  className={`w-full self-center rounded-xl font-semibold shadow-xl active:shadow-sm transition-all px-3 py-2 text-white bg-gradient-to-tr from-green-800 via-lime-600 to-green-700 active:from-green text-lg disabled:bg-none disabled:bg-gray-600`}>
                  back to wallet
                </motion.button>
            </a>
          </div>
        </div>
      )}
    </form>
  );
}
