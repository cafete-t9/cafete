import { useEffect, useState, Fragment } from "react";
import qrcode from "qrcode";
import { useMutation, useQuery } from "src/backend/client";
import {
  add,
  differenceInDays,
  format,
  formatDistanceStrict,
  formatDuration,
  intervalToDuration,
} from "date-fns";
import { LoadingSpinner } from "../icons/LoadingSpinner";
import type {
  FlatrateActivity,
  FlatrateSubscription,
} from "@prisma/client";
import toast, { Toaster } from "react-hot-toast"

export function WalletReceivePage({
  user,
  qrCode,
}: {
  user: {
    id: string;
    email?: string;
    username: string;
    roles: string[];
  };
  qrCode: string;
}) {
  const [counter, setCounter] = useState(0);

  return (
    <div className="container mx-auto lg:pt-10 max-w-xl">
      <div className="lg:mt-10 lg:rounded-xl ">
        <div
          className={`h-72 w-full lg:rounded-lg rounded-b-none ${
            false ? "bg-yellow-600" : "bg-gradient-to-tr from-green-800 via-lime-600 to-green-900"
          }  shadow-black flex`}>
          <img
            src={qrCode}
            alt={user.username}
            className="h-52 w-52 rounded-md mx-auto self-center"
          />
        </div>
        <div className="mx-auto w-16 h-16 rounded-full relative -top-8 shadow-md">
          <div
            className={`w-20 h-20 ${
              false ? "bg-yellow-600" : "bg-gradient-to-tr from-green-800 via-lime-600 to-green-900"
            } absolute -left-2 -top-2 rounded-full`}></div>
          <div className="w-16 h-16 bg-gray-300 absolute left-0 top-0 rounded-full flex">
            <div className="mx-auto self-center font-bold text-4xl">
              {user.username[0]?.toUpperCase()}
            </div>
          </div>
        </div>
        {/* <div className="mx-auto w-16 h-16 bg-gray-300 rounded-full relative -top-8 shadow-md"></div> */}

        <div className="p-2 min-h-[60px] -mt-8 w-full">
          <div className="mx-auto text-center font-bold">
            {user?.username}
          </div>
          
          <div className="">
          </div>
        </div>
      </div>
      
      
      <Toaster />
    </div>
  );
}
