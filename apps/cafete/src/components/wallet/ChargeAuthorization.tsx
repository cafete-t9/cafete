import { useEffect, useState } from "react";
import toast from "react-hot-toast";
import {
  runSubscription,
  useMutation,
} from "src/backend/client";
import { motion, AnimatePresence } from "framer-motion";

export function ChargeAuthorization({
  user,
}: {
  user: {
    roles: string[];
  };
}) {
  useEffect(() => {
    const subscription = runSubscription(
      "onChargeRequest",
      (data: any) => {
        const not = toast.custom(
          <AuthorizeModal
            amount={parseInt(data.description)}
            requestId={data.data.requestId}
            onClose={() => {
              toast.remove(not);
            }}
          />,
          {
            duration: Infinity,
          },
        );
      },
    );

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  return null;
}

const AuthorizeModal = ({
  requestId,
  amount,
  onClose = () => {},
}: {
  requestId: string;
  amount: number;
  onClose: () => any;
}) => {
  const runCharge = useMutation("acceptCharge");
  const declineCharge = useMutation("declineCharge");

  return (
    <motion.div
      initial={{ opacity: 0, y: -300 }}
      animate={{
        opacity: 1,
        y: 0,
      }}
      className="bg-white rounded-xl p-4 m-2 shadow-xl w-full max-w-lg">
      <h2 className="text-xl font-bold my-3">
        Payment Requested
      </h2>

      <div className="text-sm">
        Cafete @T9 is requesting a payment of{" "}
        <p className="text-white font-bold bg-green-800 px-2 py-1 rounded-lg">
          {amount} T9C
        </p>
      </div>

      <div className="grid grid-cols-2 gap-3 mt-3">
        <button
          type="button"
          onClick={async () => {
            try {
              await runCharge({
                id: requestId,
              });
            } catch (e: any) {
              toast.error(`${e.message}`);
            }
            onClose();
          }}
          className="bg-green-700 px-3 py-3 rounded-xl font-semibold text-white">
          Accept
        </button>
        <button
          type="button"
          onClick={async () => {
            try {
              await declineCharge({
                id: requestId,
              });
            } catch (e: any) {
              toast.error(e.message);
            }
            onClose();
          }}
          className="bg-red-700 px-3 py-3 rounded-xl font-semibold text-white">
          Reject
        </button>
      </div>
    </motion.div>
  );
};
