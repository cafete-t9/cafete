import {
  adminProcedure,
  router,
  semiPublicProcedure,
  publicProcedure,
  teamProcedure,
  userProcedure,
} from "../t";
import db from "src/db";
import {
  queryUserInputSchema,
  queryUserOutputSchema,
  queryUsersInputSchema,
  queryUsersOutputSchema,
  queryUserWithFlatSchema,
  username,
} from "../schemas/user";
import { sub } from "date-fns";
import { FlatActivityType } from "@prisma/client";
import { z } from "zod";
import {
  paginatedProductListSchema,
  productListSchema,
  productSchema,
  queryProductInputSchema,
  queryProductsInputSchema,
} from "../schemas/products";
import {
  queryCategoriesInputSchema,
  queryCategoriesOutputSchema,
  queryCategoryInputSchema,
  queryCategoryOutputSchema,
} from "../schemas/categories";
import { emitActivity } from "../emitters";
import { getUserWithFlatData } from "../services/flatrate/getUserWithFlatdata";

export const queryRouter = router({
  getUserByName: adminProcedure
    .input(z.object({ username: username }))
    .output(queryUserOutputSchema)
    .query(async (req) => {
      return await db.user.findFirst({
        where: { username: req.input.username }
      })
    }),
  getUser: teamProcedure
    .input(queryUserInputSchema)
    .output(queryUserOutputSchema)
    .query(async (req) => {
      return await db.user.findFirst({
        where: { id: req.input.id },
      });
    }),
  getUserWithFlat: teamProcedure
    .input(queryUserInputSchema)
    .output(queryUserWithFlatSchema)
    .query(async (req) => {
      const data = await getUserWithFlatData({
        userId: req.input.id,
      });

      return data;
    }),
  isUsernameFree: semiPublicProcedure
    .input(z.object({ username: z.string() }))
    .output(z.boolean())
    .query(async ({ input, ctx }) => {
      if (input.username === ctx?.user?.username) {
        return true;
      }

      const exists = await db.user.findFirst({
        where: {
          username: input.username,
        },
      });

      return !exists;
    }),
  getProfile: userProcedure
    .output(queryUserOutputSchema)
    .query(async (req) => {
      return await db.user.findFirst({
        where: { id: req.ctx.user.id },
      });
    }),
  getWalletByUserId: adminProcedure
    .input(z.object({ id: z.string().uuid() }))
    .query(async (req) => {
    return await db.wallet.findFirst({
      where: {
        user: {
          id: req.input.id,
        }
      }
    })
  }),
  getWallet: userProcedure.query(async (req) => {
    return await db.wallet.findFirst({
      where: {
        user: {
          id: req.ctx.user.id,
        },
      },
    });
  }),
  getProducts: publicProcedure
    .input(queryProductsInputSchema)
    .output(paginatedProductListSchema)
    .query(async (req) => {
      const [count, items] = await Promise.all([
        db.product.count({
          where: req.input.where,
        }),
        db.product.findMany({
          where: req.input.where,
          skip: req.input.page * req.input.itemsPerPage,
          take: req.input.itemsPerPage,
          include: {
            categories: true,
          },
        }),
      ]);

      const result = {
        items,
        itemsPerPage: req.input.itemsPerPage,
        maxPage: Math.round(count / req.input.itemsPerPage),
        page: req.input.page,
        sortBy: req.input.sortBy,
      };

      return result;
    }),
  getProduct: adminProcedure
    .input(queryProductInputSchema)
    .output(productSchema.nullable())
    .query(async (req) => {
      return await db.product.findFirst({
        where: {
          id: req.input.id,
        },
        include: {
          categories: true,
        },
      });
    }),

  getCategories: adminProcedure
    .input(queryCategoriesInputSchema)
    .output(queryCategoriesOutputSchema)
    .query(async (req) => {
      const [count, items] = await Promise.all([
        db.productCategory.count({
          where: req.input.where,
        }),
        db.productCategory.findMany({
          where: req.input.where,
          skip: req.input.page * req.input.itemsPerPage,
          take: req.input.itemsPerPage,
        }),
      ]);

      const result = {
        items,
        itemsPerPage: req.input.itemsPerPage,
        maxPage: Math.round(count / req.input.itemsPerPage),
        page: req.input.page,
        sortBy: req.input.sortBy,
      };

      return result;
    }),

  getCategory: adminProcedure
    .input(queryCategoryInputSchema)
    .output(queryCategoryOutputSchema)
    .query(async (req) => {
      return await db.productCategory.findFirst({
        where: {
          id: req.input.id,
        },
      });
    }),
  getUsers: teamProcedure
    .input(queryUsersInputSchema)
    .output(queryUsersOutputSchema)
    .query(async (req) => {
      const [count, items] = await Promise.all([
        db.user.count({
          where: req.input.where,
        }),
        db.user.findMany({
          where: req.input.where,
          skip: req.input.page * req.input.itemsPerPage,
          take: req.input.itemsPerPage,
          select: {
            id: true,
            createdAt: true,
            updatedAt: true,
            name: true,
            username: true,
            roles: true,
          },
        }),
      ]);

      const result = {
        items,
        itemsPerPage: req.input.itemsPerPage,
        maxPage: Math.round(count / req.input.itemsPerPage),
        page: req.input.page,
        sortBy: req.input.sortBy,
      };

      return result;
    }),
  getCurrentFlatData: userProcedure.query(
    async ({ ctx: { user } }) => {
      const flat = await db.flatrateSubscription.findFirst({
        where: {
          userId: user.id,
          expiresAt: {
            gte: new Date(),
          },
        },
      });

      const history = await db.flatrateActivity.findMany({
        where: {
          userId: user.id,
        },
        orderBy: {
          createdAt: "desc",
        },
        take: 10,
      });

      const isBlocked = await db.flatrateActivity.findFirst(
        {
          where: {
            userId: user.id,
            subscriptionId: flat?.id,
            type: FlatActivityType.PRODUCTBUY,
            createdAt: {
              gt: sub(new Date(), { minutes: 90 }),
            },
          },
          orderBy: { 
            createdAt: "desc",
          },
        },
      );

      return {
        flat,
        history,
        coffeeBlock: isBlocked,
      };
    },
  ),
});

export type QueryRouter = typeof queryRouter;
