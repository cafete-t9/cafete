import { getAuthenticatedUser } from "@astroshuttle/auth";
import type { User } from "@prisma/client";
import db from "src/db";

export const pageShield = async (
  request: Request,
  role?: string,
) => {
  const session = getAuthenticatedUser({
    server: request,
  }) as { user: Pick<User, "username" | "id" | "roles"> };
  if (!session?.user)
    return { user: null, restrictPage: true };

  const actualUser = await db.user.findFirst({
    where: { id: session.user.id },
  });

  if (!session?.user || !actualUser)
    return { user: null, restrictPage: true };

  if (role && !actualUser?.roles.includes(role))
    return { user: null, restrictPage: true };

  return {
    user: {
      username: actualUser.username,
      id: actualUser.id,
      roles: actualUser.roles,
      name: actualUser.name,
    },
    restrictPage: false,
  };
};

export const pageShieldList = async (
  request: Request,
  roles: string[],
) => {
  const session = getAuthenticatedUser({
    server: request,
  }) as { user: Pick<User, "username" | "id" | "roles"> };
  if (!session?.user)
    return { user: null, restrictPage: true };

  const actualUser = await db.user.findFirst({
    where: { id: session.user.id },
  });

  if (!session?.user || !actualUser)
    return { user: null, restrictPage: true };

  let foundRole = false;
  for (let role of roles) {
    if (actualUser.roles.includes(role)) {
      foundRole = true;
    }
  }

  if (!foundRole) {
    return { user: null, restrictPage: true };
  }

  return {
    user: {
      username: actualUser.username,
      id: actualUser.id,
      roles: actualUser.roles,
      name: actualUser.name,
    },
    restrictPage: false,
  };
};
