// import { EventEmitter } from "node:events";
import { events } from "./memory";

export const emitActivity = async (
  userId: string,
  payload: { title: string; description?: string | null, href?: string | null, data?: any },
) => {
  await events.publish(userId, payload);
};

export const activityEvents = (userId: string, listener: (msg: any) => any) => {
  return {
    async subscribe() {
      return await events.subscribe(userId, listener);
    },
    async unsubscribe() {
      return await events.unsubscribe(
        userId,
        listener,
      );
    },
  };
};

// await redisClient.set("key", "value");
// const value = await redisClient.get("key");
// await redisClient.disconnect();
// export const activityEmitter = new EventEmitter().setMaxListeners(10);

// export const emitActivity = (userId: string, payload: any) => {
//   activityEmitter.emit(userId, payload)
// }

// export const activityEvents = (userId: string) => {
//   return {
//     subscribe(listener: (...args: any[]) => void) {
//       console.log("current listener count for: ", userId, " :: ", activityEmitter.listenerCount(userId))
//       return activityEmitter.on(userId, listener);
//     },
//     unsubscribe(listener?: (...args: any[]) => void) {
//       console.log("Is it even unsubscribing?")
//       activityEmitter.removeListener(userId, () => {

//       })

//       return activityEmitter.off(userId, listener ? listener : () => {})
//     }
//   };
// };
