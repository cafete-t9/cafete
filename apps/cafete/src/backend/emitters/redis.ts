import {
  createClient,
  RedisClientType,
  RedisFunctions,
  RedisModules,
  RedisScripts,
} from "redis";

export const redisClient = createClient({
  url: import.meta.env.REDIS_URL,
});

interface EventInterface {
  __isReady: boolean;
  __isPublisherReady: boolean;
  __clientList: {
    [key: string]: RedisClientType &
      RedisFunctions &
      RedisModules &
      RedisScripts | null;
  };
  __publisher: typeof redisClient;
  __publisherReady: () => Promise<EventInterface>;
  isReady: boolean;
  ready: () => Promise<EventInterface>;
  connect: () => Promise<void>;
  subscribe: (
    channelId: string,
    listener: (data: any) => {},
  ) => Promise<boolean>;
  unsubscribe: (channelId: string) => Promise<void>;
  publish: (channelId: string, message: any) => Promise<number>;
}

export const events: EventInterface = {
  __isReady: false,
  __clientList: {},
  __publisher: redisClient.duplicate(),
  __isPublisherReady: false,
  get isReady() {
    return this.__isReady;
  },
  async __publisherReady() {
    await this.ready()
    if (this.__isPublisherReady) {
      return this;
    }
    await this.__publisher.connect();
    this.__isPublisherReady = true;

    return this;
  },
  async ready() {
    if (this.__isReady) {
      return this;
    }
    await this.connect();
    this.__isReady = true;

    return this;
  },
  async connect() {
    const res = await redisClient.connect();
    return res;
  },
  async subscribe(channelId, listener) {
    await this.ready();
    if (!this.__clientList[channelId]) {
      const client = redisClient.duplicate();
      this.__clientList[channelId] = client as any;
      await client.connect();
    }

    const client = this.__clientList[channelId];

    client?.subscribe(channelId, (msg) => {
      listener(JSON.parse(msg));
    });

    return true;
  },
  async unsubscribe(channelId: string) {
    await this.ready()
    if (
      this.__clientList[channelId]?.isOpen
    ) {
      await this.__clientList[channelId]?.disconnect();
    }
    this.__clientList[channelId] = null;
  },
  async publish(channelId, message) {
    await this.__publisherReady();
    return await this.__publisher.publish(channelId, JSON.stringify(message))
    // await redisClient.publish(channelId, JSON.stringify(message))
  }
};


if (import.meta.hot?.accept()) {
  console.log("Reload Redis")
}