import { EventEmitter } from "node:events";

const emitter = new EventEmitter().setMaxListeners(10)

export const events = {
  __isReady: true,
  get isReady() {
    return this.__isReady
  },
  async ready() {
    return this;
  },
  async connect() {
    return emitter;
  },
  async subscribe(channelId: string, listener: (data: any) => any) {
    emitter.on(channelId, listener)
    return true;
  },
  async unsubscribe(channelId: string, listener: (data: any) => any) {
    emitter.off(channelId, listener)

    return true;
  },
  async publish(channelId: string, message: any) {
    emitter.emit(channelId, message)
  }
}