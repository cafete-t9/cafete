// import { queryRouter } from "./queries/";
// import { mutationRouter } from "./mutations/";
import { t, teamProcedure, userProcedure } from "./t";
import { getAuthenticatedUser } from "@astroshuttle/auth";
import type { User } from "@prisma/client";
import { queryRouter } from "./queries";
import { mutationRouter } from "./mutations";
import { observable } from "@trpc/server/observable";
import { activityEvents } from "./emitters";

export const appRouter = t.mergeRouters(
  queryRouter,
  mutationRouter,
  t.router({
    onNotification: userProcedure.subscription((req) => {
      // `resolve()` is triggered for each client when they start subscribing `onAdd`
      // return an `observable` with a callback which is triggered immediately
      return observable<any>((emit) => {
        const onNotification = (data: any) => {
          // emit data to client
          emit.next(data);
        };
        const userEvents = activityEvents(
          req.ctx.user.id,
          onNotification,
        );

        const subscribe = async () => {
          await userEvents.subscribe();
        };

        subscribe();

        // unsubscribe function when client disconnects or stops subscribing
        return async () => {
          // ee.off('add', onAdd);
          await userEvents.unsubscribe();
        };
      });
    }),
    onChargeRequest: userProcedure.subscription((req) => {
      return observable<any>((emit) => {
        const onChargeRequested = (data: any) => {
          emit.next(data);
        };
        const events = activityEvents(
          `${req.ctx.user.id}:requestCharge`,
          onChargeRequested,
        );

        const subscribe = async () => {
          await events.subscribe();
        };

        subscribe();

        // unsubscribe function when client disconnects or stops subscribing
        return async () => {
          await events.unsubscribe();
        };
      });
    }),
    onTeamRequest: teamProcedure.subscription((req) => {
      return observable<any>((emit) => {
        const onChargeRequested = (data: any) => {
          emit.next(data);
        };
        const events = activityEvents(
          `${req.ctx.user.id}:paymentDone`,
          onChargeRequested,
        );

        const subscribe = async () => {
          await events.subscribe();
        };

        subscribe();

        // unsubscribe function when client disconnects or stops subscribing
        return async () => {
          await events.unsubscribe();
        };
      });
    }),
    onChargeReject: teamProcedure.subscription((req) => {
      return observable<any>((emit) => {
        const onChargeRequested = (data: any) => {
          emit.next(data);
        };
        const events = activityEvents(
          `${req.ctx.user.id}:paymentAborted`,
          onChargeRequested,
        );

        const subscribe = async () => {
          await events.subscribe();
        };

        subscribe();

        // unsubscribe function when client disconnects or stops subscribing
        return async () => {
          await events.unsubscribe();
        };
      });
    }),
  }),
);
export type AppRouter = typeof appRouter;

export const serverRpc = {
  authorize: (request: Request) => {
    const session = getAuthenticatedUser({
      server: request,
    }) as { user: Pick<User, "username" | "id" | "roles"> };
    return appRouter.createCaller({
      user: session?.user,
    });
  },
};

type USR = {
  id: string;
  username: string;
  roles: string[];
};
type AdminRPC = ReturnType<typeof appRouter.createCaller>;
type AdminFullRPC = AdminRPC & {
  impersonate: (
    user: USR,
  ) => ReturnType<typeof appRouter.createCaller>;
};

const rpcAdmin: AdminRPC = appRouter.createCaller({
  user: {
    id: "SERVER",
    username: "SERVER",
    roles: ["USER", "TEAM", "ADMIN"],
  },
});

(rpcAdmin as any).impersonate = (user: USR) => {
  return appRouter.createCaller({
    user: {
      id: user.id,
      username: user.username,
      roles: user.roles,
    },
  });
};

export const serverRpcAdmin: AdminFullRPC =
  rpcAdmin as unknown as any;
