import { ActivityType, Prisma } from "@prisma/client"
import db from "src/db"
import { createActivity } from "../createActivity";

export const transferCoins = async ({
  fromId,
  toId,
  amount,
  comment
}: {
  fromId: string;
  toId: string;
  amount: number;
  comment?: string|null;
}) => {
  const senderWallet = await getWallet({
    userId: fromId,
    tx: db,
  })
  
  if (!senderWallet) {
    throw new Error("You don't have a wallet.")
  }

  const receiverWallet = await getWallet({ userId: toId, tx: db })

  if (!receiverWallet) {
    throw new Error("Recipient has no wallet.")
  }

  await db.$transaction(async (tx) => {
    if (senderWallet.id === receiverWallet.id) {
      throw new Error("You can't send T9C to yourself...")
    }

    const senderUpdatedWallet = await decrementBalance({ walletId: senderWallet.id, tx, amount })

    if (senderUpdatedWallet.balance < 0) {
      throw new Error("Insufficient balance. Please get some T9C in the Cafete downstairs!")
    }

    await incrementBalance({ walletId: receiverWallet.id, tx, amount })

    await logTransaction({ fromId, toId, amount, comment, tx })
  })

  await createActivity({
    userId: toId,
    issuedById: fromId,
    internalId: "TXU",
    title: `Received ${amount} T9C from @${senderWallet.user?.username}`,
    description: comment || "",
    type: ActivityType.RECEIVE,
  })

  await createActivity({
    issuedById: toId,
    userId: fromId,
    internalId: "TXU",
    title: `Sent ${amount} T9C to @${receiverWallet.user?.username}`,
    description: comment || "",
    type: ActivityType.RECEIVE,
  })

  return [senderWallet, receiverWallet];
}

export const getWallet = async ({ userId, tx }: { userId: string, tx: Prisma.TransactionClient }) => {
  return await tx.wallet.findFirst({
    where: {
      user: {
        id: userId
      }
    },
    include: {
      user: true
    }
  })
}

export const decrementBalance = async ({ walletId, tx, amount }: { walletId: string; tx: Prisma.TransactionClient, amount: number }) => {
  return await tx.wallet.update({
    where: { id: walletId },
    data: {
      balance: {
        decrement: amount
      }
    }
  })
}

export const incrementBalance = async ({ walletId, tx, amount }: { walletId: string; tx: Prisma.TransactionClient, amount: number }) => {
  return await tx.wallet.update({
    where: { id: walletId },
    data: {
      balance: {
        increment: amount
      }
    }
  })
}

export const logTransaction = async ({ fromId, toId, comment, amount, tx }: {
  fromId: string;
  toId: string;
  amount: number;
  comment?: string|null;
  tx: Prisma.TransactionClient
}) => {
  await tx.transaction.create({
    data: {
      fromId,
      toId,
      comment,
      amount
    },
  })
}