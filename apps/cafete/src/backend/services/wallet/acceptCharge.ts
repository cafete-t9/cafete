import {
  ActivityType,
  PaymentStatus,
} from "@prisma/client";
import db from "src/db";
import { getWallet } from "./transferCoins";

export const acceptCharge = async ({
  userId,
  requestId,
}: {
  userId: string;
  requestId: string;
}) => {
  const charge = await db.paymentRequest.findFirst({
    where: {
      id: requestId,
    },
  });

  if (!charge || charge.userId !== userId || charge?.status !== PaymentStatus.REQUESTED) {
    throw new Error(
      "You are not authorized to handle this payment request.",
    );
  }
  const wallet = await getWallet({ userId, tx: db });

  if (!wallet) {
    throw new Error(
      "Not authorized to handle this payment request.",
    );
  }

  const newCharge = await db.$transaction(async (tx) => {
    const updatedWallet = await tx.wallet.update({
      where: {
        id: wallet.id,
      },
      data: {
        balance: {
          decrement: charge.amount,
        },
      },
    });

    if (updatedWallet.balance < 0) {
      throw new Error(
        "Not enough T9C to handle this transaction",
      );
    }

    await tx.activity.create({
      data: {
        internalId: "TXCHARGE",
        title: "Payment",
        description: "Payment authorized @Cafete T9",
        type: ActivityType.PRODUCTBUY,
        issuedById: charge.issuedById,
        userId: userId,
      },
    });

    return await tx.paymentRequest.update({
      where: { id: requestId },
      data: {
        status: PaymentStatus.DONE,
      },
    });
  });

  return newCharge;
};
