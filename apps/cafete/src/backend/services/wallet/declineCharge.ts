import { PaymentStatus } from "@prisma/client";
import db from "src/db";

export const declineCharge = async ({
  requestId,
  userId
}: { requestId: string, userId: string }) => {
  const charge = await db.paymentRequest.findFirst({
    where: {
      id: requestId,
    },
  });

  if (!charge || charge.userId !== userId || charge?.status !== PaymentStatus.REQUESTED) {
    throw new Error(
      "You are not authorized to handle this payment request.",
    );
  }

  const updatedCharge = await db.paymentRequest.update({
    where: { id: requestId },
    data: {
      status: PaymentStatus.ABORTED,
    },
  });

  return updatedCharge
}