import db from "src/db";

export const getActiveSubscription = async ({ userId }: { userId: string }) => {
  return db.flatrateSubscription.findFirst({
    where: {
      userId: userId,
      expiresAt: {
        gt: new Date(),
      },
    },
    orderBy: {
      expiresAt: "desc",
    },
  });
}