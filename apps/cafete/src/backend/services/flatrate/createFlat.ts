import { ActivityType } from "@prisma/client";
import { add } from "date-fns";
import db from "src/db";
import { createActivity } from "../createActivity";

export const createFlat = async ({
  days,
  userId,
  workerId,
}: { days: number; userId: string; workerId: string }) => {
  const subscription = await db.flatrateSubscription.create({
    data: {
      type: "COFFEE",
      expiresAt: add(new Date(), { days }),
      userId: userId,
      issuedById: workerId,
    },
  });

  await createActivity({
    title: "Flat Activated",
    description: `@ Cafete T9 (${days} days)`,
    issuedById: workerId,
    type: ActivityType.BUY,
    userId: userId,
  })

  return subscription;
}