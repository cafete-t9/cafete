import db from "src/db";
import { FlatActivityType } from "@prisma/client";
import { sub } from "date-fns";
// import db from "src/db";

export const getUserWithFlatData = async ({ userId }: { userId: string }) => {
  const user = await db.user.findFirst({
    where: { id: userId },
  });

  const flat = await db.flatrateSubscription.findFirst({
    where: {
      userId: userId,
      expiresAt: {
        gte: new Date(),
      },
    },
  });

  const isBlocked = await db.flatrateActivity.findFirst(
    {
      where: {
        userId: userId,
        subscriptionId: flat?.id,
        type: FlatActivityType.PRODUCTBUY,
        createdAt: {
          gt: sub(new Date(), { minutes: 90 }),
        },
      },
      orderBy: {
        createdAt: "desc",
      },
    },
  );

  return {
    user,
    flat,
    isBlocked: !!isBlocked
  }
}