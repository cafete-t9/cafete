import {
  FlatActivityType,
  ActivityType,
} from "@prisma/client";
import db from "src/db";
import { createActivity } from "../createActivity";
import { canChargeFlatrateCoffee } from "./canChargeFlatrateCoffee";

export const chargeCoffeeFromFlat = async ({
  userId,
  workerId,
}: {
  userId: string;
  workerId: string;
}) => {
  const subscriptionId = await canChargeFlatrateCoffee({ userId })

  const newActivity = await db.flatrateActivity.create({
    data: {
      title: "☕ Coffee Time",
      description: "@Cafete T9",
      issuedById: workerId,
      userId: userId,
      type: FlatActivityType.PRODUCTBUY,
      subscriptionId: subscriptionId,
    },
  });

  const generalActivity = await createActivity({
    title: "☕ Coffee Time",
    description: "@Cafete T9",
    issuedById: workerId,
    userId: userId,
    type: ActivityType.PRODUCTBUY,
    internalId: "COFFEE_FLAT_CHARGE",
  });

  return newActivity;
};
