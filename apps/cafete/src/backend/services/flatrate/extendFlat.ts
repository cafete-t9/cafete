import { ActivityType, FlatrateSubscription } from "@prisma/client";
import { add } from "date-fns";
import db from "src/db";
import { createActivity } from "../createActivity";

export const extendFlat = async ({
  userId,
  workerId,
  days,
  subscription,
}: {
  userId: string;
  workerId: string;
  days: number;
  subscription: FlatrateSubscription;
}) => {
  await createActivity({
    title: "Flat Extended",
    description: `@ Cafete T9 (${days} days)`,
    issuedById: workerId,
    type: ActivityType.BUY,
    userId: userId,
  });

  return await db.flatrateSubscription.update({
    where: {
      id: subscription.id,
    },
    data: {
      expiresAt: add(subscription.expiresAt, { days }),
    },
  });
};
