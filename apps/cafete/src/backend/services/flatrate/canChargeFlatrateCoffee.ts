import { FlatActivityType } from "@prisma/client";
import db from "src/db";
import { sub } from "date-fns";

export const canChargeFlatrateCoffee = async ({
  userId
}: {
  userId: string
}) => {
  const activeSub = await db.flatrateSubscription.findFirst(
    {
      where: {
        userId: userId,
        expiresAt: {
          gt: new Date(),
        },
      },
      orderBy: {
        expiresAt: "desc",
      },
    },
  );

  const lastCoffee = await db.flatrateActivity.findFirst({
    where: {
      userId: userId,
      type: FlatActivityType.PRODUCTBUY,
      createdAt: {
        gt: sub(new Date(), { minutes: 90 }),
      },
    },
    orderBy: {
      createdAt: "desc",
    },
  });

  if (!activeSub) {
    throw new Error("User has no coffee flatrate!")
  }
  
  if (lastCoffee) {
    throw new Error("User is still blocked!");
  }

  return activeSub.id;
}