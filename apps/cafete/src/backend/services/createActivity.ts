import type { ActivityType } from "@prisma/client"
import db from "src/db"


export const createActivity = async ({
  issuedById,
  userId,
  type,
  internalId,
  title,
  description
}: {
  title: string;
  description?: string;
  issuedById: string,
  userId: string,
  type: ActivityType,
  internalId?: string;
}) => {
  return await db.activity.create({
    data: {
      title: title,
      description: description,
      issuedById: issuedById,
      userId: userId,
      type: type,
      internalId: internalId,
    },
  })
}