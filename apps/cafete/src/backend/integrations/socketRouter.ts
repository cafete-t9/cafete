import { getAuthenticatedUser } from "@astroshuttle/auth";
import type { User } from "@prisma/client";
import type {
  CreateWSSContextFnOptions,
} from "@trpc/server/adapters/ws";
import { AppRouter, appRouter } from "..";
import db from "src/db";

export const socketContext = async ({ req }: { req: CreateWSSContextFnOptions["req"] }) => {
  const session = getAuthenticatedUser({
    webSocket: req
  }) as {
    user: Pick<User, "email" | "id" | "roles">;
  };

  if (session?.user?.id) {
    const actualUser = await db.user.findFirst({
      where: {
        id: session.user.id,
      },
      select: {
        email: true,
        id: true,
        roles: true,
        name: true,
        username: true,
      },
    });

    return {
      user: actualUser ? actualUser : undefined,
    };
  }
  return { user: undefined };
}

export const socketRouter = appRouter
export type SocketRouter = AppRouter