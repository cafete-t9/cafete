import { generateRPCClient } from "@astroshuttle/rpc-client"
import superjson from 'superjson';
import { createTRPCProxyClient, createWSClient, wsLink } from "@trpc/client";
import type { SocketRouter, socketRouter } from "./integrations/socketRouter";

const client = generateRPCClient<SocketRouter, SocketRouter>({
  url: `${import.meta.env?.SITE_BASE || ""}/api/trpc`,
  transformer: superjson,
  socketURL: `${import.meta.env?.PUBLIC_WS_BASE || ""}`
})

// if (typeof window !== "undefined") {
//   console.log("browser ctx", import.meta.env?.PUBLIC_WS_BASE)
//   client.rawClient.onAdd.subscribe(undefined, {
//     onStarted() {
//         console.log("conn eestabilshed")
//     },
//     onData(value) {
//         console.log("DATA ", value)
//     },
//     onError(err) {
//         console.error(err)
//     },
//     onComplete() {
//         console.log("completed")
//     },
//     onStopped() {
//         console.log("stopped")
//     },
//   })

//   console.log("subscription started")
// }

export const useQuery = client.useQuery
export const useMutation = client.useMutation
export const runQuery = client.runQueryAndThrow
export const runMutation = client.runMutationAndThrow
export const runSubscription = client.subscribe