import db from "src/db";
import { z } from "zod";
import { updateUserInputSchema, queryUserOutputSchema, username } from "../schemas/user";
import { router, teamProcedure, userProcedure } from "../t";

export const userRouter = router({
  updateUser: teamProcedure
    .input(updateUserInputSchema)
    .output(queryUserOutputSchema)
    .mutation(async ({ input }) => {
      const { id, username, name, roles } = input;

      return await db.user.update({
        where: { id },
        data: {
          username,
          name,
          roles,
        },
      });
    }),
    updateProfile: userProcedure
    .input(
      z.object({
        name: z.string().optional().nullable(),
        username: username,
      }),
    )
    .output(queryUserOutputSchema)
    .mutation(async ({ input, ctx }) => {
      if (input.username !== ctx.user.username) {
        const usernameExists = await db.user.findFirst({
          where: {
            username: input.username,
          },
        });

        if (usernameExists) {
          throw new Error("Username already exists");
        }
      }

      return await db.user.update({
        where: {
          id: ctx.user.id,
        },
        data: {
          ...input,
        },
      });
    }),
})