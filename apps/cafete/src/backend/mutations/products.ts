import db from "src/db";
import { createCategoryInputSchema, updateCategoryInputSchema } from "../schemas/categories";
import { createProductSchema, updateProductSchema, deleteProductSchema } from "../schemas/products";
import { adminProcedure, router } from "../t";

export const productRouter = router({
  createCategory: adminProcedure
    .input(createCategoryInputSchema)
    .mutation(async ({ input, ctx }) => {
      return await db.productCategory.create({
        data: {
          ...input,
        },
      });
    }),
  updateCategory: adminProcedure
    .input(updateCategoryInputSchema)
    .mutation(async ({ input, ctx }) => {
      return await db.productCategory.update({
        where: {
          id: input.id,
        },
        data: {
          ...input,
        },
      });
    }),
  createProduct: adminProcedure
    .input(createProductSchema)
    .mutation(async ({ input, ctx }) => {
      return await db.product.create({
        data: {
          ...input,
          available: !!input.available,
          categories: {
            connect: input.categories.map((k) => ({
              id: k.id,
            })),
          },
        },
      });
    }),

  updateProduct: adminProcedure
    .input(updateProductSchema)
    .mutation(async ({ input, ctx }) => {
      return await db.product.update({
        where: {
          id: input.id,
        },
        data: {
          ...input,
          available: !!input.available,
          categories: {
            set: input.categories.map((k) => ({
              id: k.id,
            })),
          },
        },
      });
    }),

  deleteProduct: adminProcedure
    .input(deleteProductSchema)
    .mutation(async ({ input, ctx }) => {
      return await db.product.delete({
        where: {
          id: input.id,
        },
      });
    }),

})