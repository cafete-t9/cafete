import {
  ActivityType,
  PaymentStatus,
} from "@prisma/client";
import db from "src/db";
import { z } from "zod";
import { emitActivity } from "../emitters";
import { acceptCharge } from "../services/wallet/acceptCharge";
import { declineCharge } from "../services/wallet/declineCharge";
import { transferCoins } from "../services/wallet/transferCoins";
import { router, teamProcedure, userProcedure } from "../t";

export const walletRouter = router({
  transferCoins: userProcedure
    .input(
      z.object({
        amount: z.number().int().min(1).max(1000),
        receiver: z.string(),
        comment: z
          .string()
          .max(150)
          .optional()
          .nullable()
          .default("-"),
      }),
    )
    .mutation(async ({ input, ctx }) => {
      const recipient = await db.user.findFirst({
        where: { username: input.receiver },
      });

      if (!recipient) {
        throw new Error("Recipient not found.");
      }

      await transferCoins({
        amount: input.amount,
        fromId: ctx.user.id,
        toId: recipient.id,
        comment: input.comment,
      });

      await emitActivity(recipient.id || "", {
        title: `Received ${input.amount} T9C`,
        description: `From: ${ctx.user.username} \n${
          input.comment || ""
        }`,
        href: "/wallet",
      });

      await emitActivity(ctx.user.id, {
        title: "Sent T9C",
        description: `Successfully sent ${input.amount} T9C to ${recipient.username}`,
        href: "/wallet",
      });

      return { done: true };
    }),
  declineCharge: userProcedure
    .input(
      z.object({
        id: z.string().uuid(),
      }),
    )
    .mutation(async ({ input, ctx }) => {
      const charge = await declineCharge({
        requestId: input.id,
        userId: ctx.user.id,
      });

      await emitActivity(`${charge.userId}`, {
        title: "Payment aborted",
        description:
          "You've rejected the payment. No T9C was charged.",
      });

      await emitActivity(
        `${charge.issuedById}:paymentAborted`,
        {
          title: "Payment Rejected",
          description: "Payment was rejected by user.",
        },
      );

      return { done: true };
    }),
  acceptCharge: userProcedure
    .input(
      z.object({
        id: z.string().uuid(),
      }),
    )
    .mutation(async ({ input, ctx }) => {
      try {
        const charge = await acceptCharge({
          userId: ctx.user.id,
          requestId: input.id,
        });

        await emitActivity(ctx.user.id, {
          title: "Payment Done!",
          description: `You've successfully paid ${charge.amount} T9C @Cafete T9`,
          href: "/wallet",
        });

        await emitActivity(
          `${charge?.issuedById}:paymentDone`,
          {
            title: "Payment successful!",
            description: `User was successfully charged ${charge?.amount} T9C`,
          },
        );
      } catch (e: any) {
        const charge = await db.paymentRequest.findFirst({
          where: { id: input.id },
        });
        await emitActivity(
          `${charge?.issuedById}:paymentAborted`,
          {
            title: "Payment Rejected",
            description: `There was an error: ${e.message}`,
          },
        );

        throw e;
      }

      return { done: true };
    }),
  requestCharge: teamProcedure
    .input(
      z.object({
        amount: z.number().int().min(1).max(50000),
        userId: z.string().uuid(),
      }),
    )
    .mutation(async ({ input, ctx }) => {
      const receiverWallet = await db.wallet.findFirst({
        where: {
          user: {
            id: input.userId,
          },
        },
        include: {
          user: true,
        },
      });

      if (!receiverWallet) {
        throw new Error(
          "The paying user does not have a wallet.",
        );
      }

      const pRequest = await db.paymentRequest.create({
        data: {
          userId: input.userId,
          amount: input.amount,
          issuedById: ctx.user.id,
          status: PaymentStatus.REQUESTED,
        },
      });

      await emitActivity(
        `${receiverWallet.user?.id}:requestCharge` || "",
        {
          title: `REQ_PAY`,
          description: `${input.amount}`,
          data: {
            requestId: pRequest.id,
          },
        },
      );

      return { done: true };
    }),
  topUpCoins: teamProcedure
    .input(
      z.object({
        amount: z.number().int().min(1).max(50000),
        receiver: z.string(),
      }),
    )
    .mutation(async ({ input, ctx }) => {
      const tx = await db.$transaction(async (tx) => {
        const receiverWallet = await tx.wallet.findFirst({
          where: {
            user: {
              username: input.receiver,
            },
          },
          include: {
            user: true,
          },
        });

        if (!receiverWallet) {
          throw new Error(
            "The receiving user does not have a wallet.",
          );
        }

        await tx.wallet.update({
          where: {
            id: receiverWallet.id,
          },
          data: {
            balance: {
              increment: input.amount,
            },
          },
        });
        await tx.transaction.create({
          data: {
            fromId: ctx.user.id,
            toId: receiverWallet.user!.id,
            comment: `TOP-UP T9C`,
            amount: input.amount,
          },
        });
        await tx.activity.create({
          data: {
            userId: receiverWallet.user!.id,
            issuedById: ctx.user.id,
            internalId: "TOP",
            title: `${input.amount} T9C was added to your balance`,
            description: "Top up @Cafete T9",
            type: ActivityType.TOPUP,
          },
        });

        await emitActivity(receiverWallet.user?.id || "", {
          title: `Received ${input.amount} T9C`,
          description: `🤖 Spend wisely 🤖`,
          href: "/wallet",
        });

        await emitActivity(ctx.user.id, {
          title: "Sent T9C",
          description: `Successfully sent ${input.amount} T9C to ${receiverWallet.user?.username}`,
        });
      });

      return { done: true };
    }),

  createWallet: userProcedure.mutation(
    async ({ input, ctx }) => {
      const hasWallet = await db.wallet.findFirst({
        where: {
          user: {
            id: ctx.user.id,
          },
        },
      });

      if (hasWallet) {
        return hasWallet;
      }

      const update = await db.user.update({
        where: {
          id: ctx.user.id,
        },
        data: {
          wallet: {
            create: {
              balance: 0,
            },
          },
        },
      });

      return await db.wallet.findFirst({
        where: { user: { id: ctx.user.id } },
      });
    },
  ),
});
