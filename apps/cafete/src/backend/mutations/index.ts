import { mergeRouters } from "src/backend/t";
import { flatRouter } from "./coffeeFlat";
import { productRouter } from "./products";
import { userRouter } from "./user";
import { walletRouter } from "./wallet";

export const mutationRouter = mergeRouters(
  flatRouter,
  productRouter,
  userRouter,
  walletRouter,
);

export type MutationRouter = typeof mutationRouter;
