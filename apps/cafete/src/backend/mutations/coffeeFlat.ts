import {
  FlatActivityType,
  ActivityType,
} from "@prisma/client";
import { sub, add } from "date-fns";
import db from "src/db";
import { emitActivity } from "../emitters";
import {
  chargeCoffeeFromFlatInputSchema,
  chargeCoffeeFromFlatOutputSchema,
  activateFlatInputSchema,
  activateFlatOutputSchema,
} from "../schemas/coffeeFlat";
import { chargeCoffeeFromFlat } from "../services/flatrate/chargeCoffeeFromFlat";
import { createFlat } from "../services/flatrate/createFlat";
import { extendFlat } from "../services/flatrate/extendFlat";
import { getActiveSubscription } from "../services/flatrate/getActiveSubscription";
import { router, teamProcedure } from "../t";

export const flatRouter = router({
  chargeCoffeeFromFlat: teamProcedure
    .input(chargeCoffeeFromFlatInputSchema)
    .output(chargeCoffeeFromFlatOutputSchema)
    .mutation(async ({ input, ctx: { user } }) => {
      const newActivity = await chargeCoffeeFromFlat({
        userId: input.userId,
        workerId: user.id,
      });

      await emitActivity(input.userId, {
        title: "☕ Coffee Time",
        description: "@Cafete T9",
      });

      return newActivity;
    }),
  activateCoffeeFlat: teamProcedure
    .input(activateFlatInputSchema)
    .output(activateFlatOutputSchema)
    .mutation(async ({ input, ctx: { user } }) => {
      const { type, userId, days } = input;
      let activeSub = await getActiveSubscription({
        userId,
      });

      // Subscription exists already, extend instead
      if (activeSub) {
        activeSub = await extendFlat({
          userId,
          workerId: user.id,
          days,
          subscription: activeSub,
        });
        // No subscription exists, create one
      } else {
        activeSub = await createFlat({
          days,
          userId,
          workerId: user.id,
        });

        await emitActivity(userId, {
          title: "Flat Activated",
          description: `@ Cafete T9 (${days} days)`,
        });
      }

      return activeSub;
    }),
});
