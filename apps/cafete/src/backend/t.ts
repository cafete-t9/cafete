import type { User } from "@prisma/client";
import { TRPCError, initTRPC } from '@trpc/server';
import superjson from 'superjson';

export const t = initTRPC.context<{ user?: Pick<User, "username"|"id"|"roles"> }>().create({
  transformer: superjson
})

const isAdmin = t.middleware(({ next, ctx, }) => {
  if (!ctx.user?.roles.includes("ADMIN")) {
    throw new TRPCError({ code: "UNAUTHORIZED" })
  }

  return next({
    ctx: {
      user: ctx.user,
    }
  })
})


const isTeamMember = t.middleware(({ next, ctx, }) => {
  if (!ctx.user?.roles.includes("TEAM")) {
    if (!ctx.user?.roles.includes("ADMIN")) {
      throw new TRPCError({ code: "UNAUTHORIZED" })
    }
  }

  return next({
    ctx: {
      user: ctx.user,
    }
  })
})

const isUser = t.middleware(({ next, ctx, }) => {
  if (!ctx.user) {
      throw new TRPCError({ code: "UNAUTHORIZED" })
  }

  return next({
    ctx: {
      user: ctx.user,
    }
  })
})

const couldBeUser = t.middleware(({ next, ctx, }) => {
  return next({
    ctx: {
      user: ctx?.user,
    }
  })
})


export const middleware = t.middleware;
export const router = t.router;
export const mergeRouters = t.mergeRouters;
export const publicProcedure = t.procedure;
export const semiPublicProcedure = t.procedure.use(couldBeUser)
export const adminProcedure = t.procedure.use(isAdmin)
export const teamProcedure = t.procedure.use(isTeamMember)
export const userProcedure = t.procedure.use(isUser)