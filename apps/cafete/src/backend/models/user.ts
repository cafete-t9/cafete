import { signInHandler, signUpHandler } from "@astroshuttle/auth";
import db from "src/db";
// import db from "../../../db";

export const authentication = {
  async handleSignUp({ username, password, confirmPassword }: { username: string, password: string, confirmPassword: string }) {
    return await signUpHandler({ confirmPassword, username, password }, db)
  },
  async handleSignIn({ username, password }: { username: string, password: string }) {
    return await signInHandler({ username, password }, db)
  }
}