import db from "src/db";
import { z } from "zod";
import { username } from "./user";

export const uniqUsernameServer = username.optional().refine(async (usr) => {
  if (!usr) return true;

  return !(await db.user.findFirst({
    where: {
      username: usr
    }
  }))
}, { message: "Username already exists" })
