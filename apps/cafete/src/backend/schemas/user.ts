import {
  EditSchema,
  paginated,
  paginatedInput,
  TableSchema,
} from "@astroshuttle/ui/types";
import type { User } from "@prisma/client";
import { z } from "zod";
import { runQuery } from "../client";

export const contains = z
  .object({
    contains: z.string().optional(),
  })
  .optional();

const usernameRegex =
  /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}$/gim;
const id = z.string();
const email = z.string();
export const username = z
  .string()
  .trim()
  .min(2)
  .max(30)
  .regex(usernameRegex, {
    message: "Please use a valid username (0-9, a-z, _).",
  })
  .transform((usr) => usr.toLowerCase());
const roles = z.array(z.string());
const name = z.string().nullable();
const createdAt = z.date();
const updatedAt = z.date();

export const uniqUsernameClient = username.refine(
  async (usr) => {
    try {
      const isFree = await runQuery("isUsernameFree", {
        username: usr,
      });

      return isFree;
    } catch (e) {
      return false;
    }
  },
  { message: "Username already exists" },
);

export const queryUsersInputSchema = paginatedInput(
  z.object({
    username: contains,
  }),
);

export const queryUserInputSchema = z.object({
  id,
});

export const queryUserOutputSchema = z
  .object({
    id,
    username,
    roles,
    name,
    createdAt,
    updatedAt,
  })
  .nullable();

export const queryUsersOutputSchema = paginated(
  z.object({
    id,
    username,
    roles,
    name,
    createdAt,
    updatedAt,
  }),
);

export const queryUserWithFlatSchema = z.object({
  user: queryUserOutputSchema,
  isBlocked: z.boolean().optional().nullable(),
  flat: z.object({
    id,
    type: z.string(),
    expiresAt: z.date(),
  }).nullable().optional(),
})

export const deleteUsersInputSchema = z.object({
  id,
});

export const updateUserInputSchema = z.object({
  id,
  username,
  roles,
  name,
});

export const usersTableSchema: TableSchema<User> = {
  username: { renderAs: "string", label: "Username" },
  roles: {
    renderAs: "pill",
    label: "Role",
    type: "array",
  },
  name: { renderAs: "string", label: "Full name" },
  createdAt: { renderAs: "date", label: "Signed up" },
};

export const usersEditSchema: EditSchema<User> = {
  id: {
    renderAs: "string",
    disabled: true,
    label: "ID",
    description: "",
    type: "primitive",
  },
  username: {
    renderAs: "string",
    label: "Username",
    type: "primitive",
    description: "",
  },
  name: {
    renderAs: "string",
    label: "Full Name",
    description: "Users full name",
  },
  roles: {
    renderAs: "pill",
    type: "array",
    label: "Roles",
    description: "User Roles",
    options: [
      { label: "Admin", value: "ADMIN" },
      { label: "Team", value: "TEAM" },
      { label: "User", value: "USER" },
    ],
  },
};
