import { FlatActivityType, FlatrateType } from "@prisma/client"
import { z, infer } from "zod"

const userId = z.string().uuid()
const type = z.nativeEnum(FlatrateType)
const activityType = z.nativeEnum(FlatActivityType)
const expiresAt = z.date().min(new Date())
const createdAt = z.date()
const days = z.number().int().min(1)
const issuedById = userId

export const activateFlatInputSchema = z.object({
  userId,
  type,
  days
})

export const chargeCoffeeFromFlatInputSchema = z.object({
  userId,
})

export const chargeCoffeeFromFlatOutputSchema = z.object({
  userId,
  createdAt,
  issuedById,
  type: activityType,
  title: z.string(),
  description: z.string(),
})

export const activateFlatOutputSchema = z.object({
  userId,
  type,
  issuedById,
  expiresAt,
  createdAt
})