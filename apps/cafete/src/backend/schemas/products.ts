import {
  paginated,
  paginatedInput,
} from "@astroshuttle/ui/types";
import { z } from "zod";

const id = z.string().uuid();
const available = z.boolean().optional().nullable();
const stock = z.number().int().min(0).max(100000);
const title = z.string().optional().nullable();
const description = title;
const euroPrice = z.number().int().min(0).max(1000000);
const walletPrice = z.number().int().min(0).max(1000000);
const createdAt = z.date();
const updatedAt = createdAt;
const str = z.string();
const categories = z.array(
  z.object({ id, title: str, key: str }),
);

export const createProductSchema = z.object({
  available,
  stock,
  title,
  description,
  euroPrice,
  walletPrice,
  categories,
});

export const productSchema = z.object({
  id,
  available,
  stock,
  title,
  description,
  euroPrice,
  walletPrice,
  categories,
  createdAt,
  updatedAt,
});

export const productListSchema = z.array(productSchema);

export const paginatedProductListSchema =
  paginated(productSchema);


const keyQuery = z.object({
  key: z.string(),
}).optional()

export const queryProductsInputSchema = paginatedInput(
  z.object({
    categories: z.object({
      every: keyQuery,
      some: keyQuery,
    }).optional().nullable(),
  }),
);

export const queryProductInputSchema = z.object({ id });

export const updateProductSchema = z.object({
  id,
  available,
  stock,
  title,
  description,
  euroPrice,
  categories,
  walletPrice,
});

export const deleteProductSchema = z.object({
  id,
});
