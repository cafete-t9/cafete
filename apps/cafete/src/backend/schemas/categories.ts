import {
  EditSchema,
  paginated,
  paginatedInput,
  TableSchema,
} from "@astroshuttle/ui/types";
import type { ProductCategory } from "@prisma/client";
import { z } from "zod";
import { runQuery } from "../client";

export const contains = z
  .object({
    contains: z.string().optional(),
  })
  .optional();

const id = z.string();
const key = z.string();
const title = z.string();
const createdAt = z.date();
const updatedAt = z.date();

export const queryCategoriesInputSchema = paginatedInput(
  z.object({
    username: contains,
  }),
);

export const queryCategoryInputSchema = z.object({
  id,
});

export const queryCategoryOutputSchema = z
  .object({
    id,
    title,
    key
  })
  .nullable();

export const queryCategoriesOutputSchema = paginated(
  z.object({
    id,
    title,
    key,
  }),
);

export const deleteCategoryInputSchema = z.object({
  id,
});

export const createCategoryInputSchema = z.object({
  title,
  key
});

export const updateCategoryInputSchema = z.object({
  id,
  title,
  key
});

export const categoriesTableSchema: TableSchema<ProductCategory> = {
  id: { renderAs: "string", label: "ID" },
  title: { renderAs: "string", label: "Title" },
  key: { renderAs: "string", label: "INTERNAL_KEY" }
};

export const categoryEditSchema: EditSchema<ProductCategory> = {
  id: {
    renderAs: "string",
    disabled: true,
    label: "ID",
    description: "",
    type: "primitive",
  },
  key: {
    renderAs: "string",
    label: "INTERNAL_KEY",
    type: "primitive",
    description: "",
  },
  title: {
    renderAs: "string",
    label: "Title",
    type: "primitive",
    description: "",
  },
};

export const categoryCreateSchema: EditSchema<ProductCategory> = {
  key: {
    renderAs: "string",
    label: "INTERNAL_KEY",
    type: "primitive",
    description: "",
  },
  title: {
    renderAs: "string",
    label: "title",
    type: "primitive",
    description: "",
  },
};