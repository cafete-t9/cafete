import dotenv from "dotenv"
import express from 'express';
import rateLimit from "express-rate-limit"
import { RateLimiterMemory } from "rate-limiter-flexible";
// import { handler as ssrHandler } from './dist/server/entry.mjs';

dotenv.config()

const rateLimiter = new RateLimiterMemory({
  points: 200,
  duration: 1,
  blockDuration: 5,
})

export const runApp = (handler) => {
  const app = express();
  app.set("trust proxy", true)
  
  const limiter = rateLimit({
    windowMs: 1 * 60 * 1000, // 1 minute
    max: 1000,
    standardHeaders: true,
    legacyHeaders: false,
  })
  
  const limiterMiddleware = async (req, res, next) => {
    const ip = req.ip
    try {
      await rateLimiter.consume(req.ip)
      next()
    } catch (e) {
      res.status(429).json({ error: "Too many requests!" })
    }
  }
  // app.use(limiter)
  app.use(express.static('dist/client/'))
  app.use(limiterMiddleware, ssrHandler);
  
  const port = process.env.PORT ? parseInt(process.env.PORT) : 3001;
  app.listen(port, () => {
    console.log("server started")
  });
}

export const createWSServer = () => {
  
}