import { defineConfig } from "astro/config";
import react from "@astrojs/react";
import tailwind from "@astrojs/tailwind";
import node from "@astrojs/node";
import astroAdmin from "@astroshuttle/core";
import path from "node:path";
import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// https://astro.build/config
const config =  defineConfig({
  integrations: [
    react(),
    tailwind(),
    astroAdmin({ modules: { tasks: true } }),
  ],
  output: "server",
  adapter: node({
    mode: "middleware",
  }),
  outDir: "dist/exports",
  vite: {
    ssr: {
      external: [],
      noExternal: []
    },
    build: {
      emptyOutDir: false,
      commonjsOptions: {
        transformMixedEsModules: true,
      },
      rollupOptions: {
        output: {
          manualChunks(id) {
            // if (id.includes("src/backend/index.ts")) {
            //   return "router"
            // }
            if (id.includes("src/backend")) {
              return "backend"
            }
          },
          chunkFileNames: (chunkInfo) => {
            // console.log(chunkInfo.)
            if (chunkInfo.name === "backend") {
              return "chunks/backend.js"
            }

            if (chunkInfo.name === "router") {
              return "chunks/router.js"
            }

            // chunkInfo.name
            return "chunks/[name].[hash].js"
          },
          minifyInternalExports: false,
        }
      }
    },
    plugins: [],
  },
});

export default config;