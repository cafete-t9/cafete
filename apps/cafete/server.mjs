
import dotenv from "dotenv"
import express from 'express';
import { RateLimiterMemory } from "rate-limiter-flexible";
import { handler as ssrHandler } from './dist/main/dist/server/entry.mjs';
const ws = await import("ws")
import { applyWSSHandler } from "@trpc/server/adapters/ws";
dotenv.config()
import { socketContext, socketRouter } from "./socket.mjs"

const rateLimiter = new RateLimiterMemory({
  points: 200,
  duration: 1,
  blockDuration: 5,
})

const wss = new ws.WebSocketServer({
  noServer: true,
});

const handler = applyWSSHandler({
  batching: {
    enabled: true,
  },
  wss,
  router: socketRouter,
  createContext: socketContext,
});

const app = express(); 
app.set("trust proxy", true)

const limiterMiddleware = async (req, res, next) => {
  const ip = req.ip
  try {
    await rateLimiter.consume(req.ip)
    next()
  } catch (e) {
    res.status(429).json({ error: "Too many requests!" })
  }
}
app.use(express.static('dist/main/dist/client/'))
app.use(ssrHandler);

const port = process.env.PORT ? parseInt(process.env.PORT) : 3001;

const exApp = app.listen(port, () => {
  console.log("server started on ::", port)
});

exApp.on("upgrade", (request, socket, head) => {
  wss.handleUpgrade(request, socket, head, socket => {
    wss.emit("connection", socket, request);
  })
})

process.on('SIGTERM', () => {
  console.log('SIGTERM');
  handler.broadcastReconnectNotification();
  wss.close();
});