import dotenv from "dotenv";
import express from "express";
import { handler as ssrHandler } from "./../dist/main/dist/server/entry.mjs";
// import WebSocketServer from "ws"
const ws = await import("ws");
import { applyWSSHandler } from "@trpc/server/adapters/ws";
dotenv.config();
import path from "node:path";
import {
  socketContext,
  socketRouter,
} from "./../socket.mjs";

const envParsed = dotenv.config({
  path: path.resolve(process.cwd(), ".env.test"),
});

const env = envParsed.parsed;

if (env?.NODE_ENV !== "test") {
  throw new Error("NOT A TEST ENVIRONMENT");
}

const wss = new ws.WebSocketServer({
  noServer: true,
});

const handler = applyWSSHandler({
  batching: {
    enabled: true,
  },
  wss,
  router: socketRouter,
  createContext: socketContext,
});

const app = express();
app.set("trust proxy", true);

app.use(express.static("dist/client/"));
app.use(ssrHandler);

// eslint-disable-next-line turbo/no-undeclared-env-vars
const port = process.env.PORT
  ? // eslint-disable-next-line turbo/no-undeclared-env-vars
    parseInt(process.env.PORT)
  : 3001;

export const runServer = () =>
  new Promise<void>((resolve) => {
    const exApp = app.listen(port, () => {
      console.log("server started on ::", port);
      resolve();
    });

    exApp.on("upgrade", (request, socket, head) => {
      wss.handleUpgrade(request, socket, head, (socket) => {
        wss.emit("connection", socket, request);
      });
    });
  });
