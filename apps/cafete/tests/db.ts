import { Prisma, PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

const tables = Prisma.dmmf.datamodel.models
  .map((model) => model.dbName || model.name)
  .filter((table) => table);

export const clearDB = async () => {
  await prisma.$transaction([
    ...tables.map((table) =>
      prisma.$executeRawUnsafe(
        `TRUNCATE "${table}" CASCADE;`,
      ),
    ),
  ]);
};

export const testDB = new PrismaClient();

export default testDB;
