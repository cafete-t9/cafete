import { test, expect } from "@playwright/test";
import { Prisma } from "@prisma/client";
import testDB, { clearDB } from "./db.js";
import { runServer } from "./spinupServer.js"
// import testDB, { clearDB} from "./db";

const BASEURL = "http://localhost:3000/";
const url = (path: string) => `${BASEURL}${path}`

test.beforeAll(async () => {
  await runServer()
  console.log("clearing db");
  await clearDB();
  console.log("db cleared");
});

test.afterAll(async () => {
  await clearDB()
  console.log("Cleaned up Test DB")
})

test("meta is correct", async ({ page }) => {
  await page.goto(BASEURL);

  await expect(page).toHaveTitle("Cafete T9 Flat");
});


test.describe("Authentication", () => {
  test.describe("Sign Up", () => {
    test("Sign Up Form works", async ({ page }) => {
      await page.goto(url("sign-up"))
  
      await expect(page).toHaveTitle("Sign Up")
  
      await page.type('input[name=username]', 'testuser');
      await page.type('input[name=password]', 'test123');
      await page.type('input[name=confirmPassword]', 'test123');
      await page.screenshot({ path: 'screenshots/signup.png' })
      await page.click('button[type=submit]:visible');
      await page.waitForURL(url("flat"))
      await page.screenshot({ path: 'screenshots/post-signup.png' })
  
      const dbUser = await testDB.user.findFirstOrThrow({ where: { username: "testuser" }})
  
      await expect(dbUser?.username).toBe("testuser")
    })

    test("Rejects falsy usernames", async ({ page }) => {
      await page.goto(url("sign-up"))
  
      await expect(page).toHaveTitle("Sign Up")
  
      await page.type('input[name=username]', 'test@#)user');
      await page.type('input[name=password]', 'test123');
      await page.type('input[name=confirmPassword]', 'test123');
      await page.screenshot({ path: 'screenshots/falsy-signup.png' })
      await page.click('button[type=submit]:visible');
      const errormsg = await page.waitForSelector("label[for=username] > span")

      await expect(errormsg.innerText()).resolves.toEqual("Please use a valid username (0-9, a-z, _).")
      await page.screenshot({ path: 'screenshots/post-falsy-signup.png' })
  
      const dbUser = await testDB.user.findFirstOrThrow({ where: { username: "testuser" }})
  
      await expect(dbUser?.username).toBe("testuser")
    })

  })

  test.describe("Sign In", () => {
    test("Sign In works", async ({ page }) => {
      await page.goto(url("sign-in"))
  
      await expect(page).toHaveTitle("Login")
  
      await page.type('input[name=username]', 'testuser');
      await page.type('input[name=password]', 'test123');
      await page.screenshot({ path: 'screenshots/sign-in.png' })
      await page.click('button[type=submit]:visible');
      await page.waitForURL(url("flat"))
      await page.screenshot({ path: 'screenshots/post-signin.png' })
    })
  })
})

test.describe("Wallets", () => {
  
})