import { defineConfig } from "astro/config";

import react from "@astrojs/react";
import tailwind from "@astrojs/tailwind";
import node from "@astrojs/node";
import prefetch from "@astrojs/prefetch";
import sitemap from "@astrojs/sitemap";
import image from "@astrojs/image";

import astroAdmin from "@astroshuttle/core";

// https://astro.build/config
export default defineConfig({
  integrations: [
    react(),
    tailwind(),
    prefetch(),
    sitemap(),
    image(),
    astroAdmin(),
  ],
  output: "server",
  site: process.env.SITE_BASE,
  adapter: node(),
  vite: {
    ssr: {
      external: [
      ],
    },
    build: {
      commonjsOptions: {
        transformMixedEsModules: true,
      },
    },
  },
});
