import {
  queryUserInputSchema,
  queryUserOutputSchema,
  queryUsersInputSchema,
  queryUsersOutputSchema,
} from "src/backend/schemas/user";
import {
  router,
  teamProcedure,
} from "src/backend/t";
import db from "src/db";

export const userRouter = router({
  getUser: teamProcedure
    .input(queryUserInputSchema)
    .output(queryUserOutputSchema)
    .query(async (req) => {
      return await db.user.findFirst({
        where: { id: req.input.id },
      });
    }),
  getUsers: teamProcedure
    .input(queryUsersInputSchema)
    .output(queryUsersOutputSchema)
    .query(async (req) => {
      const [count, items] = await Promise.all([
        db.user.count({
          where: req.input.where,
        }),
        db.user.findMany({
          where: req.input.where,
          skip: req.input.page * req.input.itemsPerPage,
          take: req.input.itemsPerPage,
          select: {
            id: true,
            createdAt: true,
            updatedAt: true,
            name: true,
            email: true,
            roles: true,
          },
        }),
      ]);

      const result = {
        items,
        itemsPerPage: req.input.itemsPerPage,
        maxPage: Math.round(count / req.input.itemsPerPage),
        page: req.input.page,
        sortBy: req.input.sortBy,
      };

      return result;
    }),
});

export type QueryRouter = typeof userRouter;
