import { publicProcedure, router, t, userProcedure } from "src/backend/t";
import { z } from "zod"
import { userRouter } from "./user";

export const queryRouter = t.mergeRouters(router({
  test: publicProcedure
    .input(z.object({
      testValue: z.string()
    }))
    .query((req) => {
      return { hello: "Hello World" };
    }),
  testUser: userProcedure
    .query((req) => {
      return { email: req.ctx.user.email }
    }),
}), userRouter);

export type QueryRouter = typeof queryRouter;
