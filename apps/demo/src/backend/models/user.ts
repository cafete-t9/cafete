import { signInHandler, signUpHandler } from "@astroshuttle/auth";
import db from "src/db";

export const authentication = {
  async handleSignUp({ email, password, confirmPassword }: { email: string, password: string, confirmPassword: string }) {
    return await signUpHandler({ confirmPassword, email, password }, db)
  },
  async handleSignIn({ email, password }: { email: string, password: string }) {
    return await signInHandler({ email, password }, db)
  }
}