import type { AppRouter } from "./index";
import { generateRPCClient } from "@astroshuttle/rpc-client"
import superjson from 'superjson';

const client = generateRPCClient<AppRouter>({
  url: `${import.meta.env?.SITE_BASE || ""}/api/trpc`,
  transformer: superjson,
})

export const useQuery = client.useQuery
export const useMutation = client.useMutation
export const runQuery = client.runQueryAndThrow
export const runMutation = client.runMutationAndThrow