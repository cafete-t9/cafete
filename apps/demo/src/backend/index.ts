import { queryRouter } from "./queries/";
import { mutationRouter } from "./mutations/";
import { t } from "./t";
import { getAuthenticatedUser } from "@astroshuttle/auth";
import type { User } from "@prisma/client";

export const appRouter = t.mergeRouters(
  queryRouter, mutationRouter
)

export type AppRouter = typeof appRouter
export const serverRpc = {
  ...appRouter.createCaller({}),
  authorize: (request: Request) => {
    const session = getAuthenticatedUser({ server: request }) as { user: Pick<User, "email"|"id"|"roles"> }

    return appRouter.createCaller({
      user: session?.user
    })
  }
}
export const restrictPage = (request: Request, role?: string) => {
  const session = getAuthenticatedUser({ server: request }) as { user: Pick<User, "email"|"id"|"roles"> }

  if (!session?.user) return true
  if (role && session.user.roles.includes(role)) true

  return false
}

export const pageShield = (request: Request, role?: string) => {
  const session = getAuthenticatedUser({ server: request }) as { user: Pick<User, "email"|"id"|"roles"> }
  
  if (!session?.user) return { user: null, restrictPage: true }
  if (role && !session.user.roles.includes(role)) return { user: null, restrictPage: true }

  return {
    user: session.user,
    restrictPage: false
  }
}