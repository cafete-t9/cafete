import { z } from "zod";
import zodToJsonSchema from "zod-to-json-schema";

const page = z.number().min(0).default(0);
const maxPage = z.number().min(0).default(0);
const itemsPerPage = z.number().min(1).max(50).default(20);
const sortBy = z
  .object({
    key: z.string(),
    direction: z.enum(["asc", "desc"]),
  })
  .optional();

export function paginated<T extends z.AnyZodObject>(
  zShape: T,
) {
  return z.object({
    page,
    itemsPerPage,
    sortBy,
    maxPage,
    items: z.array(zShape),
  });
}

export const paginatedInput = (zShape: z.AnyZodObject) =>
  z.object({
    page,
    itemsPerPage,
    sortBy,
    where: zShape,
  });

export type PaginatedZodType<T extends z.AnyZodObject> =
  ReturnType<typeof paginated<T>>;
export type PaginatedShape<T extends z.AnyZodObject> =
  z.infer<PaginatedZodType<T>>;

export const schemaJSON = (zShape: z.AnyZodObject) =>
  zodToJsonSchema(zShape);



export const contains = z
  .object({
    contains: z.string().optional(),
  })
  .optional();
