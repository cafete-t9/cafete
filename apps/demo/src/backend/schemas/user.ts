import type {
  EditSchema,
  TableSchema,
} from "@astroshuttle/ui/types";
import type { User } from "@prisma/client";
import { z } from "zod";
import {
  paginated,
  paginatedInput,
  contains,
} from "./paginated";

const id = z.string();
const email = z.string();
const roles = z.array(z.string());
const name = z.string().nullable();
const createdAt = z.date();
const updatedAt = z.date();

export const queryUsersInputSchema = paginatedInput(
  z.object({
    email: contains,
  }),
);

export const queryUserInputSchema = z.object({
  id,
});

export const queryUserOutputSchema = z.object({
  id,
  email,
  roles,
  name,
  createdAt,
  updatedAt,
}).nullable();

export const queryUsersOutputSchema = paginated(
  z.object({
    id,
    email,
    roles,
    name,
    createdAt,
    updatedAt,
  }),
);

export const deleteUsersInputSchema = z.object({
  id,
});

export const updateUserInputSchema = z.object({
  id,
  email,
  roles,
  name,
})

export const usersTableSchema: TableSchema<User> = {
  email: { renderAs: "string", label: "Email" },
  roles: {
    renderAs: "pill",
    label: "Role",
    type: "array",
  },
  name: { renderAs: "string", label: "Full name" },
  createdAt: { renderAs: "date", label: "Signed up" },
};

export const usersEditSchema: EditSchema<User> = {
  id: {
    renderAs: "string",
    disabled: true,
    label: "ID",
    description: "",
    type: "primitive",
  },
  email: {
    renderAs: "string",
    label: "E-Mail",
    type: "primitive",
    description: "E-Mail adress of the user"
  },
  name: {
    renderAs: "string",
    label: "Full Name",
    description: "Users full name"
  },
  roles: {
    renderAs: "pill",
    type: "array",
    label: "Roles",
    description: "User Roles",
    options: [
      { label: "Admin", value: "ADMIN" },
      { label: "Team", value: "TEAM" },
      { label: "User", value: "USER" },
      { label: "User2", value: "USER2" },
      { label: "User3", value: "USER3" },
      { label: "User4", value: "USER4" },
      { label: "User5", value: "USER5" },
    ]
  },
  description: {
    renderAs: "rte",
    type: "json",
    label: "Rich Text",
  }
};
