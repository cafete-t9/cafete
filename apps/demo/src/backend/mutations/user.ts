import { adminProcedure, publicProcedure, router, teamProcedure } from "src/backend/t";
import db from "src/db";
import { z } from "zod"
import { queryUserOutputSchema, updateUserInputSchema } from "../schemas/user";

export const userMutationRouter = router({
  updateUser: teamProcedure
    .input(updateUserInputSchema)
    .output(queryUserOutputSchema)
    .mutation(async ({ input }) => {
      const { id, email, name, roles } = input;

      return await db.user.update({
        where: { id },
        data: {
          email, name, roles
        }
      })
    })
});

export type MutationRouter = typeof userMutationRouter;
