import { adminProcedure, publicProcedure, router, teamProcedure } from "src/backend/t";
import db from "src/db";
import { z } from "zod"

export const boardMutationRouter = router({
  createBoard: adminProcedure
    .input(z.object({}))
    .mutation(async ({ input, ctx }) => {
      const creator = ctx.user
      return db.board.create({
        data: {
          
        }
      })
    })
  // testMutation: publicProcedure
  //   .input(z.object({
  //     value: z.string()
  //   }))
  //   .mutation((req) => {
  //     return { hello: req.input.value };
  //   }),
});

export type MutationRouter = typeof boardMutationRouter;
