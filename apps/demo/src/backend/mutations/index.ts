import { mergeRouters, publicProcedure, router } from "src/backend/t";
import { z } from "zod"
import { userMutationRouter } from "./user";

export const testRouter = router({
  testMutation: publicProcedure
    .input(z.object({
      value: z.string()
    }))
    .mutation((req) => {
      return { hello: req.input.value };
    }),
});

export const mutationRouter = mergeRouters(testRouter, userMutationRouter)

export type MutationRouter = typeof mutationRouter;
