import { AdminToast } from "@astroshuttle/ui";
import { useStore } from '@nanostores/react';
import { useEffect } from "react";
import { mountClientState, pageTransition, removeToast, toastList } from "src/state";

export function ToastProvider({}) {
  const $toastList = useStore(toastList)
  const $pageState = useStore(pageTransition)

  useEffect(() => {
    mountClientState()
  }, [])


  return (
    <div className="fixed top-4 left-1/2 -translate-x-1/2 z-[90] flex flex-col gap-3">
      {
        !$pageState.isTransitioning && (
          <AdminToast toasts={$toastList} onClose={(toastId) => {
            removeToast(toastId)
          }} />
        )
      }
    </div>
  )
}