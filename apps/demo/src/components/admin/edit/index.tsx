import { useMutation, useQuery } from "src/backend/client";
import { EditEntity, ErrorHandler } from "@astroshuttle/ui";
import type { EditSchema } from "@astroshuttle/ui/types";
import { useStore } from "@nanostores/react";
import { useEffect, useState } from "react";
import { pushRoute, pushToast, toastList } from "src/state";
import { TableLoader } from "../table";

type QueryParams = Parameters<typeof useQuery>;
type QueryKey = QueryParams[0];
type QueryInput<T extends QueryKey> = Parameters<
  typeof useQuery<T>
>[1];

type MutationParams = Parameters<typeof useMutation>;
type MutationKey = MutationParams[0];
// type MutationInput<T extends MutationKey> = Parameters<
//   typeof useMutation<T>
// >[1];

const emptyQuery = {};


export function ModelEditor<T extends QueryKey, S extends MutationKey>({
  schema,
  queryKey,
  defaultQueryInput,
  mutationKey,
  idKey,
  redirectTo,
}: {
  schema: EditSchema;
  queryKey: T;
  mutationKey: S;
  defaultQueryInput: QueryInput<T>;
  idKey?: string;
  redirectTo?: string;
}) {
  const $toastList = useStore(toastList)
  const { data, error, isLoading, refresh } = useQuery(
    queryKey,
    defaultQueryInput,
  );
  const mutate = useMutation<S>(mutationKey)

  if (isLoading) {
    return <ModelEditorLoader />;
  }

  if (error) {
    return <ErrorHandler error={error} />;
  }
 
  return <EditEntity schema={schema} data={data} onSubmit={async (formData) => {
    await mutate(formData)
    // toastList.push({ title: "Success!" });
    // toastList.set([...toastList.get(), { title: "Success!" }])
    pushToast({ title: "User updated", type: "success" })
    console.log("pushed", $toastList)
    if (redirectTo) {
      pushRoute(redirectTo)
    }
  }} />
}

export function ModelEditorLoader() {
  return (
    <>
      <div className="animate-pulse w-full bg-gray-300 dark:bg-gray-600 rounded-2xl min-h-[500px]">
        <div className="w-full h-30 border-b border-b-gray-400 dark:border-b-gray-700 rounded-t-2xl" />
      </div>
    </>
  );
}
