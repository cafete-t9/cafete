import { useMutation, useQuery, runQuery } from "src/backend/client"
import { useState } from "react"

// const data = await rpcClient.query("test", { testValue: "ok" })
// const result = await runQuery("testUser") 

export const UserCard = ({ prefill }: { prefill: any }) => {
  const [mutationRes, setMut] = useState({})
  const { data, isLoading } = useQuery("testUser")
  const mutate = useMutation("testMutation")

  return <div>
    user: {isLoading && "loading..."}
    {JSON.stringify(data)}

    <button className="px-3 py-1 bg-gray-300" onClick={async () => {
      const result = await mutate({ value: `Hello World: ${Math.random()}` })

      setMut(result)
      
    }}>Test Mutation</button>
    mutation result: {JSON.stringify(mutationRes)}

    <p>prefill: {JSON.stringify(prefill)}</p>
  </div>
}