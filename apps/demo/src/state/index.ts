import type { TAdminToast } from "@astroshuttle/ui";
import { persistentAtom } from "@nanostores/persistent";
import { nanoid } from "nanoid";
import superjson from "superjson";

export const pageTransition = persistentAtom<{
  isTransitioning?: boolean;
  mountedAt: number;
}>(
  "pageTransition",
  {
    isTransitioning: false,
    mountedAt: Date.now(),
  },
  {
    encode: superjson.stringify,
    decode: superjson.parse,
  },
);

export const mountClientState = () => {
  toastList.set([
    ...toastList.get().map((toast) => {
      const now = Date.now()
      const diff =
        now -
        toast.snapshotAt;
      const duration = toast.duration || 0;

      console.log("now", now, toast.snapshotAt, diff,  Math.max(0, duration - diff))

      return {
        ...toast,
        duration: Math.max(0, duration - diff),
        snapshotAt: now,
      };
    }),
  ]);

  pageTransition.set({
    ...pageTransition.get(),
    mountedAt: Date.now(),
    isTransitioning: false,
  });
};

export const pushRoute = (pathname: string) => {
  pageTransition.set({
    ...pageTransition.get(),
    isTransitioning: true,
  });
  window.location.pathname = pathname;
};

export const toastList = persistentAtom<TAdminToast[]>(
  "toasts",
  [],
  {
    encode: superjson.stringify,
    decode: superjson.parse,
  },
);

export const pushToast = ({
  title,
  description,
  duration = 1000000,
  type = "info"
}: Pick<TAdminToast, "title"|"description"|"duration"|"type">) => {
  const id = nanoid();
  const now = Date.now()
  return toastList.set([
    ...toastList.get(),
    {
      title,
      description,
      duration,
      id,
      type,
      createdAt: now,
      snapshotAt: now,
    },
  ]);
};

export const removeToast = (toastId: string) => {
  return toastList.set([
    ...toastList.get().filter((t) => t.id !== toastId),
  ]);
};
