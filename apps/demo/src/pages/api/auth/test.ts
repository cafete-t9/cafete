import type { APIContext } from "astro";
import { isAuthorized } from "@astroshuttle/auth";

export const get = async (ctx: APIContext) => {
  const isok = await isAuthorized({
    role: "ADMIN",
    ctx: ctx,
  })


  return new Response("ok")
}